package preparations

import (
	"context"
	"errors"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type prepTests struct {
	ae        *abstracts.Executor
	message   string
	reqBool   bool
	targetEnv map[string]string

	auth *mock_authuser.MockAuthorized
	c    arguments.ConcreteArgs
	msg  logging.Messenger

	mockEnvCmdr func(*testing.T)
	mockTerm    func() bool

	assertAbstract  func(*testing.T, *abstracts.Executor)
	assertAuth      func(*testing.T, authuser.Authorized)
	assertCommander func(*testing.T, command.Commander)
	assertError     func(*testing.T, error)
	assertMap       func(*testing.T, map[string]string)
}

// testSysLog is an empty logger, syslog tested via Pavilion2 primarily.
var testSysLog *logrus.Entry

func init() {
	ll := logrus.New()
	ll.Out = io.Discard
	testSysLog = logrus.NewEntry(ll)
}

func TestStdError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr("Error encountered during job: %s", "cleanup").Times(1)
	m.EXPECT().Stderr("Error encountered during job: %s", "config").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "prepare").Times(1)
	m.EXPECT().Warn("Error encountered during job: %s", "run").Times(1)

	tests := map[string]prepTests{
		"error encountered during cleanup": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			message: "cleanup",
			msg:     m,
		},
		"error encountered during run": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			message: "run",
			msg:     m,
		},
		"error encountered during prepare": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			message: "prepare",
			msg:     m,
		},
		"error encountered during config": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			message: "config",
			msg:     m,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			StdError(tt.c, tt.message, tt.msg, func() {})
		})
	}
}

func TestJobContext(t *testing.T) {
	respFile := filepath.Clean("../../../test/testdata/valid_job_response.json")

	workingContext := map[string]string{
		envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      "1",
		envkeys.UserEnvPrefix + "CI_JOB_ID":             "123",
		envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          "abc123efg456hij789kl",
		envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": "abcd1234",
		envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  "user/ci-json-example",
		envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     "user",
		envkeys.UserEnvPrefix + "CI_SERVER_URL":         "https://gitlab.example.com",
		envkeys.UserEnvPrefix + envkeys.CIJobJWT:        "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6MzI0OTE4MjAxNjB9.WHWvWckMj-ZaK6tolpKCs4XJtlVwmSdnmyTk5F3Aqw4hHK-tzk9JrfLf1MeWtXpU1H2ZTUTCKjD57wYBZsLoy5sHySd5cny_ao_MwJMbsE7s9pKEilr_4o4S57aL9-2pD56jt4w1lezgic9gQXxjCXuiPVNpCZqixUPimZ2lD3Vm1Hy2PTc8lTlHozKtdq8pttvBA1TlgiDOMxmn0DaPdFs59iBhvaGCeWhB3ZMcARDtOQxtwhBHo0Th9gquGxXxDCYoxI6c_lNhWoAlSeD3fbWp_oT8XjEb5V5se7-plxOHaJSWaWPkw6Kx17T1VyvH9ddY17HBEW7crGIYiYrGHw",
		"JOB_RESPONSE_FILE":                             respFile,
	}

	tests := map[string]prepTests{
		"config_exec - invalid required environments": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/valid_exec_config.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"config_exec - invalid configuration file": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to establish Configurer: error executing ReadFile(), open /missing/file/config.toml: no such file or directory",
				)
			},
		},
		"config_exec - valid configuration and environment": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/configs/valid_exec.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cleanup_exec - valid configuration but missing environment": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "../../../test/testdata/configs/valid_exec.toml",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "unable to identify ExecutorEnv")
				}
			},
		},
		"cleanup_exec - valid configuration and environment": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "../../../test/testdata/configs/valid_exec.toml",
				},
			},
			targetEnv: workingContext,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cleanup_exec - invalid env configuration": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "../../../test/testdata/configs/valid_exec.toml",
				},
			},
			targetEnv: map[string]string{
				configure.EnvVariable: "invalid-test-data",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "JACAMAR_CI_CONFIG_STR contents, illegal base64 data")
				}
			},
		},
		"cleanup_exec - invalid file configuration": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "missing.toml",
				},
			},
			targetEnv: map[string]string{
				configure.EnvVariable: "",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "open missing.toml: no such file or directory")
				}
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			_, _, err := JobContext(tt.c, func() {})
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewAuthorizedUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	curUser, _ := authuser.CurrentUser()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Options().Return(configure.Options{}).AnyTimes()

	invalidRunAs := mock_configure.NewMockConfigurer(ctrl)
	invalidRunAs.EXPECT().Options().Return(configure.Options{
		Auth: configure.Auth{
			RunAs: configure.RunAs{
				ValidationScript: "/some/file.bash",
				RunAsVariable:    "RUNAS",
			},
		},
	})

	_ = os.Setenv("CUSTOM_ENV_RUNAS", "$(bad)")
	defer func() { _ = os.Unsetenv("CUSTOM_ENV_RUNAS") }()

	tests := map[string]prepTests{
		"process authorized user from available state": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: curUser.Username,
					},
				},
			},
			c: arguments.ConcreteArgs{
				NoAuth: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertAuth: func(t *testing.T, auth authuser.Authorized) {
				assert.NotNil(t, auth)
				assert.Equal(t, curUser.Username, auth.CIUser().Username)
			},
		},
		"failed to process authorized user, invalid state": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "1-invalid-user",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"failed to establish validator": {
			ae: &abstracts.Executor{
				Cfg: invalidRunAs,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "failed to establish RunAs validator: invalid user provided account (defined by RUNAS variable)")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsExec: tt.ae,
				Args:    tt.c,
				SysLog:  testSysLog,
			}

			// Only state is analyzed at this time, static 'false'
			auth, err := f.NewAuthorizedUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuth != nil {
				tt.assertAuth(t, auth)
			}
		})
	}
}

func Test_stateReq(t *testing.T) {
	// Maintaining the state of an authorized user as well as job specific configurations
	// is vita. We need enforce the mechanisms used to determine if state is required.

	tests := map[string]prepTests{
		"config_exec stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			reqBool: false,
		},
		"prepare_exec stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			reqBool: true,
		},
		"run_exec stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			reqBool: true,
		},
		"cleanup_exec stage, configuration defined": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "some.toml",
				},
			},
			reqBool: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := stateReq(tt.c)

			assert.Equal(t, tt.reqBool, got, "expected state?")
		})
	}
}

func Test_stageConfig(t *testing.T) {
	tests := map[string]prepTests{
		"expect config_exec configuration": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			assertAbstract: func(t *testing.T, ae *abstracts.Executor) {
				assert.Equal(t, ae.Stage, "config_exec")
			},
		},
		"expected prepare_exec configuration": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			assertAbstract: func(t *testing.T, ae *abstracts.Executor) {
				assert.Equal(t, ae.Stage, "prepare_exec")
			},
		},
		"expect run_exec configuration": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Stage:  "stage_name",
					Script: "script.bash",
				},
			},
			assertAbstract: func(t *testing.T, ae *abstracts.Executor) {
				assert.Equal(t, ae.Stage, "stage_name")
				assert.Equal(t, ae.ScriptPath, "script.bash")
			},
		},
		"expected cleanup_exec configuration": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			assertAbstract: func(t *testing.T, ae *abstracts.Executor) {
				assert.Equal(t, ae.Stage, "cleanup_exec")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ae := &abstracts.Executor{}
			stageConfig(ae, tt.c)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, ae)
			}
		})
	}
}

func Test_NewCommander(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Mock messaging
	msg := mock_logging.NewMockMessenger(ctrl)

	//  Mock configurations
	missing := mock_configure.NewMockConfigurer(ctrl)
	missing.EXPECT().Auth().Return(
		configure.Auth{},
	).AnyTimes()
	missing.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().Auth().Return(
		configure.Auth{
			Downscope:   "none",
			JacamarPath: "/opt/jacamar/bin/jacamar",
		},
	).AnyTimes()
	none.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	su := mock_configure.NewMockConfigurer(ctrl)
	su.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "su",
		},
	).AnyTimes()
	su.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	sudo := mock_configure.NewMockConfigurer(ctrl)
	sudo.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "sudo",
		},
	).AnyTimes()
	sudo.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setgid := mock_configure.NewMockConfigurer(ctrl)
	setgid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope: "setgid",
		},
	).AnyTimes()
	setgid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	setuid := mock_configure.NewMockConfigurer(ctrl)
	setuid.EXPECT().Auth().Return(
		configure.Auth{
			Downscope:   "setuid",
			JacamarPath: "/bin",
		},
	).AnyTimes()
	setuid.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()

	// Mock authorizations
	pass := mock_authuser.NewMockAuthorized(ctrl)
	pass.EXPECT().CIUser().Return(authuser.UserContext{
		Username:  "user",
		HomeDir:   "/home/user",
		BuildsDir: "/home/user/builds",
		CacheDir:  "/home/user/cache",
		UID:       1000,
		GID:       2000,
		Groups:    []uint32{1000},
	}).AnyTimes()

	// Mock concrete arguments
	authPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
	}
	noAuthPrepare := arguments.ConcreteArgs{
		Prepare: &arguments.PrepareCmd{},
		NoAuth:  true,
	}

	noTerm := func() bool { return false }
	yesTerm := func() bool { return true }

	tests := map[string]prepTests{
		"no configuration during authorization": {
			ae: &abstracts.Executor{
				Cfg: missing,
				Msg: msg,
			},
			c: authPrepare,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"no configuration during no-auth start": {
			ae: &abstracts.Executor{
				Cfg: missing,
				Msg: msg,
			},
			c:    noAuthPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/usr/bin/env", cmd.CopiedCmd().Path)
			},
		},
		"none downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: none,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, command.DefaultJacamarBin+"/jacamar", cmd.CopiedCmd().Path)
			},
		},
		"su downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: su,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "/bin/su")
			},
		},
		"sudo downscope mechanism defined": {
			ae: &abstracts.Executor{
				Cfg: sudo,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Contains(t, cmd.CopiedCmd().Path, "sudo")
			},
		},
		"misspelled downscope in configuration with authorization": {
			ae: &abstracts.Executor{
				Cfg: setgid,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid runner configuration, ensure supported downscope is defined")
			},
		},
		"configuration, setuid run mechanism": {
			ae: &abstracts.Executor{
				Cfg: setuid,
			},
			c:    authPrepare,
			auth: pass,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCommander: func(t *testing.T, cmd command.Commander) {
				assert.Equal(t, "/bin/jacamar", cmd.CopiedCmd().Path)
			},
		},
		"active terminal, setuid run mechanism": {
			ae: &abstracts.Executor{
				Cfg: setuid,
			},
			auth:     pass,
			c:        authPrepare,
			mockTerm: yesTerm,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "setuid downscoping via active terminal (TTY) is not supported at this time")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsExec: tt.ae,
				Args:    tt.c,
				SysLog:  testSysLog,
			}

			if tt.mockTerm != nil {
				isTerminal = tt.mockTerm
			} else {
				isTerminal = noTerm
			}

			cmd, err := f.NewCommander(cancelCtx, tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCommander != nil {
				tt.assertCommander(t, cmd)
			}
		})
	}
}

func Test_execOptions(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := logging.NewMessenger()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().PrepareState(gomock.Eq(true)).Return("", errors.New("error message"))

	tests := map[string]struct {
		ae *abstracts.Executor
		c  arguments.ConcreteArgs
	}{
		"properly handle error in PrepareState": {
			ae: &abstracts.Executor{
				Msg: msg,
				Cfg: cfg,
			},
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			execOptions(tt.ae, tt.c, func() { return })
		})
	}
}
