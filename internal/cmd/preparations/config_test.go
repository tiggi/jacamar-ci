package preparations

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func passingAuth(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BuildState().Return(envparser.StatefulEnv{
		BaseDir:     "/base",
		BuildsDir:   "/builds",
		CacheDir:    "/cache",
		ScriptDir:   "/script",
		Username:    "user",
		ProjectPath: "group/project",
	}).AnyTimes()
	m.EXPECT().CIUser().Return(authuser.UserContext{
		Username:  "user",
		BaseDir:   "/base",
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
	}).AnyTimes()

	return m
}

func missingAuth(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BuildState().Return(envparser.StatefulEnv{}).AnyTimes()
	m.EXPECT().CIUser().Return(authuser.UserContext{}).AnyTimes()

	return m
}

func TestConfigExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{}).AnyTimes()

	tests := map[string]prepTests{
		"configuration prep success": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					// We have other more comprehensive tests that verify stateful
					// variables are properly formatted in the workflow. In this case
					// we need to just ensure that stdout is generated upon success.
					m.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
					return m
				}(),
				Runner: func() *mock_runmechanisms.MockRunner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					return m
				}(),
			},
			auth: passingAuth(ctrl),
		},
		"missing stateful variables during build": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Eq("unable to construct job configuration payload: unable to identify required stateful variable JACAMAR_CI_BASE_DIR"))
					m.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
					return m
				}(),
			},
			auth: missingAuth(ctrl),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsExec: tt.ae,
				Args: arguments.ConcreteArgs{
					Config: &arguments.ConfigCmd{},
				},
				SysLog: testSysLog,
			}

			f.ConfigExec(tt.auth, func() {})
		})
	}
}

func Test_buildState(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]prepTests{
		"missing stateful variables during build": {
			auth: missingAuth(ctrl),
			ae: &abstracts.Executor{
				Env: envparser.ExecutorEnv{},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "unable to identify required stateful")
				}
			},
		},
		"completed stateful variables identified": {
			auth: passingAuth(ctrl),
			ae: &abstracts.Executor{
				Env: envparser.ExecutorEnv{},
			},
			assertMap: func(t *testing.T, m map[string]string) {
				req := map[string]string{
					"JACAMAR_CI_AUTH_USERNAME":  "user",
					"JACAMAR_CI_BASE_DIR":       "/base",
					"JACAMAR_CI_BUILDS_DIR":     "/builds",
					"JACAMAR_CI_CACHE_DIR":      "/cache",
					"JACAMAR_CI_SCRIPT_DIR":     "/script",
					"JACAMAR_CI_PROJECT_PATH":   "group/project",
					"JACAMAR_CI_RUNNER_TIMEOUT": "60m",
				}
				assert.Equal(t, req, m)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsExec: tt.ae,
				Args:    tt.c,
				SysLog:  testSysLog,
			}

			got, err := f.buildState(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertMap != nil {
				tt.assertMap(t, got)
			}
		})
	}
}
