package preparations

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/version"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// proj represents the excepted json STDOUT of the config_exec stage.
type proj struct {
	Builds   string            `json:"builds_dir"`
	Cache    string            `json:"cache_dir"`
	Share    bool              `json:"builds_dir_is_shared"`
	Hostname string            `json:"hostname"`
	State    map[string]string `json:"job_env"`
	Driver   map[string]string `json:"driver"`
}

// ConfigExec realizes the config_exec custom executor stage. It will run without leveraging
// any authorized user, instead valid JSON payload is created for the upstream runner to ingest.
// No previously established configuration or system settings will be modified.
func (f Factory) ConfigExec(auth authuser.Authorized, exit func()) {
	js, err := f.configJSON(auth)
	if err != nil {
		errMsg := fmt.Sprintf("unable to construct job configuration payload: %s", err.Error())
		StdError(f.Args, errMsg, f.AbsExec.Msg, exit)
		return
	}

	f.AbsExec.Msg.Stdout(js) // Print configuration.
	f.SysLog.Debug(fmt.Sprintf("configuration payload established'"))
}

// configJSON encodes the included proj struct as json and returns it as a string for stdout.
func (f Factory) configJSON(auth authuser.Authorized) (string, error) {
	// Failure to retrieve hostname (resulting in empty string)
	// does not present any risk.
	host, _ := os.Hostname()

	p := proj{
		Share:    false,
		Hostname: host,
		Driver: map[string]string{
			"name":    "Jacamar CI",
			"version": version.Version(),
		},
	}

	f.updateDirs(&p, auth)

	state, err := f.buildState(auth)
	if err != nil {
		return "", err
	}

	// Configuration (managed outside StatefulEnv structure)
	state[configure.EnvVariable] = f.AbsExec.ExecOptions
	p.State = state

	data, err := json.Marshal(&p)

	return string(data), err
}

// buildState generates a mapping of values to StatefulEnv keys.
func (f Factory) buildState(auth authuser.Authorized) (map[string]string, error) {
	state := auth.BuildState()
	f.AbsExec.Env.JobResponse.ExpandState(&state)

	env := make(map[string]string)

	t := reflect.TypeOf(&state).Elem()
	v := reflect.ValueOf(&state).Elem()
	for i := 0; i < t.NumField(); i++ {
		fieldT := t.Field(i)
		fieldV := v.Field(i)

		tag := fieldT.Tag.Get(envparser.UserVarTagName)
		req := fieldT.Tag.Get(envparser.RequiredKey)

		if req == "true" && fieldV.String() == "" {
			return env, fmt.Errorf("unable to identify required stateful variable %s", tag)
		} else if req == "false" && fieldV.String() == "" {
			continue
		}

		env[tag] = fieldV.String()
	}

	return env, nil
}

func (f Factory) updateDirs(p *proj, auth authuser.Authorized) {
	p.Builds = auth.CIUser().BuildsDir
	p.Cache = auth.CIUser().CacheDir
}
