package jacamar

import (
	"context"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type jacamarTests struct {
	c         arguments.ConcreteArgs
	opt       configure.Auth
	s         string
	targetEnv map[string]string

	cfg *mock_configure.MockConfigurer
	msg *mock_logging.MockMessenger

	assertAbstract func(*testing.T, *abstracts.Executor)
	assertError    func(*testing.T, error)
	assertGeneric  func(*testing.T)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}
	exitZero = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":       "1",
		"TRUSTED_CI_JOB_ID":                      "123",
		"TRUSTED_CI_JOB_TOKEN":                   "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":          "abc",
		"TRUSTED_CI_BUILDS_DIR":                  "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                   "/gitlab/cache",
		envkeys.UserEnvPrefix + envkeys.CIJobJWT: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                  "gitlab.url",
	}
}

func Test_establishAbstract(t *testing.T) {
	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr(gomock.Any(), gomock.Any()).AnyTimes()

	tests := map[string]jacamarTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			prep, _ := establishAbstract(cancelCtx, tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, prep.AbsExec)
			}
		})
	}
}

func Test_newExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// reinforce the expectations configurations equate to the runner requirements.
	none := mock_configure.NewMockConfigurer(ctrl)
	none.EXPECT().General().Return(
		configure.General{
			Executor: "",
		},
	).Times(1)

	slurm := mock_configure.NewMockConfigurer(ctrl)
	slurm.EXPECT().General().Return(
		configure.General{
			Executor: "slurm",
		},
	).Times(1)

	lsf := mock_configure.NewMockConfigurer(ctrl)
	lsf.EXPECT().General().Return(
		configure.General{
			Executor: "lsf",
		},
	).Times(1)

	cobalt := mock_configure.NewMockConfigurer(ctrl)
	cobalt.EXPECT().General().Return(
		configure.General{
			Executor: "cobalt",
		},
	).Times(1)

	shell := mock_configure.NewMockConfigurer(ctrl)
	shell.EXPECT().General().Return(
		configure.General{
			Executor: "shell",
		},
	).Times(1)

	pbs := mock_configure.NewMockConfigurer(ctrl)
	pbs.EXPECT().General().Return(
		configure.General{
			Executor: "pbs",
		},
	).Times(1)

	tests := map[string]jacamarTests{
		"no executor defined": {
			cfg: none,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unrecognized executor type defined in configuration",
				)
			},
		},
		"slurm executor defined": {
			cfg: slurm,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"lsf executor defined": {
			cfg: lsf,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"cobalt executor defined": {
			cfg: cobalt,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"shell executor defined": {
			cfg: shell,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"pbs executor defined": {
			cfg: pbs,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ae := &abstracts.Executor{
				Cfg: tt.cfg,
			}

			_, err := newExecutor(ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_prepareExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	jobMsg := mock_logging.NewMockMessenger(ctrl)
	jobMsg.EXPECT().Notify("This is a job message!")

	jobCfg := mock_configure.NewMockConfigurer(ctrl)
	jobCfg.EXPECT().General().Return(configure.General{
		JobMessage: "This is a job message!",
	}).AnyTimes()

	sysExit = func() { return }

	tests := map[string]struct {
		ae *abstracts.Executor
		c  arguments.ConcreteArgs
	}{
		"job message configured": {
			ae: &abstracts.Executor{
				Cfg: jobCfg,
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/base",
						BuildsDir: t.TempDir() + "/builds",
						CacheDir:  t.TempDir() + "/cache",
						ScriptDir: t.TempDir() + "/script",
					},
				},
				Msg: jobMsg,
			},
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			prepareExec(tt.ae, tt.c)
		})
	}
}

func Test_mngExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	buildExit = func() {}
	sysExit = func() {}

	t.Run("unsupported concrete arguments", func(t *testing.T) {
		prep := preparations.Factory{
			Args: arguments.ConcreteArgs{},
			AbsExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Eq("Invalid subcommand - See $ jacamar --help")).Times(1)
					return m
				}(),
			},
		}

		mngExec(nil, prep)
	})
}

func TestStart(t *testing.T) {
	t.Run("jacamar signal test", func(t *testing.T) {
		cmd := exec.Command("sleep", "30")
		err := cmd.Start()
		if err != nil {
			assert.NoError(t, err, "error starting sleep command")
			return
		}

		Start(arguments.ConcreteArgs{
			Signal: &arguments.SignalCmd{
				Signal: "SIGKILL",
				PID:    strconv.Itoa(cmd.Process.Pid),
			},
		})
	})

	_, found := os.LookupEnv("CI_JOB_JWT")
	if !found {
		// Test more comprehensive shell based workflow against functional CI environment.
		// Without access to a GitLab server and related CI predefined variable testing
		// via Pavilion would be required for similar results.
		t.Skip("TestStart only written for CI environment")
	}

	usr, _ := user.Current()

	cmd := exec.Command(
		"go",
		"run",
		os.ExpandEnv("${CI_PROJECT_DIR}/tools/config2base64/config2base64.go"),
		os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/shell_no_setuid.toml"),
	)
	b, err := cmd.CombinedOutput()
	if err != nil {
		assert.NoError(t, err, "filed to prepare configuration file: "+string(b))
		return
	}

	targetEnv := map[string]string{
		"JACAMAR_CI_CONFIG_STR":     string(b),
		"JACAMAR_CI_BASE_DIR":       "/tmp/" + usr.Username,
		"JACAMAR_CI_BUILDS_DIR":     "/tmp/" + usr.Username + "/builds",
		"JACAMAR_CI_CACHE_DIR":      "/tmp/" + usr.Username + "/cache",
		"JACAMAR_CI_SCRIPT_DIR":     "/tmp/" + usr.Username + "/script",
		"JACAMAR_CI_AUTH_USERNAME":  usr.Username,
		"JACAMAR_CI_RUNNER_TIMEOUT": "5m",
	}

	tests := []jacamarTests{
		{
			s:         "prepare working directories",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth:  true,
				Prepare: &arguments.PrepareCmd{},
			},
			assertGeneric: func(t *testing.T) {
				_, err := os.Stat("/tmp/" + usr.Username)
				assert.NoError(t, err, "JACAMAR_CI_BASE_DIR not found")
			},
		},
		{
			s:         "step_script successfully executed",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Run: &arguments.RunCmd{
					Stage:  "step_script",
					Script: "../../../test/testdata/job-scripts/touch_file.bash",
				},
			},
			assertGeneric: func(t *testing.T) {
				assert.FileExists(t, "/tmp/example-file")
			},
		},
		{
			s:         "cleanup working directories",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Cleanup: &arguments.CleanupCmd{
					Configuration: os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/shell_no_setuid.toml"),
				},
			},
		},
		{
			s:         "cleanup with invalid downscope",
			targetEnv: targetEnv,
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Cleanup: &arguments.CleanupCmd{
					Configuration: os.ExpandEnv("${CI_PROJECT_DIR}/test/testdata/configs/invalid_auth.toml"),
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			for _, v := range os.Environ() {
				if strings.HasPrefix(v, "CI_") || strings.HasPrefix(v, "GITLAB_") {
					loc := strings.Index(v, "=")
					key := v[:loc]
					val := v[loc+1:]
					t.Setenv("CUSTOM_ENV_"+key, val)
				}
			}

			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			Start(tt.c)

			if tt.assertGeneric != nil {
				tt.assertGeneric(t)
			}
		})
	}
}

func Test_cleanupExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	sysExit = func() {}

	tests := map[string]struct {
		ae            *abstracts.Executor
		c             arguments.ConcreteArgs
		prepareDirs   func(*testing.T, *abstracts.Executor)
		assertAbsExec func(*testing.T, *abstracts.Executor)
	}{
		"standard cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/standard",
						BuildsDir: t.TempDir() + "/standard/builds",
						CacheDir:  t.TempDir() + "/standard/cache",
						ScriptDir: t.TempDir() + "/standard/script",
					},
				},
			},
			prepareDirs: func(t *testing.T, ae *abstracts.Executor) {
				_ = usertools.CreateDirectories(ae.Env.StatefulEnv, ae.Cfg.General())
			},
		},
		"invalid cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir: t.TempDir(),
					},
				},
				Msg: func() logging.Messenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Any())
					return m
				}(),
			},
		},
		"static builds cleanup directories": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			ae: &abstracts.Executor{
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{
						StaticBuildsDir: true,
						StaticMinDays:   -1,
					}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/builds",
						BuildsDir: t.TempDir() + "/builds/static",
						CacheDir:  t.TempDir() + "/builds/cache",
						ScriptDir: t.TempDir() + "/builds/script",
					},
				},
			},
			prepareDirs: func(t *testing.T, ae *abstracts.Executor) {
				_ = usertools.CreateDirectories(ae.Env.StatefulEnv, ae.Cfg.General())
				_ = os.Mkdir(ae.Env.StatefulEnv.BuildsDir+"/123", 0700)
			},
			assertAbsExec: func(t *testing.T, ae *abstracts.Executor) {
				_, err := os.ReadDir(ae.Env.BuildsDir + "/1234")
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepareDirs != nil {
				tt.prepareDirs(t, tt.ae)
			}

			cleanupExec(tt.ae, tt.c)

			if tt.assertAbsExec != nil {
				tt.assertAbsExec(t, tt.ae)
			}
		})
	}
}
