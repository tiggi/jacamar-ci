package jacamar

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/cobalt"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/flux"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/lsf"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/pbs"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/shell"
	"gitlab.com/ecp-ci/jacamar-ci/internal/executors/slurm"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	// Function to facilitate associated process exit.
	sysExit, buildExit, exitZero func()
)

// Start initializes the 'jacamar' application process, requires valid command arguments and
// custom executor provided context to be present.
func Start(c arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(msg)

	switch {
	case c.Signal != nil:
		usertools.ManageSignal(msg, *c.Signal, sysExit, exitZero)
		return
	case c.Cleanup != nil:
		sysExit = exitZero
	}

	prep, auth := establishAbstract(ctx, c, msg)
	mngExec(auth, prep)
}

func establishAbstract(
	ctx context.Context,
	c arguments.ConcreteArgs,
	msg logging.Messenger,
) (prep preparations.Factory, auth authuser.Authorized) {
	ae, syslog, err := preparations.JobContext(c, sysExit)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return
	}
	ae.Msg = msg

	prep.AbsExec = ae
	prep.Args = c
	prep.SysLog = syslog

	auth, err = prep.NewAuthorizedUser()
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return
	}

	cmdr, err := prep.NewCommander(ctx, auth)
	if err != nil {
		preparations.StdError(c, err.Error(), msg, sysExit)
		return
	}
	ae.Runner = basic.NewMechanism(cmdr)

	return
}

func newExecutor(ae *abstracts.Executor) (executors.Executor, error) {
	tar := (ae.Cfg.General()).Executor
	switch strings.TrimSpace(strings.ToLower(tar)) {
	case "shell":
		return shell.NewExecutor(ae), nil
	case "cobalt", "qsub":
		return cobalt.NewExecutor(ae)
	case "lsf", "bsub":
		return lsf.NewExecutor(ae)
	case "pbs":
		return pbs.NewExecutor(ae)
	case "slurm", "sbatch":
		return slurm.NewExecutor(ae)
	case "flux":
		return flux.NewExecutor(ae)
	default:
		return nil, errors.New("unrecognized executor type defined in configuration")
	}
}

func mngExec(auth authuser.Authorized, prep preparations.Factory) {
	switch {
	case prep.Args.Config != nil:
		prep.ConfigExec(auth, sysExit)
	case prep.Args.Prepare != nil:
		prepareExec(prep.AbsExec, prep.Args)
	case prep.Args.Run != nil:
		exec, err := newExecutor(prep.AbsExec)
		if err != nil {
			preparations.StdError(prep.Args, err.Error(), prep.AbsExec.Msg, sysExit)
		}

		prep.AbsExec.Runner.CommandDir(auth.CIUser().ScriptDir)
		runExec(prep.AbsExec, auth, exec, prep.Args)
	case prep.Args.Cleanup != nil:
		cleanupExec(prep.AbsExec, prep.Args)
	default:
		preparations.StdError(
			prep.Args,
			"Invalid subcommand - See $ jacamar --help",
			prep.AbsExec.Msg,
			sysExit,
		)
	}
}

func prepareExec(ae *abstracts.Executor, args arguments.ConcreteArgs) {
	jobMessage := ae.Cfg.General().JobMessage
	if jobMessage != "" {
		ae.Msg.Notify(jobMessage)
	}

	if err := executors.Prepare(ae); err != nil {
		preparations.StdError(args, err.Error(), ae.Msg, sysExit)
	}
}

func runExec(
	ae *abstracts.Executor,
	auth authuser.Authorized,
	exec executors.Executor,
	c arguments.ConcreteArgs,
) {
	if err := executors.Run(ae, exec, auth); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, buildExit)
	}
}

func cleanupExec(ae *abstracts.Executor, c arguments.ConcreteArgs) {
	if err := usertools.PreCleanupVerification(ae.Env.StatefulEnv, ae.Cfg.General()); err != nil {
		preparations.StdError(c, err.Error(), ae.Msg, sysExit)
	}

	executors.Cleanup(ae)

	staticDirCleanup(ae.Cfg.General(), ae.Env.StatefulEnv.BaseDir)
}

func staticDirCleanup(gen configure.General, baseDir string) {
	if !gen.StaticBuildsDir || gen.StaticMinDays == 0 {
		// Account for potential configurations or invalid state that would allow for us
		// to skip the static directory cleanup process.
		return
	}

	// As we've already verified the folder structure/permissions in an earlier
	// step any error encountered here is unlikely but more important an indication
	// that the files no longer exists and thus attempting to notify the user
	// would be unnecessary.
	folders, _ := os.ReadDir(baseDir + "/static")

	now := time.Now()
	dur := time.Duration(gen.StaticMinDays*24) * time.Hour

	for _, f := range folders {
		folderInfo, _ := f.Info()
		if folderInfo.ModTime().Add(dur).Before(now) {
			_ = os.RemoveAll(baseDir + "/static/" + folderInfo.Name())
		}
	}
}

func exitPanic(msg logging.Messenger) {
	if r := recover(); r != nil {
		msg.Error(fmt.Sprintf("Unexpected error encountered: %v", r))
		sysExit()
	}
}

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()

	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
	exitZero = func() { os.Exit(0) }
}
