// Package auth realizes the core of the jacamar-auth application by ensuring the strict
// adherence to the authorization flow and the validity of the subsequent user context used
// to execute the configured downscope mechanism.
package auth

import (
	"context"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms/basic"
	"gitlab.com/ecp-ci/jacamar-ci/internal/seccomp"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	// Minimum functional runner version required.
	minMajor = 14
	minMinor = 1
)

var (
	// Functions to facilitate associated process exit.
	sysExit, buildExit func()
)

// Start initializes the jacamar-auth application process, requires valid command arguments and
// custom executor provided job context.
func Start(c arguments.ConcreteArgs) {
	ctx := context.Background()
	msg := logging.NewMessenger()

	defer exitPanic(c, msg)

	verifycaps.CurrentExec(c, msg, sysExit)
	stagePrep(c, msg)

	auth, prep, err := establishAbstract(ctx, c, msg)
	if err != nil {
		// All errors encountered here will be considered system errors, even if they are
		// potentially caused by a user influenced settings.
		errorhandling.MessageError(c, msg, err)
		sysExit()
		return
	}

	systemPrep(auth, prep, c)
	mngExec(auth, prep)
}

func stagePrep(c arguments.ConcreteArgs, msg logging.Messenger) {
	switch {
	case c.Config != nil:
		// Runner version validation offers us the ability to provide a better actionable
		// error message that would otherwise rely upon missing environment variables.
		if !envparser.ValidRunnerVersion(minMajor, minMinor) {
			msg.Stderr(fmt.Sprintf(
				"Invalid runner version detected, minimum supported: %d.%d",
				minMajor,
				minMinor,
			))
		}
	case c.Cleanup != nil:
		// Only fail cleanup during explicit cleanup errors, not
		// due to a repeat of an error from config_exec.
		sysExit = func() { os.Exit(0) }
	}
}

func establishAbstract(
	ctx context.Context,
	c arguments.ConcreteArgs,
	msg logging.Messenger,
) (
	auth authuser.Authorized,
	prep preparations.Factory,
	err error,
) {
	ae, syslog, err := preparations.JobContext(c, sysExit)
	if err != nil {
		// Jacamar system logger likely not available at this point, we will need to
		// rely on runner's built in logging.
		err = errorhandling.NewJobCtxError(err)
		return
	}
	ae.Msg = msg

	prep.AbsExec = ae
	prep.Args = c
	prep.SysLog = syslog

	auth, err = prep.NewAuthorizedUser()
	if err != nil {
		// Logging managed by authuser package.
		err = errorhandling.NewAuthError(err)
		return
	}

	cmdr, err := prep.NewCommander(ctx, auth)
	if err != nil {
		syslog.Error("unable to build runner/commander interface: ", err.Error())
		err = errorhandling.NewRunMechanismError(err)
		return
	}
	ae.Runner = basic.NewMechanism(cmdr)

	return auth, prep, err
}

func mngExec(auth authuser.Authorized, prep preparations.Factory) {
	switch {
	case prep.Args.Config != nil:
		if prep.AbsExec.Cfg.Auth().DevMode {
			prep.SysLog.Info("application running in auth.dev_mode")
		}
		verifycaps.IsNoNewPrivs(prep.SysLog)
		prep.ConfigExec(auth, sysExit)
	case prep.Args.Prepare != nil:
		prepareExec(prep.AbsExec, auth, prep.SysLog, prep.Args)
	case prep.Args.Run != nil:
		runExec(prep.AbsExec, prep.SysLog, prep.Args)
	case prep.Args.Cleanup != nil:
		cleanupExec(prep.AbsExec, prep.SysLog, prep.Args)
	default:
		// Catch all in case additional subcommands are ever supported for jacamar-auth.
		prep.SysLog.Error("invalid jacamar-auth subcommand encountered: ", prep.Args)
		preparations.StdError(
			prep.Args,
			"Invalid subcommand - See $ jacamar-auth --help",
			prep.AbsExec.Msg,
			sysExit,
		)
	}
}

func systemPrep(auth authuser.Authorized, prep preparations.Factory, c arguments.ConcreteArgs) {
	err := seccomp.Factory{
		Stage:  prep.AbsExec.Stage,
		Opt:    prep.AbsExec.Cfg.Options(),
		Usr:    auth.CIUser(),
		SysLog: prep.SysLog,
	}.Establish()
	if err != nil {
		prep.SysLog.Error("unable to establish seccomp filter: ", err.Error())
		preparations.StdError(c, err.Error(), prep.AbsExec.Msg, sysExit)
	}

	if prep.AbsExec.Cfg.Options().Auth.NoNewPrivs {
		err = seccomp.SetNoNewPrivs()
		if err != nil {
			prep.SysLog.Error("unable to establish set no_new_privs: ", err.Error())
			preparations.StdError(c, err.Error(), prep.AbsExec.Msg, sysExit)
		}
	}
}

func prepareExec(
	ae *abstracts.Executor,
	auth authuser.Authorized,
	syslog *logrus.Entry,
	c arguments.ConcreteArgs,
) {
	if (ae.Cfg.Auth()).RootDirCreation {
		if err := rootDirManagement(auth); err != nil {
			err = errorhandling.NewAuthError(fmt.Errorf("error managaing base directory: %w", err))
			syslog.Error(err)
			errorhandling.MessageError(c, ae.Msg, err)
			sysExit()
		}
	}

	if err := ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing prepare_exec: %s", err.Error())
		syslog.Error(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		sysExit()
	}
}

func runExec(ae *abstracts.Executor, syslog *logrus.Entry, c arguments.ConcreteArgs) {
	err := ae.Runner.TransferScript(c.Run.Script, c.Run.Stage, ae.Env, ae.Cfg.Options())
	if err != nil {
		errMsg := fmt.Sprintf("Failed to transfer job script (%s): %s", c.Run.Stage, err.Error())
		syslog.Warning(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, sysExit)
	}

	if err = ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing run_exec: %s", err.Error())
		syslog.Debug(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		buildExit()
	}
}

func cleanupExec(ae *abstracts.Executor, syslog *logrus.Entry, c arguments.ConcreteArgs) {
	cleanupCfgCheck(syslog, ae.Msg, c.Cleanup.Configuration)

	if err := ae.Runner.PipeOutput(""); err != nil {
		errMsg := fmt.Sprintf("Error executing cleanup_exec: %s", err.Error())
		syslog.Debug(errMsg)
		preparations.StdError(c, errMsg, ae.Msg, buildExit)
		sysExit()
	}
}

// rootDirManagement creates/manages the root directory for an authorization user during
// the privileged authorization process.
func rootDirManagement(auth authuser.Authorized) error {
	usrCtx := auth.CIUser()
	info, err := os.Stat(usrCtx.BaseDir)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if os.IsNotExist(err) {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Mkdir(usrCtx.BaseDir, 0700); err != nil {
			return err
		}
	} else if info.Mode().String() != "drwx------" {
		/* #nosec */
		// directory 700 permissions required
		if err = os.Chmod(usrCtx.BaseDir, 0700); err != nil {
			return err
		}
	}

	// enforce permissions
	return os.Chown(
		usrCtx.BaseDir,
		usrCtx.UID,
		usrCtx.GID,
	)
}

func exitPanic(c arguments.ConcreteArgs, msg logging.Messenger) {
	if r := recover(); r != nil {
		err := fmt.Errorf("panic encountered: %v", r)
		errorhandling.MessageError(c, msg, errorhandling.NewPanicError(err))
		sysExit()
	}
}

// cleanupCfgCheck supports additional verification of the supplied configuration.
func cleanupCfgCheck(logger *logrus.Entry, msg logging.Messenger, file string) {
	meta, _, err := configure.MetaOptions(file)
	if err != nil {
		// We can ignore errors as they are captured and shared elsewhere.
		return
	}

	// warn/log of unrecognized keys
	if len(meta.Undecoded()) > 0 {
		m := fmt.Sprintf(
			"unrecognized key(s) detected in configuration (%s)",
			meta.Undecoded(),
		)
		msg.Stderr(m)
		logger.Warn(m)
	}
}

func init() {
	// Initialize exit status in order to facilitate potential failures at any stage.
	sys, build := envparser.ExitCodes()

	sysExit = func() { os.Exit(sys) }
	buildExit = func() { os.Exit(build) }
}
