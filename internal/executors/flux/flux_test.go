package flux

import (
	"errors"
	"os"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput("/working/after_script.bash").Return(nil)
	run.EXPECT().JobScriptOutput("/failing/get_sources.bash").Return(errors.New("error message"))

	jobScript := `#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running a test..."\n'
exit 0
`

	tmpJobScript := t.TempDir() + "/job.bash"
	_ = os.WriteFile(tmpJobScript, []byte(jobScript), 0700)

	jobCfg := mock_configure.NewMockConfigurer(ctrl)
	jobCfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "42s",
	}).AnyTimes()

	jobRun := mock_runmechanisms.NewMockRunner(ctrl)
	jobRun.EXPECT().JobScriptOutput(gomock.Any(), gomock.Any()).
		DoAndReturn(func(script interface{}, stdin ...interface{}) error {
			time.Sleep(1 * time.Second)
			return nil
		})
	jobRun.EXPECT().RequestContext().Return(nil).AnyTimes()

	jobMng := mock_batch.NewMockManager(ctrl)
	jobMng.EXPECT().BatchCmd(gomock.Any()).Return("flux mini alloc")
	jobMng.EXPECT().UserArgs().Return("args")
	jobMng.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), "$(flux jobs --format {id} -n --name ci-test_job)", gomock.Any()).Return()
	jobMng.EXPECT().NFSTimeout(gomock.Eq("42s"), gomock.Any()).AnyTimes()

	jobMsg := mock_logging.NewMockMessenger(ctrl)

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"working after_script launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					ScriptPath: "/working/after_script.bash",
					Stage:      "after_script",
				},
			},
			assertError: tst.AssertNoError,
		},
		"failing get_sources launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					ScriptPath: "/failing/get_sources.bash",
					Stage:      "get_sources",
				},
			},
			assertError: tst.AssertError,
		},
		"non-existent script path defined": {
			fields: fields{
				absExec: &abstracts.Executor{
					ScriptPath: "/file/does/not/exist.bash",
					Stage:      "step_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated Flux job script: unexpect script detected, only runner generated currently supported")
			},
		},
		"successfully run job": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg:        jobCfg,
					Runner:     jobRun,
					Msg:        jobMsg,
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng: jobMng,
			},
			assertError: tst.AssertNoError,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
				jobName: "ci-test_job",
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tempDir := t.TempDir()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	binCfg := mock_configure.NewMockConfigurer(ctrl)
	binCfg.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin:     "/ci/test/bin",
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	allowCfg := mock_configure.NewMockConfigurer(ctrl)
	allowCfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: true,
	}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS").Times(2)

	illegalMsg := mock_logging.NewMockMessenger(ctrl)
	illegalMsg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: --job-name"),
	).Times(2)

	tests := map[string]struct {
		ae             *abstracts.Executor
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, *executor)
	}{
		// We don't need to test for many failure states as we can
		// rely on that the data provided has already been parsed.
		"basic Flux executor created for non step_script stage": {
			ae: &abstracts.Executor{
				Stage: "after_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, e.absExec.Stage, "after_script")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Flux executor for step_script": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.NotNil(t, e.mng)
				assert.Contains(t, e.jobName, "ci-101_")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"scheduler bin observed": {
			ae: &abstracts.Executor{
				Cfg: binCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, "/ci/test/bin/flux mini alloc", e.mng.BatchCmd(""))
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--job-name custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Nil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"illegal argument allowed": {
			ae: &abstracts.Executor{
				Cfg: allowCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "--job-name custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
