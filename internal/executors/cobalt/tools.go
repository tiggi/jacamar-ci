package cobalt

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

// qsubJobID parses the output from a successful qsub command in order
// to identify the jobID. Verifies jobID by confirming integer conversion.
func qsubJobID(out string) (string, error) {
	out = strings.TrimSpace(out)
	lines := strings.Split(out, "\n")
	id := lines[len(lines)-1]
	if _, err := strconv.Atoi(id); err != nil {
		return "", err
	}

	return id, nil
}

// exitStatus checks the (jobID).cobaltlog file for a string associated with a
// successful job. Failed jobs or if no status can be identified will return an error.
func (e *executor) exitStatus(log string) error {
	if !e.absExec.Cfg.Batch().SkipCobaltLog {
		file, err := os.Open(filepath.Clean(log))
		if err != nil {
			return fmt.Errorf("unable to open CobaltLog: %w", err)
		}

		/* #nosec */
		// file is not written too
		defer func() { _ = file.Close() }()

		sc := bufio.NewScanner(file)
		for sc.Scan() {
			line := sc.Text()
			if strings.Contains(line, "task completed normally with an exit code of 0") {
				return nil
			}
		}
		if err = sc.Err(); err != nil {
			return fmt.Errorf("scanner error: %w", err)
		}
	}

	if err := e.backupExitCheck(); err == nil {
		return nil
	}

	return errors.New("unable to find successful exit code in CobaltLog")
}

func (e *executor) backupExitCheck() (err error) {
	file, err := os.Open(filepath.Clean(e.outLog))
	if err != nil {
		return fmt.Errorf("unable to open CobaltOutput: %w", err)
	}

	sc := bufio.NewScanner(file)
	for sc.Scan() {
		line := sc.Text()
		if strings.Contains(line, e.completeMsg) {
			return nil
		}
	}

	return errors.New("unable find completed message in CobaltOutput")
}

func summarizeErrors(log, id string, msg logging.Messenger) {
	if envparser.TrueEnvVar(summaryVar) {
		log, _ = filepath.Abs(log)
		/* #nosec */
		// variable file path required
		data, err := os.ReadFile(log)
		if err != nil {
			msg.Warn(strings.Join([]string{"Unable to read", log, "for error summary", err.Error()}, " "))
		}

		if len(data) != 0 {
			msg.Notify(strings.Join([]string{"Cobalt job (", id, ") error summary:"}, " "))
			msg.Stderr(string(data))
		}
	}
}
