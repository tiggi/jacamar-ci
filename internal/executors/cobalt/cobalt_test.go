package cobalt

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

// Testing the entirety of Cobalt requires extensive mocking, we'll
// explore better ways to verify interactions via integration testing.
// Tests here should focus on ensuring code will run without major
// errors in a reasonably expected way.

type cobaltTests struct {
	files  []string
	log    string
	outLog string
	errLog string
	env    envparser.ExecutorEnv

	ae  *abstracts.Executor
	cfg *mock_configure.MockConfigurer
	mng *mock_batch.MockManager
	run *mock_runmechanisms.MockRunner
	msg *mock_logging.MockMessenger

	prepare func(t *testing.T, s string)

	assertError    func(t *testing.T, err error)
	assertExecutor func(*testing.T, *executor)
	assertSlice    func(t *testing.T, s []string)
}

func mockCfgAny(ctrl *gomock.Controller, opt configure.Options) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Options().Return(opt).AnyTimes()
	m.EXPECT().Auth().Return(opt.Auth).AnyTimes()
	m.EXPECT().Batch().Return(opt.Batch).AnyTimes()
	m.EXPECT().General().Return(opt.General).AnyTimes()

	return m
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	binCfg := mock_configure.NewMockConfigurer(ctrl)
	binCfg.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin: "/ci/test/bin",
	}).AnyTimes()

	badDelay := mock_configure.NewMockConfigurer(ctrl)
	badDelay.EXPECT().Batch().Return(configure.Batch{
		CommandDelay: "10z",
	}).AnyTimes()

	validDelay := mock_configure.NewMockConfigurer(ctrl)
	validDelay.EXPECT().Batch().Return(configure.Batch{
		CommandDelay: "5s",
	}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS").AnyTimes()
	msg.EXPECT().Warn("Invalid command_delay specified by runner configuration: %s", gomock.Any()).AnyTimes()

	illegalMsg := mock_logging.NewMockMessenger(ctrl)
	illegalMsg.EXPECT().Warn("Illegal argument detected. Please remove: -o").Times(1)

	tests := map[string]cobaltTests{
		"non-step_script, minimal executor scope": {
			ae: &abstracts.Executor{
				Stage: "get_sources",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Equal(t, *e.absExec, abstracts.Executor{
					Stage: "get_sources",
				})
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"step_script batch executor prepared": {
			ae: &abstracts.Executor{
				Cfg:   mockCfgAny(ctrl, configure.Options{}),
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, "qsub", e.mng.BatchCmd(""))
				// default command_delay when none configured.
				assert.Equal(t, e.sleepTime, 30*time.Second)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid command_delay": {
			ae: &abstracts.Executor{
				Cfg:   badDelay,
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, e.sleepTime, 30*time.Second)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"valid command_delay": {
			ae: &abstracts.Executor{
				Cfg:   validDelay,
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, e.sleepTime, 5*time.Second)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"verify completeMsg": {
			ae: &abstracts.Executor{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "42",
					},
				},
				Cfg:   validDelay,
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, e.completeMsg, "Cobalt CI Job 42 completed.")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"step_script batch executor prepared with configured bin": {
			ae: &abstracts.Executor{
				Cfg:   binCfg,
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e.mng)
				assert.Equal(t, "/ci/test/bin/qsub", e.mng.BatchCmd(""))
				// default command_delay when none configured.
				assert.Equal(t, e.sleepTime, 30*time.Second)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal arguments error observed": {
			ae: &abstracts.Executor{
				Cfg:   binCfg,
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T, s string) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "-o custom.output")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t, "")
			}
			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	bashRun := mock_runmechanisms.NewMockRunner(ctrl)
	bashRun.EXPECT().JobScriptOutput("/dir/after_script.bash").Return(nil)

	modifyCmd := mock_runmechanisms.NewMockRunner(ctrl)
	modifyCmd.EXPECT().ModifyCmd(command.IdentifyShell(configure.General{
		Shell: "/bin/bash",
	}))

	type fields struct {
		absExec *abstracts.Executor
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"non-step_script, minimal executor scope": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     bashRun,
					Stage:      "after_script",
					ScriptPath: "/dir/after_script.bash",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"command modified with error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Env: envparser.ExecutorEnv{
						RequiredEnv: envparser.RequiredEnv{
							JobID: "123",
						},
					},
					Cfg: mockCfgAny(ctrl, configure.Options{
						General: configure.General{
							Shell: "/bin/bash",
						},
					}),
					Runner:     modifyCmd,
					Stage:      "step_script",
					ScriptPath: "/dir/job_script.bash",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated Cobalt job script: unexpect script detected, only runner generated currently supported")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_runCobalt(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Batch mocks
	anyBatch := mock_batch.NewMockManager(ctrl)
	anyBatch.EXPECT().BatchCmd("--args").Return("qsub --args").AnyTimes()
	anyBatch.EXPECT().UserArgs().Return("--args").AnyTimes()
	anyBatch.EXPECT().StateCmd().Return("qstat").AnyTimes()
	anyBatch.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).AnyTimes()
	anyBatch.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes()

	tailErrMng := anyBatch
	tailErrMng.EXPECT().TailFiles(
		gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(),
	).Return(errors.New("error message")).AnyTimes()

	// Run mocks
	failedRun := mock_runmechanisms.NewMockRunner(ctrl)
	failedRun.EXPECT().JobScriptReturn("/job/script.bash", "qsub --args").Return(
		"Failed submission", errors.New("error message")).AnyTimes()

	noJobID := mock_runmechanisms.NewMockRunner(ctrl)
	noJobID.EXPECT().JobScriptReturn("/job/script.bash", "qsub --args").Return("abc", nil).AnyTimes()

	successRun := mock_runmechanisms.NewMockRunner(ctrl)
	successRun.EXPECT().JobScriptReturn("/job/script.bash", "qsub --args").Return("123", nil).AnyTimes()
	successRun.EXPECT().RequestContext().Return(nil).AnyTimes()
	successRun.EXPECT().ReturnOutput("qstat 123").Return("", errors.New("error message")).AnyTimes()

	// Msg mocks
	tailErrMsg := mock_logging.NewMockMessenger(ctrl)
	tailErrMsg.EXPECT().Warn("Unable to monitor files, job output distrusted: %s", "error message").AnyTimes()

	tests := map[string]cobaltTests{
		"failed job submission": {
			run: failedRun,
			mng: anyBatch,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error while attempting to submit job to Cobalt: failed to request allocation (qsub --args): Failed submission")
			},
		},
		"failed to identify jobID": {
			run: noJobID,
			mng: anyBatch,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error while attempting to submit job to Cobalt: failed to identify jobid from qsub stdout: abc")
			},
		},
		"error encountered attempting to tail files": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: t.TempDir(),
				},
			},
			cfg: mockCfgAny(ctrl, configure.Options{}),
			run: successRun,
			mng: tailErrMng,
			msg: tailErrMsg,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cobalt job (123) encountered an error during execution")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: &abstracts.Executor{
					Env:        tt.env,
					Cfg:        tt.cfg,
					Msg:        tt.msg,
					Runner:     tt.run,
					ScriptPath: "/job/script.bash",
				},
				mng:         tt.mng,
				completeMsg: "Cobalt CI Job 123 completed.",
				sleepTime:   1 * time.Second,
			}

			err := e.runCobalt()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
