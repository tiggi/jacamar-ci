package pbs

import (
	"context"
	"errors"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

type pbsTests struct {
	e         *executor
	ae        *abstracts.Executor
	targetEnv map[string]string

	assertError func(*testing.T, error)
	assertExec  func(*testing.T, *executor)
}

func mockStateful(tempDir string) envparser.ExecutorEnv {
	usr, _ := user.Current()

	return envparser.ExecutorEnv{
		StatefulEnv: envparser.StatefulEnv{
			Username:  usr.Username,
			BaseDir:   tempDir + "/" + usr.Username,
			BuildsDir: tempDir + "/" + usr.Username + "/builds",
			ScriptDir: tempDir + "/" + usr.Username + "/script",
			CacheDir:  tempDir + "/" + usr.Username + "/cache",
		},
	}
}

// We should mainly rely on Pavilion + Facility/Container to realize more comprehensive
// testing against PBS. Use these tests to ensure we properly handle errors, goroutines,
// and channel communications where possible.

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.TODO()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Notify("Submitted batch job %s", "0.hostname")
	msg.EXPECT().Notify("The PBS executor for Jacamar CI is currently under active developments, changes will occur.").AnyTimes()
	msg.EXPECT().Stdout(gomock.Any(), gomock.Any()).AnyTimes()
	msg.EXPECT().Warn(gomock.Any(), gomock.Any()).AnyTimes()

	qsub := mock_batch.NewMockManager(ctrl)
	qsub.EXPECT().StateCmd().Return("qstat").AnyTimes()
	qsub.EXPECT().StopCmd().Return("qdel").AnyTimes()
	qsub.EXPECT().UserArgs().Return("--args").AnyTimes()
	qsub.EXPECT().BatchCmd(gomock.Any()).Return("qsub --args").AnyTimes()
	qsub.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	qsub.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).AnyTimes()
	qsub.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), gomock.Eq("0.hostname"), gomock.Any()).Return().AnyTimes()

	passScript := tst.WriteTmpFile(t, "pass_script", `#!/bin/bash

job script contents ...
exit 0
`)

	tests := map[string]pbsTests{
		"successful get_sources": {
			e: &executor{
				absExec: &abstracts.Executor{
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/example/script.bash").Return(nil)
						return m
					}(),
					ScriptPath: "/example/script.bash",
					Stage:      "get_sources",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"failed after_script": {
			e: &executor{
				absExec: &abstracts.Executor{
					Msg: msg,
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/example/script.bash").Return(errors.New("error message"))
						return m
					}(),
					ScriptPath: "/example/script.bash",
					Stage:      "after_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"step_script missing script": {
			e: &executor{
				absExec: &abstracts.Executor{
					Env:        mockStateful(t.TempDir()),
					Msg:        msg,
					ScriptPath: t.TempDir() + "/missing.file",
					Stage:      "step_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated PBS job script: unexpect script detected, only runner generated currently supported")
			},
		},
		"functional step_script": {
			e: &executor{
				absExec: &abstracts.Executor{
					Env:        mockStateful(t.TempDir()),
					Msg:        msg,
					ScriptPath: passScript,
					Stage:      "step_script",
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptReturn(gomock.Any(), gomock.Eq("qsub --args")).Return("0.hostname", nil)
						m.EXPECT().ReturnOutput("qstat 0.hostname").Return("", errors.New("job completed"))
						m.EXPECT().ReturnOutput("qstat -x -f -F json 0.hostname").Return(qstatExitZero, nil)
						m.EXPECT().RequestContext().Return(ctx).AnyTimes()
						return m
					}(),
					Cfg: func() configure.Configurer {
						m := mock_configure.NewMockConfigurer(ctrl)
						m.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()
						return m
					}(),
				},
				mng: qsub,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid qsub submission": {
			e: &executor{
				absExec: &abstracts.Executor{
					Env:        mockStateful(t.TempDir()),
					Msg:        msg,
					ScriptPath: passScript,
					Stage:      "step_script",
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptReturn(gomock.Any(), gomock.Any()).Return("qsub message", errors.New("error message"))
						return m
					}(),
					Cfg: func() configure.Configurer {
						m := mock_configure.NewMockConfigurer(ctrl)
						m.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()
						return m
					}(),
				},
				mng: qsub,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "job submission failed: qsub message")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := logging.NewMessenger()

	tests := map[string]pbsTests{
		"after_script executor": {
			ae: &abstracts.Executor{
				Stage: "after_script",
			},
			assertExec: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				if e != nil {
					assert.Empty(t, e.outFile)
				}
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"step_script illegal arguments": {
			ae: &abstracts.Executor{
				Stage: "step_script",
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg: msg,
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{
						ArgumentsVariable: []string{"ILLEGAL_SCHEDULER_PARAMETERS"},
					}).AnyTimes()
					return m
				}(),
			},
			targetEnv: map[string]string{
				"ILLEGAL_SCHEDULER_PARAMETERS": "-j eo",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"step_script functional": {
			ae: &abstracts.Executor{
				Stage: "step_script",
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: t.TempDir(),
					},
				},
				Msg: msg,
				Cfg: func() configure.Configurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().Batch().Return(configure.Batch{}).AnyTimes()
					return m
				}(),
			},
			targetEnv: map[string]string{
				"SCHEDULER_PARAMETERS": "-A account01",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(envkeys.UserEnvPrefix+k, v)
			}

			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExec != nil {
				tt.assertExec(t, got)
			}
		})
	}
}
