package pbs

import (
	"fmt"
	"path/filepath"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

type executor struct {
	absExec *abstracts.Executor
	mng     batch.Manager

	outFile   string
	jobID     string
	sleepTime time.Duration // Sleep between scheduler integrations.
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	e.absExec.Msg.Notify("The PBS executor for Jacamar CI is currently under active developments, changes will occur.")

	// Job scripts to be submitted via sbatch, needs to be updated.
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to updated PBS job script: %w", err)
	}

	return e.runPBS()
}

func (e *executor) runPBS() error {
	// Pre-create output files to avoid potential issues with jobs stuck in queue.
	batch.CreateFiles([]string{e.outFile}, e.absExec.Msg)

	out, err := e.submitJob()
	if err != nil {
		return fmt.Errorf("job submission failed: %s", out)
	}
	e.jobID = out
	e.absExec.Msg.Notify("Submitted batch job %s", out)

	return e.monitorJob()
}

func (e *executor) monitorJob() error {
	var wg sync.WaitGroup
	wg.Add(2)

	jobDone := make(chan struct{})
	go func() {
		defer wg.Done()
		e.mng.MonitorTermination(e.absExec.Runner, jobDone, e.jobID, e.absExec.Env.StatefulEnv.ScriptDir)
	}()

	go func() {
		defer wg.Done()
		e.watchOutput(jobDone)
	}()

	e.completed()

	close(jobDone)
	wg.Wait()

	// Wait for configured NFS timeout window.
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	return e.exitStatus()
}

func (e *executor) submitJob() (string, error) {
	qsubStdin := e.mng.BatchCmd(fmt.Sprintf(
		"-j oe -o %s %s",
		e.outFile,
		e.mng.UserArgs(),
	))

	e.absExec.Msg.Stdout(fmt.Sprintf("%s %s", qsubStdin, e.absExec.ScriptPath))
	out, err := e.absExec.Runner.JobScriptReturn(e.absExec.ScriptPath, qsubStdin)
	if err != nil {
		return out, err
	}

	return qsubJobID(out), nil
}

func (e *executor) watchOutput(jobDone chan struct{}) {
	// We generate output file in previous step from local host, add arbitrary short timeout.
	err := e.mng.TailFiles([]string{e.outFile}, jobDone, 10*time.Second, e.absExec.Msg)
	if err != nil {
		e.absExec.Msg.Warn("Unable to monitor output file (%s): %s", e.outFile, err.Error())
	}
}

// NewExecutor generates a valid PBS executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "qsub",
			StateCmd:    "qstat",
			StopCmd:     "qdel",
			IllegalArgs: []string{"-o", "-j"},
		}

		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}

		e.outFile = filepath.Clean(fmt.Sprintf(
			"%s/pbs-ci-%s.out",
			ae.Env.StatefulEnv.ScriptDir,
			ae.Env.RequiredEnv.JobID,
		))

		e.sleepTime = batch.CommandDelay(ae.Cfg.Batch().CommandDelay, ae.Msg)
	}

	return e, nil
}
