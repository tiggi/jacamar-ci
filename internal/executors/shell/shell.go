package shell

import (
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
)

type executor struct {
	absExec *abstracts.Executor
}

func (e *executor) Run() error {
	return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
}

// NewExecutor generates a valid Shell executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) *executor {
	return &executor{
		absExec: ae,
	}
}
