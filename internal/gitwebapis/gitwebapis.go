package gitwebapis

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

const (
	get = "GET"
)

var (
	defaultTimeout time.Duration
)

type clientManager struct {
	web httpClient
}

type httpClient interface {
	Do(*http.Request) (*http.Response, error)
}

type Client interface {
	// NoAuthGetJSON make GET request to target URL, response unmarshalled into interface.
	NoAuthGetJSON(string, interface{}) error
	// GetJSON make GET request to target URL, response unmarshalled into interface,
	// any supplied headers will be set.
	GetJSON(string, map[string]string, interface{}) error
}

func DefaultClient() Client {
	return clientManager{
		web: &http.Client{
			Timeout: defaultTimeout,
		},
	}
}

func CustomClient(c *http.Client) Client {
	if c == nil {
		return DefaultClient()
	}

	if c.Timeout == 0 {
		// disallow unspecified timeouts
		c.Timeout = defaultTimeout
	}

	return clientManager{
		web: c,
	}
}

func (cm clientManager) NoAuthGetJSON(tarURL string, v interface{}) error {
	m := make(map[string]string)
	return cm.GetJSON(tarURL, m, v)
}

func (cm clientManager) GetJSON(tarURL string, headers map[string]string, v interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()

	resp, err := cm.makeRequest(ctx, tarURL, get, headers, nil)
	if err != nil {
		return err
	}
	defer safeClose(resp)

	if resp.StatusCode != http.StatusOK {
		return invalidReq(resp)
	}
	if err = json.NewDecoder(resp.Body).Decode(v); err != nil {
		return fmt.Errorf("failed decode response, %w", err)
	}

	return nil
}

func safeClose(resp *http.Response) {
	if resp != nil {
		_ = resp.Body.Close()
	}
}

func invalidReq(resp *http.Response) (err error) {
	err = fmt.Errorf("invalid response (%v) received from http request", resp.StatusCode)

	buf := new(strings.Builder)
	i, _ := io.Copy(buf, resp.Body)
	if i != 0 {
		err = fmt.Errorf("%w: %s", err, buf.String())
	}

	return
}

func (cm clientManager) makeRequest(
	ctx context.Context,
	tarURL, method string,
	headers map[string]string,
	body io.Reader,
) (*http.Response, error) {
	req, err := newRequest(ctx, tarURL, method, headers, body)
	if err != nil {
		return nil, err
	}

	resp, err := cm.web.Do(req)
	if err != nil {
		return resp, fmt.Errorf("unable to complete http request: %w", err)
	}

	return resp, nil
}

func newRequest(
	ctx context.Context,
	tarURL, method string,
	headers map[string]string,
	body io.Reader,
) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, method, tarURL, body)
	if err != nil {
		return nil, fmt.Errorf("unable to establish NewRequest: %w", err)
	}

	for key, val := range headers {
		req.Header.Set(key, val)
	}

	return req, nil
}

func init() {
	defaultTimeout = 10 * time.Second
}
