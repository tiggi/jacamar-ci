package seccomp

import (
	"errors"
	"io"
	"os/exec"
	"syscall"
	"testing"

	libseccomp "github.com/seccomp/libseccomp-golang"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

var sysLog *logrus.Entry

func init() {
	ll := logrus.New()
	ll.Out = io.Discard
	sysLog = logrus.NewEntry(ll)
}

// Successful creation and application of filters will remain part of integration testing
// found in test/pavilion/tests/seccomp.yaml. Focus here should be on ensuring
// errors are handled as expected.

func TestFactory_Establish(t *testing.T) {
	type fields struct {
		Stage string
		Opt   configure.Options
		Usr   authuser.UserContext
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"seccomp disabled": {
			fields: fields{
				Opt: configure.Options{
					Auth: configure.Auth{
						Seccomp: configure.Seccomp{
							Disabled: true,
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"no downscope": {
			fields: fields{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "none",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid systemcall": {
			fields: fields{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
						Seccomp: configure.Seccomp{
							BlockCalls: []string{"notASystemCall"},
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "could not resolve syscall name")
			},
		},
		"block valid (unused) systemcall": {
			fields: fields{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "none",
						Seccomp: configure.Seccomp{
							BlockCalls: []string{"settimeofday"},
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"plugin error": {
			fields: fields{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "none",
						Seccomp: configure.Seccomp{
							FilterPlugin: t.TempDir() + "/plugin.so",
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				Stage:  tt.fields.Stage,
				Opt:    tt.fields.Opt,
				Usr:    tt.fields.Usr,
				SysLog: sysLog,
			}
			err := f.Establish()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}

	t.Run("pluginErr (panic) encountered", func(t *testing.T) {
		pluginErr = errors.New("plugin panic encountered")

		f := Factory{
			Opt: configure.Options{
				Auth: configure.Auth{
					Downscope: "none",
					Seccomp: configure.Seccomp{
						BlockCalls: []string{"settimeofday"},
					},
				},
			},
			SysLog: sysLog,
		}

		err := f.Establish()

		assert.EqualError(t, err, pluginErr.Error())
	})
}

func Test_config_enable(t *testing.T) {
	t.Run("invalid default action", func(t *testing.T) {
		err := config{
			defaultAction: libseccomp.ActInvalid,
		}.enable(sysLog)
		assert.Error(t, err)
	})
}

func Test_config_applyRules(t *testing.T) {
	invalidConFilter, _ := libseccomp.NewFilter(libseccomp.ActAllow)
	validConFilter, _ := libseccomp.NewFilter(libseccomp.ActAllow)

	tests := map[string]struct {
		c            config
		filter       *libseccomp.ScmpFilter
		assertFilter func(*testing.T, *libseccomp.ScmpFilter)
		assertError  func(*testing.T, error)
	}{
		"no rules defined": {
			c: config{},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"avoid default action config": {
			c: config{
				defaultAction: libseccomp.ActAllow,
				rules: []rule{
					rule{
						callName: "mkdir",
						action:   libseccomp.ActAllow,
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid conditional compare": {
			c: config{
				defaultAction: libseccomp.ActAllow,
				rules: []rule{
					rule{
						callName: "setuid",
						action:   defaultBlockAct,
						conditions: []condition{
							condition{
								compare: libseccomp.CompareInvalid,
								arg:     0,
								val:     uint64(1),
							},
						},
					},
				},
			},
			filter: invalidConFilter,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"valid conditional and rule": {
			c: config{
				defaultAction: libseccomp.ActAllow,
				rules: []rule{
					rule{
						callName: "mkdir",
						action:   defaultBlockAct,
					},
					rule{
						callName: "ioctl",
						action:   defaultBlockAct,
						conditions: []condition{
							condition{
								compare: libseccomp.CompareEqual,
								arg:     1,
								val:     syscall.TIOCSTI,
							},
						},
					},
				},
			},
			filter: validConFilter,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertFilter: func(t *testing.T, f *libseccomp.ScmpFilter) {
				assert.NotNil(t, f)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.c.applyRules(tt.filter)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertFilter != nil {
				tt.assertFilter(t, tt.filter)
			}
		})
	}
}

func Test_config_applyPlugin(t *testing.T) {
	versionSO := t.TempDir() + "/ver.so"
	cmd := exec.Command("go", []string{
		"build",
		"-trimpath",
		"-buildmode=plugin",
		"-o",
		versionSO,
		"../../test/pavilion/share/scripts/seccomp/panic.go",
	}...)
	_, err := cmd.CombinedOutput()
	assert.Nil(t, err, "plugin build")

	panicSO := t.TempDir() + "/ver.so"
	cmd = exec.Command("go", []string{
		"build",
		"-buildmode=plugin",
		"-o",
		panicSO,
		"../../test/pavilion/share/scripts/seccomp/panic.go",
	}...)
	_, err = cmd.CombinedOutput()
	assert.Nil(t, err, "plugin build")

	lookupSO := t.TempDir() + "/lookup.so"
	cmd = exec.Command("go", []string{
		"build",
		"-buildmode=plugin",
		"-o",
		lookupSO,
		"../../test/pavilion/share/scripts/seccomp/lookup.go",
	}...)
	_, err = cmd.CombinedOutput()
	assert.Nil(t, err, "plugin build")

	symSO := t.TempDir() + "/sym.so"
	cmd = exec.Command("go", []string{
		"build",
		"-buildmode=plugin",
		"-o",
		symSO,
		"../../test/pavilion/share/scripts/seccomp/symbol.go",
	}...)
	_, err = cmd.CombinedOutput()
	assert.Nil(t, err, "plugin build")

	type fields struct {
		pluginFile string
		stage      string
	}
	tests := map[string]struct {
		fields          fields
		filter          *libseccomp.ScmpFilter
		assertPluginErr func(*testing.T, error)
		assertError     func(*testing.T, error)
	}{
		"no plugin file defined": {
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"unable to locate file": {
			fields: fields{
				pluginFile: t.TempDir() + "/no.plugin",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"built with different version of GO": {
			fields: fields{
				pluginFile: versionSO,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "plugin was built with a different version of package")
				}
			},
		},
		"lookup error": {
			fields: fields{
				pluginFile: lookupSO,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "plugin: symbol SeccompExpansion not found in plugin")
				}
			},
		},
		"symbol error": {
			fields: fields{
				pluginFile: symSO,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "unable to locate 'SeccompExpansion(*libseccomp.ScmpFilter, string) error'")
				}
			},
		},
		"panic plugin": {
			fields: fields{
				pluginFile: panicSO,
			},
			assertPluginErr: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			c := config{
				pluginFile: tt.fields.pluginFile,
				stage:      tt.fields.stage,
			}

			err = c.applyPlugin(tt.filter)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertPluginErr != nil {
				tt.assertPluginErr(t, pluginErr)
			}
		})
	}
}

func TestSetNoNewPrivs(t *testing.T) {
	t.Run("run no_new_privs", func(t *testing.T) {
		_ = SetNoNewPrivs()
	})
}

func Test_config_loggingBit(t *testing.T) {
	tests := map[string]struct {
		c      config
		filter *libseccomp.ScmpFilter
	}{
		"logging bit defined": {
			c: config{},
			filter: func() *libseccomp.ScmpFilter {
				filter, err := libseccomp.NewFilter(libseccomp.ActKillThread)
				if err == nil {
					err = filter.SetLogBit(true)
					assert.NoError(t, err, "SetLogBit prep")
				}
				return filter
			}(),
		},
		"low API": {
			c: config{
				apiLevel: uint(1),
			},
			filter: func() *libseccomp.ScmpFilter {
				filter, err := libseccomp.NewFilter(libseccomp.ActKillThread)
				if err == nil {
					err = filter.SetLogBit(false)
					assert.NoError(t, err, "SetLogBit prep")
				}
				return filter
			}(),
		},
		"minimum API, invalid filter": {
			c: config{
				apiLevel: uint(3),
			},
			filter: func() *libseccomp.ScmpFilter {
				return &libseccomp.ScmpFilter{}
			}(),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.c.loggingBit(tt.filter, sysLog)
		})
	}
}
