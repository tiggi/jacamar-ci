package verifycaps

import (
	"errors"
	"io"
	"os"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"kernel.org/pub/linux/libs/security/libcap/cap"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_checkCapRules(t *testing.T) {
	tst.LinuxRootSkip(t)

	tests := map[string]struct {
		path        string
		mockFile    func(*testing.T, string)
		assertError func(*testing.T, error)
	}{
		"no capabilities found on file": {
			path: "/bin/bash",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"capabilities, 755 binary": {
			path: t.TempDir() + "/permissions.755",
			mockFile: func(t *testing.T, s string) {
				file, err := os.Create(s)
				assert.NoError(t, err, "generating test file")
				_ = file.Close()

				_ = os.Chmod(s, 0755)
				caps, _ := cap.FromText("cap_setgid,cap_setuid=ep")
				err = caps.SetFile(s)
				assert.NoError(t, err, "failed to set test capabilities")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "binary capabilities detected, ensure all group/world file permissions removed from Jacamar-Auth")
			},
		},
		"capabilities, valid binary": {
			path: t.TempDir() + "/permissions.700",
			mockFile: func(t *testing.T, s string) {
				file, err := os.Create(s)
				assert.NoError(t, err, "generating test file")
				_ = file.Close()

				_ = os.Chmod(s, 0700)
				caps, _ := cap.FromText("cap_setgid,cap_setuid=ep")
				err = caps.SetFile(s)
				assert.NoError(t, err, "failed to set test capabilities")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"capabilities, invalid setuid/setgid permissions": {
			path: t.TempDir() + "/permissions.invalid",
			mockFile: func(t *testing.T, s string) {
				file, err := os.Create(s)
				assert.NoError(t, err, "generating test file")
				_ = file.Close()

				_ = os.Chmod(s, 0700)
				caps, _ := cap.FromText("cap_setgid,cap_setuid=pie")
				err = caps.SetFile(s)
				assert.NoError(t, err, "failed to set test capabilities")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "capabilities inheritable (cap_setgid,cap_setuid=eip), only effective/permitted")
			},
		},
		"capabilities, valid binary with chown": {
			path: t.TempDir() + "/permissions.chown",
			mockFile: func(t *testing.T, s string) {
				file, err := os.Create(s)
				assert.NoError(t, err, "generating test file")
				_ = file.Close()

				_ = os.Chmod(s, 0700)
				caps, _ := cap.FromText("cap_chown,cap_setgid,cap_setuid=ep")
				err = caps.SetFile(s)
				assert.NoError(t, err, "failed to set test capabilities")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"capabilities, valid binary with 500": {
			path: t.TempDir() + "/permissions.500",
			mockFile: func(t *testing.T, s string) {
				file, err := os.Create(s)
				assert.NoError(t, err, "generating test file")
				_ = file.Close()

				_ = os.Chmod(s, 0500)
				caps, _ := cap.FromText("cap_setgid,cap_setuid=ep")
				err = caps.SetFile(s)
				assert.NoError(t, err, "failed to set test capabilities")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockFile != nil {
				tt.mockFile(t, tt.path)
			}

			err := checkCapRules(tt.path)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestCurrentExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stderr(gomock.Any(), gomock.Any())

	tests := []struct {
		name       string
		c          arguments.ConcreteArgs
		msg        logging.Messenger
		mockIDExec func()
		sysExit    func()
	}{
		{
			name: "no capabilities detected",
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: msg,
			sysExit: func() {
				assert.True(t, false, "sysExit triggered incorrectly")
			},
		},
		{
			name: "failed to identify executable/user",
			mockIDExec: func() {
				identifyExecutable = func() (string, error) {
					return "", errors.New("failed to identify exec")
				}
			},
			msg:     msg,
			sysExit: func() {},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mockIDExec != nil {
				tt.mockIDExec()
			}

			CurrentExec(tt.c, tt.msg, tt.sysExit)
		})
	}
}

func TestSignalAllowed(t *testing.T) {
	tests := []struct {
		name       string
		mockIDExec func()
		assertBool func(*testing.T, bool)
	}{
		{
			name: "user detected",
			mockIDExec: func() {
				identifyExecutable = identifyExec
			},
			assertBool: func(t *testing.T, b bool) {
				curUsr, _ := user.Current()
				if curUsr.Uid == "0" {
					assert.True(t, b, "root user")
				} else {
					assert.False(t, b, "non root-user")
				}
			},
		},
		{
			name: "failed to identify executable/user",
			mockIDExec: func() {
				identifyExecutable = func() (string, error) {
					return "", errors.New("failed to identify exec")
				}
			},
			assertBool: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mockIDExec != nil {
				tt.mockIDExec()
			}

			got := SignalAllowed()
			tt.assertBool(t, got)
		})
	}
}

func Test_checkSignalRules(t *testing.T) {
	tst.LinuxRootSkip(t)

	capPath := t.TempDir() + "/jacamar-auth"
	file, err := os.Create(capPath)
	assert.NoError(t, err, "generating test file")
	_ = file.Close()

	_ = os.Chmod(capPath, 0700)
	caps, _ := cap.FromText("cap_kill,cap_setgid,cap_setuid=ep")
	err = caps.SetFile(capPath)
	assert.NoError(t, err, "failed to set test capabilities")

	tests := map[string]struct {
		uid        string
		path       string
		assertBool func(*testing.T, bool)
	}{
		"root user": {
			uid: "0",
			assertBool: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"non-root invalid path": {
			uid:  "1001",
			path: t.TempDir() + "/invalid/path",
			assertBool: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"non-root cap_kill": {
			uid:  "1001",
			path: capPath,
			assertBool: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := checkSignalRules(tt.uid, tt.path)
			tt.assertBool(t, got)
		})
	}
}

func TestIsNoNewPrivs(t *testing.T) {
	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	t.Run("standard run", func(t *testing.T) {
		IsNoNewPrivs(sysLog)
	})

	t.Run("error encountered", func(t *testing.T) {
		getNoNewPrivs = func() (int, error) {
			return 0, errors.New("error message")
		}

		IsNoNewPrivs(sysLog)
	})
}
