// Package verifycaps manages interactions with the underlying system
// to verify and enforce supported usage of Linux capabilities(7).
package verifycaps

import (
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"syscall"

	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
	"kernel.org/pub/linux/libs/security/libcap/cap"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	identifyExecutable func() (string, error)
	getNoNewPrivs      func() (int, error)
)

// CurrentExec checks the executable for unsupported Unix permissions and
// capabilities. These include allowing public/group access when any capability
// is detected and allowing known capability requirements to be inheritable.
// If encountered a configuration error and system failure will be triggered.
func CurrentExec(c arguments.ConcreteArgs, msg logging.Messenger, sysExit func()) {
	path, err := identifyExecutable()
	if err != nil {
		errorhandling.MessageError(c, msg, errorhandling.NewJobCtxError(err))
		sysExit()
		return
	}

	if err = checkCapRules(path); err != nil {
		// Error must be obfuscated in case the verification not preformed before opening
		// access to all users.
		errorhandling.MessageError(c, msg, errorhandling.NewJobCtxError(err))
		sysExit()
	}
}

// SignalAllowed identify if the current executable, either through user permissions or
// capabilities, is able to send signals to a downscoped process.
func SignalAllowed() bool {
	usr, usrErr := user.Current()
	path, pathErr := identifyExecutable()

	if usrErr != nil || pathErr != nil {
		// Any error in gather context can be associated with a lack of permissions.
		return false
	}

	return checkSignalRules(usr.Uid, path)
}

// IsTerminal identifies if the current session is executed in a TTY.
func IsTerminal() bool {
	// Obtain current settings, failure would indicate no terminal for current process.
	// https://man7.org/linux/man-pages/man4/tty_ioctl.4.html
	_, err := unix.IoctlGetTermios(int(os.Stdin.Fd()), syscall.TCGETS)

	return err == nil
}

// IsNoNewPrivs identifies if process will operate in a privilege restricted mode
// and logs if true or unable to identify.
func IsNoNewPrivs(syslog *logrus.Entry) {
	v, err := getNoNewPrivs()
	if err != nil {
		syslog.Debug("Error encountered checking no_new_privs: ", err.Error())
	} else if v == 1 {
		// A return of 1 indicates no_new_privs has been set.
		// https://man7.org/linux/man-pages/man2/prctl.2.html
		syslog.Debug("Identified no_new_privs enforced on process.")
	}
}

func identifyExec() (path string, err error) {
	path, err = os.Executable()
	if err == nil {
		path, err = filepath.EvalSymlinks(path)
	}

	return
}

func checkCapRules(path string) (err error) {
	file, err := os.Stat(path)
	if err != nil {
		// Unlikely and would indicate an issue with binary file not
		// present during startup.
		return err
	}

	caps, err := cap.GetFile(path)
	if err != nil || caps == nil {
		// Indicates a lack of capabilities since we've already ensured the file exists.
		return nil
	}

	if !(file.Mode() == os.FileMode(0700)) && !(file.Mode() == os.FileMode(0500)) {
		err = fmt.Errorf(
			"binary capabilities detected, %s",
			"ensure all group/world file permissions removed from Jacamar-Auth",
		)
	} else if inheritable(caps) {
		err = fmt.Errorf(
			"capabilities inheritable (%s), only effective/permitted",
			caps.String(),
		)
	}

	return err
}

func checkSignalRules(uid, path string) bool {
	if uid != "0" {
		caps, err := cap.GetFile(path)
		if err != nil || caps == nil {
			return false
		}
		b, _ := caps.GetFlag(cap.Effective, cap.KILL)
		return b
	}

	return true
}

// inheritable check supported capabilities are not inheritable.
func inheritable(caps *cap.Set) bool {
	for _, i := range supportedCap() {
		inherit, err := caps.GetFlag(cap.Inheritable, i)
		if inherit || err != nil {
			return true
		}
	}
	return false
}

func supportedCap() []cap.Value {
	return []cap.Value{
		cap.SETUID,
		cap.SETGID,
		// Though documented as supported they can function in limited cases
		// and should be verified.
		cap.KILL,
		cap.CHOWN,
	}
}

func prctlRetInt() (int, error) {
	return unix.PrctlRetInt(
		unix.PR_GET_NO_NEW_PRIVS,
		uintptr(0),
		uintptr(0),
		uintptr(0),
		uintptr(0),
	)
}

func init() {
	identifyExecutable = identifyExec
	getNoNewPrivs = prctlRetInt
}
