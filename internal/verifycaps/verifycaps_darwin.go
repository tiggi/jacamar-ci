package verifycaps

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

func CurrentExec(c arguments.ConcreteArgs, msg logging.Messenger, sysExit func()) {
	msg.Error("Capabilities only supported Linux.")
	sysExit()
}

func SignalAllowed() bool {
	return false
}

func IsTerminal() bool {
	return true
}

func IsNoNewPrivs(syslog *logrus.Entry) {
	syslog.Info("Check only supported on Linux.")
}
