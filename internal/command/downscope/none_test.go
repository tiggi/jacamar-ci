package downscope

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestFactory_CreateStdShell(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(
		ctrl,
		configure.Auth{
			Downscope:   "none",
			JacamarPath: "/bin",
		},
		configure.General{
			KillTimeout: "5s",
		},
	)

	bash := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
	}
	bash.build("/bin/bash")

	tests := map[string]downTests{
		"valid standard bash command generated": {
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			auth:    mockAuthWorkingID(ctrl),
			cfg:     cfg,
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Equal(t, "/bin/jacamar", s.cmd.Path)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				AbsCmdr:  tt.absCmdr,
				AuthUser: tt.auth,
				Cfg:      tt.cfg,
				SysLog:   testSysLog,
			}
			s := f.CreateStdShell()

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
		})
	}
}
