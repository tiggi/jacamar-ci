package command

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	DefaultJacamarBin = "/opt/jacamar/bin"
	defaultShell      = "/usr/bin/bash"
	jacamarApp        = "jacamar"
	killDuration      = 30 // backup duration
)

// Commander interface provides the lowest level mechanisms that are supported by
// Jacamar for influencing how commands/stdin is executed on the underlying or target
// system. Idly it should be interacted with via a Runner interface built on top of.
type Commander interface {
	// PipeOutput executes the provided stdin. All output from the command is piped
	// directly to stdout/stderr. Bypasses the runner command and instead rely solely
	// on the underlying base/environment layer to avoid execution on such resources.
	PipeOutput(stdin string) error
	// ReturnOutput executes the provided stdin. All output from the command is returned
	// as a string. Bypasses the runner command and instead rely solely on the
	// underlying base/environment layer to avoid execution on such resources.
	ReturnOutput(stdin string) (string, error)
	// CommandDir changes the directory where command will be executed.
	CommandDir(dir string)
	// SigtermReceived returns a boolean based upon if SIGTERM has been captured.
	SigtermReceived() bool
	// RequestContext returns the commander context, used to identify if SIGTERM has been captured
	// and Jacamar must being a graceful shutdown process.
	RequestContext() context.Context
	// AppendEnv appends the provided slice to the command's environment.
	AppendEnv([]string)
	// CopiedCmd returns a copy of the underlying raw command.
	CopiedCmd() *exec.Cmd
	// ModifyCmd updates the underlying execute the named program with the given arguments.
	ModifyCmd(name string, arg ...string)
}

// AbstractCommander organizes configurations for all Runner interface. Each can
// implement support for configuration in their own way with no strict enforcement
// on how they are observed.
type AbstractCommander struct {
	KillTimeout  time.Duration
	NotifyTerm   bool // Send SIGTERM to sub-processes
	TermCaptured bool // Identify is SIGTERM has been capture and shutdown in process

	sigCtx    context.Context
	cancelCtx context.CancelFunc
	sysLog    *logrus.Entry
	max       time.Duration
	mutex     sync.Mutex
}

func (a *AbstractCommander) SigtermReceived() bool {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	return a.TermCaptured
}

// CaptureSigterm update TermCaptured, indicating SIGTERM has been received.
func (a *AbstractCommander) CaptureSigterm() {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	a.TermCaptured = true
}

func (a *AbstractCommander) SignalContext() context.Context {
	return a.sigCtx
}

// EnableNotifyTerm safely enables the use of SIGKILL.
func (a *AbstractCommander) EnableNotifyTerm() {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	a.NotifyTerm = true
}

// CloneCmd clones all values from the source exec.Cmd struct to the
// provided destination.
func CloneCmd(src, dest *exec.Cmd) {
	if src == nil || dest == nil {
		// Lets avoid a panic.
		return
	}

	val := reflect.ValueOf(src)
	if val.Kind() == reflect.Ptr {
		ex := val.Elem()
		y := reflect.New(ex.Type())
		ey := y.Elem()
		ey.Set(ex)
		reflect.ValueOf(dest).Elem().Set(y.Elem())
	}
}

// NoOutputCmd executes the provided command/args who's output, both for
// success and error, will be ignored.
func NoOutputCmd(command string, arg ...string) {
	/* #nosec */
	// Arguments identified and ran as same user.
	cmd := exec.Command(command, arg...)
	err := cmd.Start()
	if err == nil {
		_ = cmd.Wait()
	}
}

// RunCmd runs the provided exec.Cmd and integrates monitoring for SIGTERM into waiting for
// any spawned processes to complete.
func (a *AbstractCommander) RunCmd(cmd *exec.Cmd, sig Signaler) (err error) {
	err = cmd.Start()
	if err != nil {
		return err
	}

	cmdErr := make(chan error, 1)
	defer close(cmdErr)

	if !a.TermCaptured {
		a.sysLog.Debug(fmt.Sprintf(
			"command (%s) executed with PID: %d",
			cmd.Path,
			cmd.Process.Pid,
		))
		go a.MonitorTermination(cmd, cmdErr, sig)
	}

	err = cmd.Wait()
	cmdErr <- err

	return
}

func IdentifyShell(gen configure.General) string {
	if gen.Shell != "" {
		return gen.Shell
	}

	path, err := exec.LookPath("bash")
	if err != nil || path == "" {
		// This should be exceedingly rare as Bash is a requirement for deployment
		// but revert to default in case encountered.
		path = defaultShell
	}
	return path
}

// JacamarCmd builds a valid stdin to invoke Jacamar via a defined run mechanism.
func JacamarCmd(auth configure.Auth, c arguments.ConcreteArgs) (string, []string) {
	var cmd bytes.Buffer

	if auth.JacamarPath != "" {
		cmd.WriteString(filepath.Clean(strings.TrimSpace(auth.JacamarPath)))
		f, _ := os.Stat(cmd.String())
		// We may not be able to verify the file path exists. It is trusted
		// that when that occurs the admin configuration is accurate.
		if f != nil && f.IsDir() {
			if !strings.HasSuffix(auth.JacamarPath, "/") {
				cmd.WriteString("/")
			}
			cmd.WriteString(jacamarApp)
		}
	} else {
		cmd.WriteString(pathDefaults())
	}

	var args []string
	switch {
	case c.Prepare != nil:
		args = []string{"--no-auth", "prepare"}
	case c.Run != nil:
		args = []string{"--no-auth", "run", "env-script", c.Run.Stage}
	case c.Cleanup != nil:
		args = []string{"--no-auth", "cleanup", "--configuration", c.Cleanup.Configuration}
	case c.Signal != nil:
		args = []string{"--no-auth", "signal", c.Signal.Signal, c.Signal.PID}
	}

	return cmd.String(), args
}

func pathDefaults() string {
	path, err := os.Executable()
	if err == nil {
		return filepath.Dir(path) + "/" + jacamarApp
	}

	// Fallback to the default location for most RPM deployments.
	return DefaultJacamarBin + "/" + jacamarApp
}

// NewAbsCmdr generate a valid AbstractCommander structure with application defaults
// establish signal (SIGTERM) monitoring.
func NewAbsCmdr(
	ctx context.Context,
	gen configure.General,
	msg logging.Messenger,
	env envparser.ExecutorEnv,
	sysLog *logrus.Entry,
) *AbstractCommander {
	t, err := time.ParseDuration(gen.KillTimeout)
	if err != nil {
		// backup if invalid time duration provided
		t = killDuration * time.Second
		if gen.KillTimeout != "" {
			// we don't need to warn if choose to not set a time
			msg.Warn("Invalid time duration in configuration (KillTimeout), defaults to 30s.")
		}
	}

	sigCtx, cancelCtx := context.WithTimeout(ctx, timeoutCtx(env.StatefulEnv.RunnerTimeout))

	a := &AbstractCommander{
		NotifyTerm:  false,
		KillTimeout: t,
		sigCtx:      sigCtx,
		cancelCtx:   cancelCtx,
		sysLog:      sysLog,
		max:         timeoutCtx(env.StatefulEnv.RunnerTimeout),
	}

	go a.MonitorSignal()

	return a
}

func timeoutCtx(t string) time.Duration {
	dur, err := time.ParseDuration(t)
	if err != nil {
		dur = 60 * time.Second
	}
	return dur
}
