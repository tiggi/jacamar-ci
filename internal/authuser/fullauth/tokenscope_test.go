package fullauth

import (
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/gitwebapis"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gitwebapis"
)

func Test_Factory_checkTokenScope(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		api gitwebapis.Client
		opt configure.Options
		env envparser.ExecutorEnv
		msg logging.Messenger

		assertErr func(*testing.T, error)
	}{
		"skip when not enforced": {
			assertErr: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid parsed server URL": {
			opt: configure.Options{
				Auth: configure.Auth{
					TokenScopeEnforced: true,
				},
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					ServerURL: "https:// invalid.example.com",
				},
			},
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "parse \"https:// invalid.example.com\": invalid character \" \" in host name")
			},
		},
		"scope disabled": {
			api: func() gitwebapis.Client {
				m := mock_gitwebapis.NewMockClient(ctrl)
				m.EXPECT().GetJSON("https://gitlab.example.com/api/v4/job", map[string]string{"JOB-TOKEN": "disabled"}, gomock.Any()).Return(nil)
				return m
			}(),
			opt: configure.Options{
				Auth: configure.Auth{
					TokenScopeEnforced: true,
				},
			},
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobToken:  "disabled",
					ServerURL: "https://gitlab.example.com",
				},
			},
			msg: logging.NewMessenger(),
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "ci_job_token_scope_enabled not enabled for the project")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			f := Factory{
				Opt: tt.opt,
				Env: tt.env,
				Msg: tt.msg,
			}
			err := f.checkTokenScope(tt.api)

			if tt.assertErr != nil {
				tt.assertErr(t, err)
			}
		})
	}
}

func Test_Factory_checkTokenScope_gitlab(t *testing.T) {
	server := os.Getenv("CI_SERVER_URL")
	jobToken := os.Getenv("CI_JOB_TOKEN")

	if jobToken == "" || server == "" {
		t.Skip("required CI env not found")
	}

	projectID := os.Getenv("CI_PROJECT_ID")
	if projectID != "13829536" {
		// Required feature can only be found on gitlab.com currently, limit to our project.
		t.Skip("only required for ecp-ci/jacamar-ci at this time")
	}

	t.Run("test scope during GitLab CI", func(t *testing.T) {
		f := Factory{
			Opt: configure.Options{
				Auth: configure.Auth{
					TokenScopeEnforced: true,
				},
			},
			Env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					ServerURL: server,
					JobToken:  jobToken,
				},
			},
			Msg: logging.NewMessenger(),
		}
		err := f.checkTokenScope(gitwebapis.DefaultClient())
		assert.NoError(t, err)
	})
}
