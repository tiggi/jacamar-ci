package fullauth

import (
	"fmt"
	"os/user"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

type targetCtx struct {
	usrCtx authuser.UserContext
	jwt    gljobctx.Claims
}

func (t targetCtx) CIUser() authuser.UserContext {
	return t.usrCtx
}

func (t targetCtx) BuildState() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		Username:    t.usrCtx.Username,
		BaseDir:     t.usrCtx.BaseDir,
		BuildsDir:   t.usrCtx.BuildsDir,
		CacheDir:    t.usrCtx.CacheDir,
		ScriptDir:   t.usrCtx.ScriptDir,
		ProjectPath: t.jwt.ProjectPath,
	}
}

func (t targetCtx) PrepareNotification() string {
	return fmt.Sprintf(
		"Running as %s UID: %d GID: %d\n",
		t.usrCtx.Username,
		t.usrCtx.UID,
		t.usrCtx.GID,
	)
}

func (t targetCtx) JobJWT() gljobctx.Claims {
	return t.jwt
}

// Factory defines requirements and interface for the entire full authorization workflow. All values
// must be defined before attempting to use.
type Factory struct {
	SysLog *logrus.Entry
	Env    envparser.ExecutorEnv
	Opt    configure.Options
	Valid  authuser.Validators
	Msg    logging.Messenger
}

// EstablishUser processes and identifies a local user for the GitLab CI job. The current environment and
// configuration are used to dictate how the user authorization process will be handled. Once the process
// is completed the user context is immutable and should only be accessed through the provided interface.
func (f Factory) EstablishUser() (authuser.Authorized, error) {
	u := authuser.UserContext{}

	// No-auth will only validate the JWT during initial user identification (config_exec).
	jwt, err := authuser.VerifySetJWT(f.Valid.JobJWT, f.Env.RequiredEnv)
	if err != nil {
		err = fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
		f.SysLog.Error(err)
		return targetCtx{}, err
	}
	f.SysLog.Info(fmt.Sprintf("JWT verified (GitLab login: %s)", jwt.UserLogin))

	if f.Env.StatefulEnv.Username != "" {
		u, err = f.processFromState()
		if err != nil {
			err = fmt.Errorf("failed to process state for %s: %w", f.Env.StatefulEnv.Username, err)
			f.SysLog.Error(err.Error())
			return nil, err
		}
		f.SysLog.Info(fmt.Sprintf("local account (%s) processed from job state", u.Username))
	} else {
		f.SysLog.Debug(fmt.Sprintf("key JWT claims: %+v", jwt))

		u, err = f.processFlow(jwt)
		if err != nil {
			err = fmt.Errorf("failed to authorize user for CI job: %w", err)
			f.SysLog.Error(err.Error())
			return nil, err
		}
		f.SysLog.Info(fmt.Sprintf("local account (%s) authorized for CI job execution", u.Username))
	}

	if err = checkUserContext(u, f.Opt.Auth); err != nil {
		return nil, err
	}

	return targetCtx{
		usrCtx: u,
		jwt:    jwt,
	}, nil
}

var curUsr func() (*user.User, error)

func init() {
	curUsr = authuser.CurrentUser
}
