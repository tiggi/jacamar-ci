package fullauth

import (
	"io"
	"os/user"
	"runtime"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type listsTests struct {
	auth     configure.Auth
	username string
	groups   []string

	assertError func(*testing.T, error)
}

func TestUserContext_validateABLists(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	full := buildMockFullOpt().Auth
	min := buildMockMinOpt().Auth

	tests := map[string]listsTests{
		"empty configure.Auth options": {
			auth:     min,
			username: "user",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"defined in user allowlist": {
			auth:     full,
			username: "u1",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"defined in user allowlist + user blocklist": {
			auth:     full,
			username: "u4",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"defined in user blocklist": {
			auth:     full,
			username: "u3",
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"u3, is not in the user allowlist and is in the user blocklist",
				)
			},
		},
		"defined in group allowlist (match)": {
			auth:     full,
			username: "u5",
			groups:   []string{"g5"},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"undefined in group allowlist (no match)": {
			auth:     full,
			username: "u6",
			groups:   []string{"g6"},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"a group allowlist exists, but u6 is not a member of any defined group",
				)
			},
		},
		"defined in group blocklist": {
			auth:     full,
			username: "u7",
			groups:   []string{"g7"},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"u7, is not on the user allowlist and is in the group blocklist",
				)
			},
		},
		"defined in user allowlist + user blocklist + group allowlist + group blocklist": {
			auth:     full,
			username: "u8",
			groups:   []string{"g8"},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := validateABLists(tt.auth, &user.User{Username: tt.username}, tt.groups)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_validateShell(t *testing.T) {
	t.Run("Empty shell list", func(t *testing.T) {
		err := validateShell([]string{}, "1", "tester")
		assert.NoError(t, err)
	})

	shells := []string{"/bin/bash", "/bin/zsh", "/bin/nushell"}

	if runtime.GOOS == "darwin" {
		t.Run("Darwin - not supported", func(t *testing.T) {
			if err := validateShell(shells, "1", "tester"); (err != nil) != true {
				t.Errorf("validateShell() error = %v, wantErr %v", err, true)
			}
		})
	} else if runtime.GOOS == "linux" {
		tests := []struct {
			name    string
			UserUID string
			wantErr bool
		}{
			{
				name:    "bash",
				UserUID: "0",
				wantErr: false,
			}, {
				name:    "nologin",
				UserUID: "1",
				wantErr: true,
			}, {
				name:    "bad uid",
				UserUID: "abc",
				wantErr: true,
			},
		}
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				if err := validateShell(shells, tt.UserUID, tt.name); (err != nil) != tt.wantErr {
					t.Errorf("validateShell() error = %v, wantErr %v", err, tt.wantErr)
				}
			})
		}

	}
}

func Test_validatePipelineSource(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("Empty source list", func(t *testing.T) {
		err := Factory{}.validatePipelineSource("push")
		assert.NoError(t, err)
	})

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Error("Error: invalid pipeline trigger source () detected, please verify workflow supported on runner.")
	msg.EXPECT().Error("Error: invalid pipeline trigger source (unexpected event) detected, please verify workflow supported on runner.")

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	sources := []string{"web", "push"}

	tests := []struct {
		source  string
		wantErr bool
	}{
		{
			source:  "push",
			wantErr: false,
		}, {
			source:  "",
			wantErr: true,
		}, {
			source:  "unexpected event",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.source, func(t *testing.T) {
			err := Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						PipelineSourceAllowlist: sources,
					},
				},
				Msg:    msg,
				SysLog: sysLog,
			}.validatePipelineSource(tt.source)
			if (err != nil) != tt.wantErr {
				t.Errorf("validatePipelineSource() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
