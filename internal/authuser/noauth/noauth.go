package noauth

import (
	"fmt"
	"os/user"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/datadir"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var curUsr func() (*user.User, error)

type targetCtx struct {
	usrCtx authuser.UserContext
	jwt    gljobctx.Claims
}

func (t targetCtx) CIUser() authuser.UserContext {
	return t.usrCtx
}

func (t targetCtx) BuildState() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		Username:    t.usrCtx.Username,
		BaseDir:     t.usrCtx.BaseDir,
		BuildsDir:   t.usrCtx.BuildsDir,
		CacheDir:    t.usrCtx.CacheDir,
		ScriptDir:   t.usrCtx.ScriptDir,
		ProjectPath: t.jwt.ProjectPath,
	}
}

func (t targetCtx) PrepareNotification() string {
	return fmt.Sprintf(
		"Running as %s UID: %d GID: %d\n",
		t.usrCtx.Username,
		t.usrCtx.UID,
		t.usrCtx.GID,
	)
}

func (t targetCtx) JobJWT() gljobctx.Claims {
	return t.jwt
}

// Factory defines requirements and interface for the entire no-authorization workflow. All values
// must be defined before attempting use.
type Factory struct {
	Env   envparser.ExecutorEnv
	Opt   configure.Options
	Valid authuser.Validators
	Msg   logging.Messenger
	usr   *user.User
}

// EstablishUser generates a valid Authorized interface for us in a no-auth workflow either using the current
// user's context or established stateful variables. Once the process is completed the user context is
// immutable and should only be accessed through the provided interface.
func (f Factory) EstablishUser() (authuser.Authorized, error) {
	// Without authorization the current user is always used.
	usr, err := curUsr()
	if err != nil {
		return nil, err
	}
	f.usr = usr

	if f.Env.StatefulEnv.Username != "" {
		u, err := authuser.ProcessFromState(usr, f.Env.StatefulEnv)
		return targetCtx{
			usrCtx: u,
		}, err
	}

	return f.currentUser()
}

func (f Factory) currentUser() (targetCtx, error) {
	u := authuser.UserContext{}

	// No-auth will only validate the JWT during initial user identification (config_exec).
	jwt, err := authuser.VerifySetJWT(f.Valid.JobJWT, f.Env.RequiredEnv)
	if err != nil {
		return targetCtx{}, fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}

	u.Username = f.usr.Username
	u.HomeDir = f.usr.HomeDir

	if err = datadir.IdentifyDirectories(&u, f.Opt, f.Env.RequiredEnv, jwt, f.Msg); err != nil {
		return targetCtx{}, fmt.Errorf("unable to identify target directories: %w", err)
	}

	err = authuser.SetIntIDs(f.usr.Uid, f.usr.Gid, &u)

	return targetCtx{
		usrCtx: u,
		jwt:    jwt,
	}, err
}

func init() {
	curUsr = authuser.CurrentUser
}
