package authuser_test

import (
	"errors"
	"os"
	"os/user"
	"strconv"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gljobctx"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestUserContext_ProcessFromState(t *testing.T) {
	usr, err := authuser.CurrentUser()
	assert.NoError(t, err, "error identifying current user")

	type args struct {
		usr *user.User
		s   envparser.StatefulEnv
	}
	tests := map[string]struct {
		u                 *authuser.UserContext
		args              args
		assertError       func(*testing.T, error)
		assertUserContext func(*testing.T, authuser.UserContext)
	}{
		"error encountered setting interface UID": {
			args: args{
				usr: &user.User{
					Uid: "uid",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"error encountered setting interface GID": {
			args: args{
				usr: &user.User{
					Uid: "1000",
					Gid: "gid",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"update context from established state": {
			args: args{
				usr: usr,
				s: envparser.StatefulEnv{
					Username:  usr.Username,
					BaseDir:   "/base",
					BuildsDir: "/base/builds",
					CacheDir:  "/base/cache",
					ScriptDir: "/base/script",
				},
			},
			assertError: tst.AssertNoError,
			assertUserContext: func(t *testing.T, u authuser.UserContext) {
				assert.NotNil(t, u)
				assert.Equal(t, usr.Username, u.Username)
				assert.Equal(t, "/base", u.BaseDir)
				assert.Equal(t, usr.Gid, strconv.Itoa(u.GID))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			u, err := authuser.ProcessFromState(tt.args.usr, tt.args.s)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, u)
			}
		})
	}
}

func Test_CurrentUser(t *testing.T) {
	// At this time the CurrentUser function should simply defer to calling the
	// associated method in the user package. Enforce this until future changes may be made.
	sysUsr, sysErr := user.Current()
	usr, err := authuser.CurrentUser()

	assert.Equal(t, sysUsr, usr)
	assert.Equal(t, sysErr, err)
}

func TestVerifySetJWT(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	jwtTest := mock_gljobctx.NewMockValidater(ctrl)
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.failed.example"),
		gomock.Eq("failed.example"),
	).Return(gljobctx.Claims{}, errors.New("error message"))
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.success.example"),
		gomock.Eq("success.example"),
	).Return(gljobctx.Claims{
		JobID: "12345",
	}, nil)

	t.Run("jwt validation error observed", func(t *testing.T) {
		_, err := authuser.VerifySetJWT(jwtTest, envparser.RequiredEnv{
			CIJobJWT:  "jwt.failed.example",
			ServerURL: "failed.example",
		})
		assert.EqualError(t, err, "unable to parse supplied CI_JOB_JWT: error message")
	})

	t.Run("jwt validation successful", func(t *testing.T) {
		got, err := authuser.VerifySetJWT(jwtTest, envparser.RequiredEnv{
			CIJobJWT:  "jwt.success.example",
			ServerURL: "success.example",
		})
		assert.NoError(t, err)
		assert.Equal(t, "12345", got.JobID)
	})
}

func TestEstablishValidators(t *testing.T) {
	type args struct {
		stage string
		opt   configure.Options
		env   envparser.ExecutorEnv
	}
	type estValidTests struct {
		args             args
		assertValidators func(*testing.T, authuser.Validators)
		assertError      func(*testing.T, error)
	}

	tests := map[string]estValidTests{
		"standard validators, no runas": {
			args: args{
				stage: "config_exec",
				opt:   configure.Options{},
				env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID:     "1",
						ServerURL: "https://gitlab.example.com/",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertValidators: func(t *testing.T, validators authuser.Validators) {
				assert.Nil(t, validators.RunAs, "RunAs validator")
				assert.NotNil(t, validators.JobJWT, "JWT validator")
			},
		},
		"error encountered creating RunAs validation": {
			args: args{
				opt: configure.Options{
					Auth: configure.Auth{
						RunAs: configure.RunAs{
							ValidationPlugin: "test",
						},
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"cleanp_exec stateful with delay error": {
			args: args{
				stage: "cleanup_exec",
				env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "test",
					},
				},
				opt: configure.Options{
					Auth: configure.Auth{
						JWTExpDelay: "10mh",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "time: unknown unit \"mh\" in duration \"10mh\"")
			},
		},
		"archive_cache stateful without delay config": {
			args: args{
				stage: "archive_cache",
				env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "test",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	tlsFile := os.Getenv("CI_SERVER_TLS_CA_FILE")
	encoded := os.Getenv("CI_JOB_JWT")
	encodedV2 := os.Getenv("CI_JOB_JWT_V2")
	server := os.Getenv("CI_SERVER_URL")
	jobID := os.Getenv("CI_JOB_ID")

	if encoded != "" {
		tests["valid CI_JOB_JWT test"] = estValidTests{
			args: args{
				stage: "config_exec",
				opt: configure.Options{
					General: configure.General{
						TLSCAFile: tlsFile,
					},
				},
				env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID:     jobID,
						ServerURL: server,
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertValidators: func(t *testing.T, validators authuser.Validators) {
				assert.Nil(t, validators.RunAs, "RunAs validator")
				assert.NotNil(t, validators.JobJWT, "JWT validator")

				if validators.JobJWT != nil {
					claims, err := validators.JobJWT.ValidateJWT(encoded, server)
					assert.NoError(t, err)
					assert.Equal(t, jobID, claims.JobID)
				}
			},
		}
	}

	if encodedV2 != "" {
		tests["valid CI_JOB_JWT test"] = estValidTests{
			args: args{
				stage: "config_exec",
				opt: configure.Options{
					General: configure.General{
						TLSCAFile: tlsFile,
					},
				},
				env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID:     jobID,
						ServerURL: server,
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertValidators: func(t *testing.T, validators authuser.Validators) {
				assert.Nil(t, validators.RunAs, "RunAs validator")
				assert.NotNil(t, validators.JobJWT, "JWT validator")

				if validators.JobJWT != nil {
					claims, err := validators.JobJWT.ValidateJWT(encodedV2, server)
					assert.NoError(t, err)
					assert.Equal(t, jobID, claims.JobID)
				}
			},
		}
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := authuser.EstablishValidators(tt.args.stage, tt.args.opt, tt.args.env)

			if tt.assertValidators != nil {
				tt.assertValidators(t, got)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
