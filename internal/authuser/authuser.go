// Package authuser (authorize user) maintains the interface for the complete authorization
// flow, leveraging the job's configuration as well as context provided by the custom executor
// to ensure a fully authorized user is identified for the CI job.
package authuser

import (
	"fmt"
	"os/user"
	"strconv"
	"time"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// Authorized implements an interface allowing read-only access to the established user
// context. All information is identified during a job's configuration stage and remains
// consistent throughout the life of the job.
type Authorized interface {
	// CIUser returns a completely authorized user context.
	CIUser() UserContext
	// BuildState populates and return the StatefulEnv.
	BuildState() envparser.StatefulEnv
	// PrepareNotification returns details on the current job's validated user to
	// used in the prepare_exec message.
	PrepareNotification() string
	// JobJWT returns a subset of the claims from a validated JSON web token  associated
	// with the  current CI job. This is only present when authorization has not been disabled.
	JobJWT() gljobctx.Claims
}

// UserContext contains validated user details for the current CI job.
type UserContext struct {
	Username string
	HomeDir  string
	UID      int
	GID      int
	// Groups all supplementary unix groups assigned to the user.
	Groups []uint32
	// BaseDir directory for CI job and command interactions.
	BaseDir string
	// BuildsDir is the working directory created on the local file system.
	BuildsDir string
	// CacheDir is the working directory created on the local file system.
	CacheDir string
	// ScriptDir is the directory for script storage and command execution.
	ScriptDir string
	// DataDirOverride proposed override for configured data_dir.
	DataDirOverride string
}

// CurrentUser returns the current user.
func CurrentUser() (*user.User, error) {
	return user.Current()
}

// ProcessFromState update basic UserContext using the supplied user object in conjunction
// with Stateful environment variables identified.
func ProcessFromState(usr *user.User, s envparser.StatefulEnv) (u UserContext, err error) {
	u.Username = usr.Username
	u.HomeDir = usr.HomeDir
	u.Username = s.Username
	u.BaseDir = s.BaseDir
	u.BuildsDir = s.BuildsDir
	u.CacheDir = s.CacheDir
	u.ScriptDir = s.ScriptDir

	err = SetIntIDs(usr.Uid, usr.Gid, &u)

	return
}

// SetIntIDs converts the supplied uid & gid to integers and updates the UserContext.
func SetIntIDs(uid, gid string, u *UserContext) error {
	iUid, err := strconv.Atoi(uid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's UID (%s)", uid)
	}
	iGid, err := strconv.Atoi(gid)
	if err != nil {
		return fmt.Errorf("unable to convert valid user's GID (%s)", gid)
	}

	u.UID = iUid
	u.GID = iGid

	return nil
}

// VerifySetJWT using the supplied validator, check to ensure the JWT is signed and contains
// an expected payload. Update the UserContext accordingly.
func VerifySetJWT(jobJWT gljobctx.Validater, req envparser.RequiredEnv) (gljobctx.Claims, error) {
	jwtClaims, err := jobJWT.ValidateJWT(req.CIJobJWT, req.ServerURL)
	if err != nil {
		return gljobctx.Claims{}, fmt.Errorf("unable to parse supplied CI_JOB_JWT: %w", err)
	}

	return jwtClaims, nil
}

type Validators struct {
	RunAs  validation.RunAsValidator
	JobJWT gljobctx.Validater
}

// EstablishValidators create a series of Validators based upon configuration and identifiable job context.
func EstablishValidators(
	stage string,
	opt configure.Options,
	env envparser.ExecutorEnv,
) (Validators, error) {
	rs, err := validation.NewRunAs(opt.Auth)
	if err != nil {
		return Validators{}, fmt.Errorf("failed to establish RunAs validator: %w", err)
	}

	expDelay, err := identifyExpDelay(stage, opt.Auth.JWTExpDelay, env.StatefulEnv.Username)
	if err != nil {
		return Validators{}, err
	}

	jobJWT := gljobctx.Options{
		JobID:               env.RequiredEnv.JobID,
		TLSCAFile:           opt.General.TLSCAFile,
		ExpDelay:            expDelay,
		ClaimsEnvValidation: true,
	}

	return Validators{
		RunAs:  rs,
		JobJWT: jobJWT,
	}, err
}

func identifyExpDelay(stage, expDelay, username string) (time.Duration, error) {
	if username == "" {
		// If there exists no username (indicating lack of stateful variables)
		// we do not allow an expired JWT.
		return 0, nil
	}

	// Stages are runner defined: https://docs.gitlab.com/runner/executors/custom.html#run
	// these are not the same as user defined pipeline stages (via .gitlab-ci.yml).
	supportedStages := map[string]bool{
		"archive_cache":               true,
		"archive_cache_on_failure":    true,
		"upload_artifacts_on_failure": true,
		"upload_artifacts_on_success": true,
		"cleanup_file_variables":      true,
		"cleanup_exec":                true,
	}

	// Treat both empty string and zero value as a desire to no allow any
	// expiration regardless of the stage.
	if expDelay != "" && expDelay != "0" && supportedStages[stage] {
		return time.ParseDuration(expDelay)
	}

	// Default to not allowing any delay.
	return 0, nil
}
