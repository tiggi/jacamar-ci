package errorhandling

import (
	"errors"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func mockWarn(ctrl *gomock.Controller, arg0 string, arg1 string) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(gomock.Eq(arg0), gomock.Eq(arg1)).Times(1)
	return m
}

func mockStderr(ctrl *gomock.Controller, arg0 string, arg1 string) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Stderr(gomock.Eq(arg0), gomock.Eq(arg1)).Times(1)
	return m
}

func TestMessageErr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		c    arguments.ConcreteArgs
		auth configure.Auth
		msg  logging.Messenger
		e    error
	}{
		"base error during prepare stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			e:   errors.New("prepare error"),
			msg: mockWarn(ctrl, stderrPrefix, "prepare error"),
		},
		"base error during cleanup stage": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e:   errors.New("cleanup error"),
			msg: mockStderr(ctrl, stderrPrefix, "cleanup error"),
		},
		"obfuscated auth error config stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			e:   NewAuthError(errors.New("auth error")),
			msg: mockStderr(ctrl, stderrPrefix, usrAuthorization+usrPostfix),
		},
		"new user error nested in obfuscated auth error config stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			e:   NewAuthError(fmt.Errorf("hidden error: %w", NewUserError(errors.New("user auth error")))),
			msg: mockStderr(ctrl, stderrPrefix, "user auth error"),
		},
		"new user error nested in obfuscated auth error cleanup stage": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e:   NewAuthError(fmt.Errorf("hidden error: %w", NewUserError(errors.New("user auth error")))),
			msg: mockStderr(ctrl, stderrPrefix, "hidden error: user auth error"+cleanCtx),
		},
		"new user error very-nested in obfuscated auth error config stage": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			e:   NewAuthError(fmt.Errorf("yet another: %w", fmt.Errorf("another hidden: %w", fmt.Errorf("hidden error: %w", NewUserError(errors.New("user auth error")))))),
			msg: mockStderr(ctrl, stderrPrefix, "user auth error"),
		},
		"obfuscated run mechanism error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e:   NewRunMechanismError(errors.New("run error")),
			msg: mockWarn(ctrl, stderrPrefix, usrRunMechanism+usrPostfix),
		},
		"obfuscated new user error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e:   NewUserError(errors.New("run error")),
			msg: mockWarn(ctrl, stderrPrefix, "run error"),
		},
		"un-obfuscated job context error cleanup stage": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e:   NewJobCtxError(errors.New("job context error")),
			msg: mockStderr(ctrl, stderrPrefix, "job context error"+cleanCtx),
		},
		"obfuscated job context error prepare stage": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			e:   NewJobCtxError(errors.New("job context error")),
			msg: mockWarn(ctrl, stderrPrefix, usrJobCtx+usrPostfix),
		},
		"UnobfuscatedError enabled": {
			c: arguments.ConcreteArgs{
				Config:            &arguments.ConfigCmd{},
				UnobfuscatedError: true,
			},
			e:   NewAuthError(errors.New("auth error II")),
			msg: mockStderr(ctrl, stderrPrefix, "auth error II"),
		},
		"obfuscated seccomp error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e:   NewSeccompError(errors.New("seccomp error")),
			msg: mockWarn(ctrl, stderrPrefix, usrSeccomp+usrPostfix),
		},
		"obfuscated panic error run stage": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			e:   NewPanicError(errors.New("panic error")),
			msg: mockWarn(ctrl, stderrPrefix, usrPanic+usrPostfix),
		},
		"cleanup error not additional context": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e:   NewRunMechanismError(errors.New("run error")),
			msg: mockStderr(ctrl, stderrPrefix, "run error"),
		},
		"cleanup error auth additional context": {
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			e:   NewAuthError(errors.New("not authorized")),
			msg: mockStderr(ctrl, stderrPrefix, "not authorized"+cleanCtx),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			MessageError(tt.c, tt.msg, tt.e)
		})
	}
}

func TestIsObfuscatedErr(t *testing.T) {
	var tmpErr ObfuscatedErr

	tests := map[string]struct {
		e    error
		want bool
	}{
		"base error": {
			e:    errors.New("test"),
			want: false,
		},
		"auth error": {
			e:    NewAuthError(errors.New("authorization")),
			want: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := IsObfuscatedErr(tt.e, &tmpErr); got != tt.want {
				t.Errorf("IsObfuscatedErr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsUserSafeErr(t *testing.T) {
	tests := map[string]struct {
		e    error
		want bool
	}{
		"base error": {
			e:    errors.New("test"),
			want: false,
		},
		"user error": {
			e:    NewUserError(errors.New("authorization")),
			want: true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if got := IsUserSafeErr(tt.e); got != tt.want {
				t.Errorf("IsUserSafeErr() = %v, want %v", got, tt.want)
			}
		})
	}
}
