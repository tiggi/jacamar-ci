package augmenter

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
)

type stepTests struct {
	path string
	msg  string

	mockFile func(*testing.T, string)
	mockStep func(*testing.T, string) *StepScript

	assertError func(*testing.T, error)
	assertFile  func(*testing.T, string)
	assertStep  func(*testing.T, *StepScript)
}

func simpleJobScript(t *testing.T, path string) {
	jobScript := `#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running a test..."\n'
exit 0
`
	err := os.WriteFile(path, []byte(jobScript), usertools.OwnerPermissions)
	assert.NoError(t, err, "failure preparing test script")
}

func TestOpenStepScript(t *testing.T) {
	tests := map[string]stepTests{
		"valid file identified": {
			path:     t.TempDir() + "/valid-file.bash",
			mockFile: simpleJobScript,
			assertStep: func(t *testing.T, step *StepScript) {
				assert.NotNil(t, step)
				assert.True(t, len(step.content) == 7)
			},
		},
		"empty file": {
			path: t.TempDir() + "/empty-file.bash",
			mockFile: func(t *testing.T, s string) {
				err := os.WriteFile(s, []byte(""), usertools.OwnerPermissions)
				assert.NoError(t, err, "failure preparing test script")
			},
			assertStep: func(t *testing.T, step *StepScript) {
				assert.NotNil(t, step)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockFile != nil {
				tt.mockFile(t, tt.path)
			}

			got := OpenStepScript(tt.path)

			if tt.assertStep != nil {
				tt.assertStep(t, got)
			}
		})
	}
}

func TestStepScript_LoginShell(t *testing.T) {
	tests := map[string]stepTests{
		"empty content encountered": {
			mockStep: func(t *testing.T, s string) *StepScript {
				return &StepScript{}
			},
			assertStep: func(t *testing.T, step *StepScript) {
				assert.True(t, step.error)
			},
		},
		"valid file modified": {
			path: t.TempDir() + "/valid-file.bash",
			mockStep: func(t *testing.T, s string) *StepScript {
				simpleJobScript(t, s)
				return OpenStepScript(s)
			},
			assertStep: func(t *testing.T, step *StepScript) {
				assert.False(t, step.error)
				assert.Equal(t, step.content[0], "#!/bin/bash --login")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			s := tt.mockStep(t, tt.path)
			s.LoginShell()

			if tt.assertStep != nil {
				tt.assertStep(t, s)
			}
		})
	}
}

func TestStepScript_MessageExit(t *testing.T) {
	tests := map[string]stepTests{
		"empty content encountered": {
			msg: "Hello World!",
			mockStep: func(t *testing.T, s string) *StepScript {
				return &StepScript{}
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unexpect script detected, only runner generated currently supported")
			},
		},
		"empty message": {
			mockStep: func(t *testing.T, s string) *StepScript {
				return &StepScript{}
			},
			assertStep: func(t *testing.T, step *StepScript) {
				assert.True(t, step.error)
			},
		},
		"valid file modified": {
			path: t.TempDir() + "/valid-file.bash",
			msg:  "Hello World!",
			mockStep: func(t *testing.T, s string) *StepScript {
				simpleJobScript(t, s)
				return OpenStepScript(s)
			},
			assertStep: func(t *testing.T, step *StepScript) {
				assert.False(t, step.error)
				assert.Equal(t, step.content[5], "echo -e \"Hello World!\" && exit 0")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			s := tt.mockStep(t, tt.path)
			s.MessageExit(tt.msg)

			if tt.assertStep != nil {
				tt.assertStep(t, s)
			}
		})
	}
}

func TestStepScript_VerifyAndWrite(t *testing.T) {
	tests := map[string]stepTests{
		"write un-edited file": {
			path: t.TempDir() + "/empty-file.bash",
			mockStep: func(t *testing.T, s string) *StepScript {
				simpleJobScript(t, s)
				return OpenStepScript(s)
			},
			assertError: func(t *testing.T, err error) {
				// It is not up to this function to ensure that a valid runner script has been provided.
				assert.NoError(t, err)
			},
		},
		"errors with writefile observed": {
			mockStep: func(t *testing.T, s string) *StepScript {
				return &StepScript{}
			},
			assertError: func(t *testing.T, err error) {
				// It is not up to this function to ensure that a valid runner script has been provided.
				assert.EqualError(t, err, "open : no such file or directory")
			},
		},
		"errors encountered in prior stage": {
			mockStep: func(t *testing.T, s string) *StepScript {
				return &StepScript{
					error: true,
				}
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unexpect script detected, only runner generated currently supported")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			s := tt.mockStep(t, tt.path)

			err := s.VerifyAndWrite()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}
