package augmenter

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"sync"

	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
)

type StepScript struct {
	path    string
	content []string
	error   bool
	m       sync.Mutex
}

// OpenStepScript opens the runner generated step script for additional modification as
// required by the executor. It assumes that the presence of the job script has been
// verified in previous steps.
func OpenStepScript(path string) *StepScript {
	content, _ := openScript(path)

	return &StepScript{
		path:    path,
		content: content,
	}
}

// LoginShell updates the Bash script contents to ensure a login shell is provided.
func (s *StepScript) LoginShell() {
	s.m.Lock()
	defer s.m.Unlock()

	// Basic check, don't cause a panic.
	if len(s.content) < 1 || !strings.Contains(s.content[0], "bash") {
		s.error = true
		return
	}
	s.content[0] = `#!/bin/bash --login`
}

// MessageExit integrates the provided message into the scripts exit process.
func (s *StepScript) MessageExit(msg string) {
	s.m.Lock()
	defer s.m.Unlock()

	var found bool
	for i := range s.content {
		if strings.HasPrefix(s.content[i], "exit") {
			// Concatenate to avoid potential issue with unknown message contents.
			s.content[i] = fmt.Sprintf("echo -e \"%s\" && ", msg) + s.content[i]
			found = true
		}
	}

	if !found {
		s.error = true
	}
}

// VerifyAndWrite joins the StepScript contents and checks for any potential errors
// before re-writing the updated version to the file script.
func (s *StepScript) VerifyAndWrite() error {
	s.m.Lock()
	defer s.m.Unlock()

	if s.error {
		return errors.New("unexpect script detected, only runner generated currently supported")
	}

	output := strings.Join(s.content, "\n")
	/* #nosec */
	// creating script, 700 permissions required
	return os.WriteFile(s.path, []byte(output), usertools.OwnerPermissions)
}
