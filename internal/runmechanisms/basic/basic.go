// Package basic implements a non-interfering runner interface that
// redirects all input to the underlying commander for execution.
package basic

import (
	"context"
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type mechanism struct {
	cmdr command.Commander
}

func (m *mechanism) JobScriptOutput(script string, stdin ...interface{}) error {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return nil
	}

	return m.PipeOutput(m.prepStdin(script, stdin...))
}

func (m *mechanism) JobScriptReturn(script string, stdin ...interface{}) (string, error) {
	if m.cmdr.SigtermReceived() {
		// SIGTERM captured, don't launch new job scripts.
		return "", nil
	}

	return m.ReturnOutput(m.prepStdin(script, stdin...))
}

func (m *mechanism) prepStdin(script string, stdin ...interface{}) string {
	var sb strings.Builder

	r := reflect.ValueOf(stdin)
	if !r.IsZero() {
		sb.WriteString(fmt.Sprint(stdin...))
		sb.WriteString(" ")
	}

	sb.WriteString(script)

	return sb.String()
}

func (m *mechanism) PipeOutput(stdin string) error {
	return m.cmdr.PipeOutput(stdin)
}

func (m *mechanism) ReturnOutput(stdin string) (string, error) {
	return m.cmdr.ReturnOutput(stdin)
}

func (m *mechanism) CommandDir(dir string) {
	m.cmdr.CommandDir(dir)
}

func (m *mechanism) SigtermReceived() bool {
	return m.cmdr.SigtermReceived()
}

func (m *mechanism) RequestContext() context.Context {
	return m.cmdr.RequestContext()
}

func (m *mechanism) TransferScript(
	path string,
	stage string,
	env envparser.ExecutorEnv,
	opt configure.Options,
) error {
	rules := augmenter.Rules{
		UnrestrictedCmdline:  opt.General.UnrestrictedCmdline,
		AllowUserCredentials: false,
	}

	contents, err := rules.JobScript(path, stage, env)
	if err != nil {
		return err
	}

	m.cmdr.AppendEnv(envparser.EstablishScriptEnv(contents, opt.Auth.MaxEnvChars))

	return nil
}

func (m *mechanism) ModifyCmd(name string, arg ...string) {
	m.cmdr.ModifyCmd(name, arg...)
}

// NewMechanism generates a basic Runner interface that will insert
// no influence over stdin/script provided.
func NewMechanism(cmdr command.Commander) *mechanism {
	return &mechanism{
		cmdr: cmdr,
	}
}
