package arguments

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_subCommands(t *testing.T) {
	tests := map[string]struct {
		c          *ConcreteArgs
		a          argumenter
		assertStr  func(*testing.T, string)
		assertArgs func(*testing.T, *ConcreteArgs)
	}{
		"missing arguments jacamar-auth": {
			a: argumenter{
				auth: true,
				name: "jacamar-auth",
			},
			assertStr: func(t *testing.T, s string) {
				assert.Contains(t, s, fmt.Sprintf("  --unobfuscated, -u\t%s\n", obfOptHelp))
				assert.Equal(t, s, buildHelp(true, "jacamar-auth"))
			},
		},
		"missing arguments custom jacamar": {
			a: argumenter{
				auth: false,
				name: "jacamar-test",
			},
			assertStr: func(t *testing.T, s string) {
				assert.Contains(t, s, fmt.Sprintf("  --no-auth, -n\t%s\n", usrOptHelp))
				assert.Equal(t, s, buildHelp(false, "jacamar-test"))
			},
		},
		"missing known subcommand": {
			a: argumenter{
				auth:        false,
				name:        "jacamar",
				nonFlagArgs: []string{"test", "-a"},
			},
			assertStr: func(t *testing.T, s string) {
				assert.Equal(t, s, missingSub)
			},
		},
		"valid config file": {
			a: argumenter{
				auth:        false,
				name:        "jacamar",
				nonFlagArgs: []string{"config", "--configuration", "/file.toml"},
			},
			c: &ConcreteArgs{},
			assertArgs: func(t *testing.T, args *ConcreteArgs) {
				assert.NotNil(t, args.Config)
				assert.Equal(t, args.Config.Configuration, "/file.toml")
			},
		},
		"prepare help": {
			a: argumenter{
				name:        "jacamar-auth",
				nonFlagArgs: []string{"prepare", "-h"},
			},
			assertStr: func(t *testing.T, s string) {
				assert.Equal(t, "Usage: jacamar-auth [options] prepare\n", s)
			},
		},
		"run help": {
			a: argumenter{
				name:        "jacamar-auth",
				nonFlagArgs: []string{"run", "--help"},
			},
			assertStr: func(t *testing.T, s string) {
				assert.Equal(t, "Usage: jacamar-auth [options] run SCRIPT STAGE\n", s)
			},
		},
		"valid prepare": {
			a: argumenter{
				name:        "jacamar-auth",
				nonFlagArgs: []string{"prepare"},
			},
			c: &ConcreteArgs{},
			assertArgs: func(t *testing.T, args *ConcreteArgs) {
				assert.NotNil(t, args.Prepare)
			},
		},
		"prepare new args": {
			a: argumenter{
				name:        "jacamar-auth",
				nonFlagArgs: []string{"prepare", "new"},
			},
			c: &ConcreteArgs{},
			assertArgs: func(t *testing.T, args *ConcreteArgs) {
				assert.NotNil(t, args.Prepare)
			},
		},
		"valid cleanup file": {
			a: argumenter{
				name:        "jacamar-auth",
				nonFlagArgs: []string{"cleanup", "--configuration", "/file.toml"},
			},
			c: &ConcreteArgs{},
			assertArgs: func(t *testing.T, args *ConcreteArgs) {
				assert.NotNil(t, args.Cleanup)
				assert.Equal(t, args.Cleanup.Configuration, "/file.toml")
			},
		},
		"attempt signal during auth": {
			a: argumenter{
				auth:        true,
				name:        "jacamar",
				nonFlagArgs: []string{"signal", "SIGTERM", "123"},
			},
			assertStr: func(t *testing.T, s string) {
				assert.Equal(t, s, missingSub)
			},
		},
		"signal valid": {
			a: argumenter{
				name:        "jacamar",
				nonFlagArgs: []string{"signal", "SIGTERM", "456"},
			},
			c: &ConcreteArgs{},
			assertArgs: func(t *testing.T, args *ConcreteArgs) {
				assert.NotNil(t, args.Signal)
				assert.Equal(t, args.Signal.Signal, "SIGTERM")
				assert.Equal(t, args.Signal.PID, "456")
			},
		},
		"signal help": {
			a: argumenter{
				name:        "jacamar",
				nonFlagArgs: []string{"signal", "--help"},
			},
			assertStr: func(t *testing.T, s string) {
				assert.Equal(t, "Usage: jacamar [options] signal SIGNAL PID\n", s)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.a.subCommands(tt.c)

			if tt.assertStr != nil {
				tt.assertStr(t, got)
			}
			if tt.assertArgs != nil {
				tt.assertArgs(t, tt.c)
			}
		})
	}
}

func Test_programName(t *testing.T) {
	tests := map[string]struct {
		auth     bool
		args     []string
		wantName string
	}{
		"default jacamar-auth": {
			auth:     true,
			wantName: "jacamar-auth",
		},
		"default jacamar": {
			wantName: "jacamar",
		},
		"custom name observed": {
			args:     []string{"/custom/path/application", "--version"},
			wantName: "application",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if gotName := programName(tt.auth, tt.args); gotName != tt.wantName {
				t.Errorf("programName() = %v, want %v", gotName, tt.wantName)
			}
		})
	}
}

func TestResolve(t *testing.T) {
	t.Run("jacamar-auth run", func(t *testing.T) {
		os.Args = []string{
			"/opt/jacamar/bin/jacamar-auth",
			"--unobfuscated",
			"run",
			"SCRIPT",
			"STAGE",
		}

		got := Resolve(true)

		assert.NotNil(t, got.Run)
		assert.True(t, got.UnobfuscatedError)
		assert.Equal(t, got.Run, &RunCmd{
			Script: "SCRIPT",
			Stage:  "STAGE",
		})
	})
}

func Test_initFlags(t *testing.T) {
	tests := map[string]struct {
		a        *argumenter
		assertFl func(*testing.T, []fl)
	}{
		"no-auth": {
			a: &argumenter{
				auth: false,
			},
			assertFl: func(t *testing.T, f []fl) {
				assert.Len(t, f, 3)
			},
		},
		"auth": {
			a: &argumenter{
				auth: true,
			},
			assertFl: func(t *testing.T, f []fl) {
				assert.Len(t, f, 3)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := initFlags(tt.a)

			if tt.assertFl != nil {
				tt.assertFl(t, got)
			}
		})
	}
}

func Test_argumenter_parse(t *testing.T) {
	tests := map[string]struct {
		a                  argumenter
		assertConcreteArgs func(*testing.T, ConcreteArgs)
	}{
		"help option": {
			a: argumenter{
				helpOpt:  true,
				zeroExit: func() {},
				errExit:  func() { assert.True(t, false) },
			},
			assertConcreteArgs: func(t *testing.T, args ConcreteArgs) {
				assert.Empty(t, args)
			},
		},
		"version option": {
			a: argumenter{
				verOpt:   true,
				zeroExit: func() {},
				errExit:  func() { assert.True(t, false) },
			},
			assertConcreteArgs: func(t *testing.T, args ConcreteArgs) {
				assert.Empty(t, args)
			},
		},
		"subcommand error": {
			a: argumenter{
				zeroExit:    func() { assert.True(t, false) },
				errExit:     func() {},
				nonFlagArgs: []string{"example"},
			},
			assertConcreteArgs: func(t *testing.T, args ConcreteArgs) {
				assert.Empty(t, args)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := tt.a.parse()

			if tt.assertConcreteArgs != nil {
				tt.assertConcreteArgs(t, got)
			}
		})
	}
}
