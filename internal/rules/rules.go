// Package rules maintains established validation for shared aspects relating
// to either Jacamar specifically or the ECP CI effort as a whole. All checks
// that have been defined realize the validator.Func interface for the
// github.com/go-playground/validator/v10 and are meant to be a component in
// any validation strategy.
package rules

import (
	"path/filepath"
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

const (
	// Observe 8KB size maximum fallback for any check. Most can enforce
	// lower values if identified (nginx.org/en/docs/http/ngx_http_core_module.html).
	maxHeaderKB = 8192
)

// CheckUsername optionally examines the provided username for validity based upon
// the GitLab server requirements while observing potentially egregious unix
// characters. Implements validator.Func for String types only.
func CheckUsername(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	} else if v.Field().String() == "" {
		return true
	}

	matched, _ := regexp.MatchString(
		`^([a-zA-Z0-9][a-zA-Z0-9._-]{0,254})$`,
		v.Field().String(),
	)
	return matched
}

// CheckSHA256 ensures the string matches the structure of a SHA256 checksum,
// implements validator.Func for string type.
func CheckSHA256(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	matched, _ := regexp.MatchString(`^[A-Fa-f0-9]{64}$`, v.Field().String())

	return matched
}

// CheckProjectPath ensures the values adheres to expectations of a GitLab group/project.,
// implements validator.Func. Path can contain only letters, digits, '_', '-' and '.'.
// Cannot start with '-', end in '.git' or end in '.atom'.
func CheckProjectPath(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()
	if strings.HasPrefix(str, "-") ||
		strings.HasSuffix(str, ".git") ||
		strings.HasSuffix(str, ".atom") {
		return false
	} else if maximumHeader([]byte(str)) {
		return false
	}

	matched, _ := regexp.MatchString(`^[a-zA-Z0-9][a-zA-Z0-9-_./]+$`, str)

	return matched
}

// CheckGitLabToken optionally ensures a potential GitLab job/personal/deploy token is present
// and potentially invalid web characters are not included. The length of the token is not
// guaranteed to correctly match all potentially token types.
// Implements validator.Func for String types only.
func CheckGitLabToken(v validator.FieldLevel) bool {
	// Pass validation to generic auth token to avoid expectant modifications
	// to a GitLab generated token for the time being.
	return CheckAuthToken(v)
}

// CheckAuthToken optionally ensures a valid generic authorization token has been
// supplied that contains no potentially malicious characters and generic maximum
// header length. Implements validator.Func for String types only.
func CheckAuthToken(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	if maximumHeader([]byte(v.Field().String())) {
		return false
	} else if v.Field().String() == "" {
		return true
	}

	matched, _ := regexp.MatchString(
		`^[a-zA-Z0-9-_][a-zA-Z0-9-_.~]*$`,
		v.Field().String(),
	)

	return matched
}

// CheckJWT provide basic check of characters found in a valid JSON Web Token,
// implements validator.Func.
func CheckJWT(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	if maximumHeader([]byte(v.Field().String())) {
		return false
	}

	matched, _ := regexp.MatchString(
		`^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+$`,
		v.Field().String(),
	)

	return matched
}

// CheckDirectory verifies the validity of an expected Unix path without checking for
// its existence on the system. Non-absolute paths are allowed.
// Implements validator.Func for String types only.
func CheckDirectory(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()
	if len(str) == 0 || len([]byte(str)) > 4096 {
		return false
	}
	matched, _ := regexp.MatchString(`^(/[\w\-.^ ]+)+/?$`, str)

	return matched
}

// CheckQualifiedDirectory verifies the validity of an expected Unix path without checking
// for  its existence on the system. Absolute and fully qualified (filepath.Clean should not
// be required by provided value) is required. Implements validator.Func for String types only.
func CheckQualifiedDirectory(v validator.FieldLevel) bool {
	matched := CheckDirectory(v)

	return matched && cleanPath(v.Field().String())
}

// CheckUnexpandedDirectory identify if a valid directory is found with potentially
// un-expanded keys allowed, without checking for its existence on the system or required
// it be fully qualified. Implements validator.Func for String types only.
func CheckUnexpandedDirectory(v validator.FieldLevel) bool {
	if v.Field().Kind() != reflect.String {
		return false
	}

	str := v.Field().String()
	if len(str) == 0 || len([]byte(str)) > 4096 {
		return false
	}
	matched, _ := regexp.MatchString(`^(/?[\w\-.^${}]+)+/?$`, str)

	return matched
}

func cleanPath(path string) bool {
	// Legitimate cases where trailing "/" suffix is expected.
	str := strings.TrimSuffix(path, "/")

	if !filepath.IsAbs(str) || str != filepath.Clean(str) {
		return false
	}

	return true
}

func maximumHeader(b []byte) bool {
	// This is not strictly true for all potentially instance, but does at least set a
	// ceiling for otherwise uncapped values.
	return len(b) > maxHeaderKB
}
