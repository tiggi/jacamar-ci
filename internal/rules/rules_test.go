package rules_test

import (
	"testing"

	"github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/rules"
)

const (
	maxHeaderKB = 8192
)

func makeIllegalStringLength() string {
	b := make([]byte, maxHeaderKB+1)
	for i := range b {
		b[i] = "a"[0]
	}
	return string(b)
}

func TestCheckUserName(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("username", rules.CheckUsername)

	type myTest struct {
		Name string `validate:"username"`
	}

	tests := map[string]struct {
		username string
		wantErr  bool
	}{
		"empty": {
			username: "",
			wantErr:  false,
		},
		"greater than 255 character": {
			username: "abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123abcdefghijklmnopqrstuvwxyz7890123",
			wantErr:  true,
		},
		"hyphen in middle": {
			username: "user-name01",
			wantErr:  false,
		},
		"alphanumeric": {
			username: "user123",
			wantErr:  false,
		},
		"underscore in middle": {
			username: "user_name01",
			wantErr:  false,
		},
		"space in middle": {
			username: "user name01",
			wantErr:  true,
		},
		"bash command": {
			username: "user$(env)",
			wantErr:  true,
		},
		"period starting": {
			username: ".username",
			wantErr:  true,
		},
		"period in middle": {
			username: "user.name",
			wantErr:  false,
		},
		"underscore starting": {
			username: "_username",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Name: tt.username,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckUsername() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"username"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestCheckSHA256(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("sha256", rules.CheckSHA256)

	type myTest struct {
		Sha256 string `validate:"sha256"`
	}

	tests := map[string]struct {
		checksum string
		wantErr  bool
	}{
		"empty": {
			checksum: "",
			wantErr:  true,
		},
		"valid sha256": {
			checksum: "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			wantErr:  false,
		},
		"invalid characters": {
			checksum: "$(hello)",
			wantErr:  true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Sha256: tt.checksum,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckSHA256() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"sha256"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestProjectPath(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("projectPath", rules.CheckProjectPath)

	type myTest struct {
		Path string `validate:"projectPath"`
	}

	tests := map[string]struct {
		path    string
		wantErr bool
	}{
		"empty": {
			path:    "",
			wantErr: true,
		},
		"end with .git": {
			path:    "group/project.git",
			wantErr: true,
		},
		"end with .atom": {
			path:    "group/project.atom",
			wantErr: true,
		},
		"begin with -": {
			path:    "-group/project",
			wantErr: true,
		},
		"invalid characters": {
			path:    "group/$(project)/1",
			wantErr: true,
		},
		"valid project path": {
			path:    "group/sub-group.1/my_project",
			wantErr: false,
		},
		"invalid length": {
			path:    makeIllegalStringLength(),
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Path: tt.path,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckProjectPath() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"projectPath"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestGitLabToken(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("gitlabToken", rules.CheckGitLabToken)

	type myTest struct {
		Token string `validate:"gitlabToken"`
	}

	tests := map[string]struct {
		token   string
		wantErr bool
	}{
		"empty": {
			token:   "",
			wantErr: false,
		},
		"valid, alphanumeric": {
			token:   "EArXoCj8gmiAa8eZzPiR",
			wantErr: false,
		},
		"valid, underscore encountered": {
			token:   "PG_buQjszuoCoxxayMZG",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Token: tt.token,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckGitLabToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"gitlabToken"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestAuthToken(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("authToken", rules.CheckAuthToken)

	type myTest struct {
		Token string `validate:"authToken"`
	}

	tests := map[string]struct {
		token   string
		wantErr bool
	}{
		"empty": {
			token:   "",
			wantErr: false,
		},
		"valid, alphanumeric + underscore + hyphen": {
			token:   "EArXoCj8g_miAa8e-ZzP_iR",
			wantErr: false,
		},
		"period starting": {
			token:   ".EArXoCj8g_miAa8e-ZzP_iR",
			wantErr: true,
		},
		"illegal character": {
			token:   "test$(%)string",
			wantErr: true,
		},
		"invalid length": {
			token:   makeIllegalStringLength(),
			wantErr: true,
		},
		"valid random jwt": {
			token:   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			wantErr: false,
		},
		"valid tilde used": {
			token:   "token~123456",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				Token: tt.token,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"authToken"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestJWT(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("jwt", rules.CheckJWT)

	type myTest struct {
		JWT string `validate:"jwt"`
	}

	tests := map[string]struct {
		jwt     string
		wantErr bool
	}{
		"empty": {
			jwt:     "",
			wantErr: true,
		},
		"invalid length": {
			jwt:     makeIllegalStringLength(),
			wantErr: true,
		},
		"valid random jwt": {
			jwt:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			wantErr: false,
		},
		"missing segment": {
			jwt:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ",
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				JWT: tt.jwt,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckAuthToken() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"jwt"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestDirectory(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("directory", rules.CheckDirectory)

	type myTest struct {
		File string `validate:"directory"`
	}

	tests := map[string]struct {
		file    string
		wantErr bool
	}{
		"empty": {
			file:    "",
			wantErr: true,
		},
		"simple filepath + script": {
			file:    "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			file:    "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"unexpanded variable": {
			file:    "/dir/$TEST/",
			wantErr: true,
		},
		"bash command": {
			file:    "$(env)",
			wantErr: true,
		},
		"home directory": {
			file:    "~/.test",
			wantErr: true,
		},
		".. elements in path": {
			file:    "/folder/user/../test",
			wantErr: false,
		},
		"backslash": {
			file:    "/test\n\\/folder",
			wantErr: true,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				File: tt.file,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"directory"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestQualifiedDirectory(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("qualifiedDirectory", rules.CheckQualifiedDirectory)

	type myTest struct {
		File string `validate:"qualifiedDirectory"`
	}

	tests := map[string]struct {
		file    string
		wantErr bool
	}{
		"empty": {
			file:    "",
			wantErr: true,
		},
		".. elements in path": {
			file:    "/home/user/../test",
			wantErr: true,
		},
		"unexpanded variable": {
			file:    "/dir/$TEST/",
			wantErr: true,
		},
		"simple filepath + script": {
			file:    "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			file:    "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				File: tt.file,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckAbsDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"qualifiedDirectory"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}

func TestCheckUnexpandedDirectory(t *testing.T) {
	validate := validator.New()
	_ = validate.RegisterValidation("unexpandedDirectory", rules.CheckUnexpandedDirectory)

	type myTest struct {
		File string `validate:"unexpandedDirectory"`
	}

	tests := map[string]struct {
		file    string
		wantErr bool
	}{
		"empty": {
			file:    "",
			wantErr: true,
		},
		"simple filepath + script": {
			file:    "/dir/test/file.bash",
			wantErr: false,
		},
		"filepath ony": {
			file:    "/dir/test/some.dir/test_123/new-example/",
			wantErr: false,
		},
		"unexpanded variable": {
			file:    "/dir/$TEST/",
			wantErr: false,
		},
		"multiple unexpanded variables": {
			file:    "/dir/$TEST/something/${USER}",
			wantErr: false,
		},
		"bash command": {
			file:    "$(env)",
			wantErr: true,
		},
		"home directory": {
			file:    "~/.test",
			wantErr: true,
		},
		".. elements in path": {
			// Not enforced in this rule set.
			file:    "/folder/user/../test",
			wantErr: false,
		},
		"home variable": {
			file:    "$HOME/.runner",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			v := myTest{
				File: tt.file,
			}

			if err := validate.Struct(v); (err != nil) != tt.wantErr {
				t.Errorf("CheckUnexpandedDirectory() return = %v, want %v", err, tt.wantErr)
			}
		})
	}

	t.Run("invalid variable type", func(t *testing.T) {
		type typeTest struct {
			Token int `validate:"unexpandedDirectory"`
		}
		err := validate.Struct(typeTest{
			Token: 1,
		})
		assert.Error(t, err)
	})
}
