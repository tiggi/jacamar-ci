package usertools

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"syscall"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type dirInfo struct {
	path     string
	notFound bool
	info     os.FileInfo
	perm     os.FileMode
	umask    os.FileMode
}

// CreateDirectories manages the CI directory creation process for a validated user
// and target directory structures. This should only be initiated after the user
// authorization process has been completed successfully and all user context can be
// trusted. The creation process will enforce strict directory permission and ownership
// criteria on all directories (base, builds cache, and script) against the current user.
func CreateDirectories(state envparser.StatefulEnv, opt configure.General) error {
	directories := []string{
		state.BaseDir,
		state.BuildsDir,
		state.CacheDir,
		state.ScriptDir,
	}

	for _, path := range directories {
		dir, err := identifyDirectory(path, opt)
		if err != nil {
			return err
		}

		// It is worth verifying the permissions for each and every directory
		// to account for potential
		if err = dir.verifyBasePermissions(); err != nil {
			return err
		}
		if err = dir.mkdir(); err != nil {
			return err
		}
	}

	return nil
}

func identifyDirectory(path string, opt configure.General) (dirInfo, error) {
	if path == "" {
		return dirInfo{}, errors.New("no base directory defined, verify runner configuration")
	}

	resolvedPath, err := filepath.EvalSymlinks(path)
	if err != nil && !os.IsNotExist(err) {
		return dirInfo{}, fmt.Errorf("failed to resolve target directory: %w", err)
	}

	dir := dirInfo{}
	if resolvedPath != "" {
		dir.path = resolvedPath
	} else {
		dir.path = path
	}

	info, err := os.Stat(dir.path)
	if err != nil && !os.IsNotExist(err) {
		return dir, fmt.Errorf("failed to identify target directory: %w", err)
	} else if os.IsNotExist(err) {
		dir.notFound = true
	}
	dir.info = info

	if opt.GroupPermissions && opt.FFGroupPermissions {
		dir.perm = GroupPermissions
		dir.umask = GroupUmask
	} else {
		dir.perm = OwnerPermissions
		dir.umask = OwnerUmask
	}

	return dir, nil
}

var curUserFactory = authuser.CurrentUser

// verifyBasePermissions ensures that if an existing directory is found, permissions
// and ownership are validated against the current user.
func (d dirInfo) verifyBasePermissions() error {
	if d.notFound || d.info == nil {
		// If the directory is not found we have nothing to verify.
		return nil
	}

	usr, err := curUserFactory()
	if err != nil {
		return fmt.Errorf("error while attempting to identifying current user: %w", err)
	}

	uid, _ := strconv.Atoi(usr.Uid)
	if d.info.Sys().(*syscall.Stat_t).Uid != uint32(uid) {
		return fmt.Errorf(
			"invalid ownership detected on directory %s, this must be manually addressed",
			d.path,
		)
	}

	// check that this is definitely a directory
	if !d.info.IsDir() {
		return fmt.Errorf(
			"%s is not a directory, this must be manually addressed",
			d.path,
		)
	}

	// check the lower 6 bits and fail if any are set,
	// unless GroupPermissions is enabled, where group permissions may be set to read/execute access
	// the lower 6 bits represent group/world permissions (see https://pkg.go.dev/io/fs#FileMode)
	if d.info.Mode()&d.umask != 0 {
		return fmt.Errorf(
			"invalid permissions for directory %s (currently %s, need to be %v), this must be manually addressed",
			d.path,
			d.info.Mode().String(),
			d.perm,
		)
	}

	return nil
}

// mkdir creates every directory provided with the package's defined dirPermissions.
func (d dirInfo) mkdir() error {
	if err := os.MkdirAll(d.path, d.perm); err != nil {
		return err
	}

	// Enforce base permissions on directory creation for user in case there are conflicts
	// with umask or other system defaults.
	return os.Chmod(d.path, d.perm)
}
