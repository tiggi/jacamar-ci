package usertools

import (
	"errors"
	"fmt"
	"os"
	"os/user"
	"strconv"
	"syscall"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type dirTests struct {
	dir   dirInfo
	state envparser.StatefulEnv
	opt   configure.General

	mockCurUsrFactory func() (*user.User, error)

	assertError    func(*testing.T, error)
	assertPath     func(*testing.T, string)
	assertCreation func(*testing.T, []string)
}

func mocksFunctional(path string) envparser.StatefulEnv {
	return envparser.StatefulEnv{
		BaseDir:   path,
		BuildsDir: path + "/builds",
		CacheDir:  path + "/cache",
		ScriptDir: path + "/script",
	}
}

func mocksEmptyBase() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		BaseDir:   "",
		BuildsDir: "/var/tmp/ci/builds",
		CacheDir:  "/var/tmp/ci/cache",
		ScriptDir: "/var/tmp/ci/script",
	}
}

func mocksInvalidBuilds() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		BaseDir:   "/var/tmp/ci",
		BuildsDir: "/var/tmp",
		CacheDir:  "/var/tmp/ci/cache",
		ScriptDir: "/var/tmp/ci/script",
	}
}

func TestCreateDirectories(t *testing.T) {
	baseDirOwnerPerm := t.TempDir() + "/ci-dir"
	_ = os.Chmod(baseDirOwnerPerm, 0700)

	baseDirGroupPerm := t.TempDir() + "/ci-dir"
	_ = os.Chmod(baseDirGroupPerm, 0750)

	tests := map[string]dirTests{
		"undefined basedir": {
			state: mocksEmptyBase(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no base directory defined, verify runner configuration")
			},
		},
		"functional directory creation process (owner permissions only)": {
			state: mocksFunctional(baseDirOwnerPerm),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCreation: func(t *testing.T, dirs []string) {
				for _, d := range dirs {
					info, err := os.Stat(d)
					assert.NoError(t, err)
					assert.Equal(t, "drwx------", info.Mode().String())
				}
			},
		},
		"functional directory creation process (owner and group permissions)": {
			state: mocksFunctional(baseDirGroupPerm),
			opt: configure.General{
				GroupPermissions:   true,
				FFGroupPermissions: true,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertCreation: func(t *testing.T, dirs []string) {
				for _, d := range dirs {
					info, err := os.Stat(d)
					assert.NoError(t, err)
					assert.Equal(t, "drwxr-x---", info.Mode().String())
				}
			},
		},
		"invalid custom builds directory defined": {
			state: mocksInvalidBuilds(),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := CreateDirectories(tt.state, tt.opt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertCreation != nil {
				dirs := []string{
					tt.state.BaseDir,
					tt.state.BuildsDir,
					tt.state.CacheDir,
					tt.state.ScriptDir,
				}
				tt.assertCreation(t, dirs)
			}
		})
	}
}

func Test_verifyBasePermissions(t *testing.T) {
	goodDir, _ := os.MkdirTemp(t.TempDir(), "ci-permissions")
	goodDirStat, _ := os.Stat(goodDir)

	badPerm, _ := os.MkdirTemp(t.TempDir(), "bad-permissions")
	_ = os.Chmod(badPerm, 0706) // Bypass syscall to umask
	badPermStat, _ := os.Stat(badPerm)

	// allow setgid
	goodOwnerPerm, _ := os.MkdirTemp(t.TempDir(), "good-owner-permissions")
	_ = os.Chmod(goodOwnerPerm, OwnerPermissions|os.ModeSetgid) // Bypass syscall to umask
	goodOwnerPermStat, _ := os.Stat(goodOwnerPerm)

	goodGroupPerm, _ := os.MkdirTemp(t.TempDir(), "good-group-permissions")
	_ = os.Chmod(goodGroupPerm, GroupPermissions|os.ModeSetgid) // Bypass syscall to umask
	goodGroupPermStat, _ := os.Stat(goodGroupPerm)

	// create a temporary file to make sure it fails the directory test
	badFile, _ := os.CreateTemp(t.TempDir(), "not-a-dir")
	badFileStat, _ := badFile.Stat()

	tests := map[string]dirTests{
		"correct directory permissions found": {
			dir: dirInfo{
				path:  goodDir,
				info:  goodDirStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"file provided": {
			dir: dirInfo{
				path:  badFile.Name(),
				info:  badFileStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					fmt.Sprintf(
						"%s is not a directory, this must be manually addressed",
						badFile.Name(),
					),
				)
			},
		},
		"invalid permissions (!= 700, != 750) found on directory": {
			dir: dirInfo{
				path:  badPerm,
				info:  badPermStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"undefined file info pointer": {
			dir: dirInfo{
				path:  goodDir,
				info:  nil,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"invalid current user detected": {
			dir: dirInfo{
				path:  goodDir,
				info:  goodDirStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			mockCurUsrFactory: func() (*user.User, error) {
				return nil, errors.New("no current user")
			},
		},
		"invalid ownership detected": {
			dir: dirInfo{
				path:  goodDir,
				info:  goodDirStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					fmt.Sprintf(
						"invalid ownership detected on directory %s, this must be manually addressed",
						goodDir,
					),
				)
			},
			mockCurUsrFactory: func() (*user.User, error) {
				return &user.User{Uid: "42"}, nil
			},
		},
		"allow setgid for owner only permissions": {
			dir: dirInfo{
				path:  goodOwnerPerm,
				info:  goodOwnerPermStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"allow setgid for owner and group permissions": {
			opt: configure.General{
				GroupPermissions:   true,
				FFGroupPermissions: true,
			},
			dir: dirInfo{
				path:  goodGroupPerm,
				info:  goodGroupPermStat,
				perm:  GroupPermissions,
				umask: GroupUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	// Configuration issue currently experienced with OpenShift testing deployment.
	usr, _ := user.Current()
	homeStat, _ := os.Stat(usr.HomeDir)
	if usr.HomeDir != "/tmp" {
		tests["home directory specified"] = dirTests{
			dir: dirInfo{
				path:  usr.HomeDir,
				info:  homeStat,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		}
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockCurUsrFactory != nil {
				curUserFactory = tt.mockCurUsrFactory
			} else {
				curUserFactory = authuser.CurrentUser
			}

			err := tt.dir.verifyBasePermissions()

			// reset factory
			curUserFactory = authuser.CurrentUser

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_mkdir(t *testing.T) {
	tarPath := t.TempDir()

	tests := map[string]dirTests{
		"target directory successfully created with owner only permissions": {
			dir: dirInfo{
				path:  tarPath,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertPath: func(t *testing.T, s string) {
				info, _ := os.Stat(s)
				assert.True(t, info.Mode().IsDir())
				assert.Equal(t, "drwx------", info.Mode().String())

				curUser, _ := user.Current()
				stats := info.Sys().(*syscall.Stat_t)
				assert.Equal(t, curUser.Uid, strconv.FormatUint(uint64(stats.Uid), 10))
				assert.Equal(t, curUser.Gid, strconv.FormatUint(uint64(stats.Gid), 10))
			},
		},
		"target directory successfully created with owner and group permissions": {
			opt: configure.General{
				GroupPermissions:   true,
				FFGroupPermissions: true,
			},
			dir: dirInfo{
				path:  tarPath,
				perm:  GroupPermissions,
				umask: GroupUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertPath: func(t *testing.T, s string) {
				info, _ := os.Stat(s)
				assert.True(t, info.Mode().IsDir())
				assert.Equal(t, "drwxr-x---", info.Mode().String())

				curUser, _ := user.Current()
				stats := info.Sys().(*syscall.Stat_t)
				assert.Equal(t, curUser.Uid, strconv.FormatUint(uint64(stats.Uid), 10))
				assert.Equal(t, curUser.Gid, strconv.FormatUint(uint64(stats.Gid), 10))
			},
		},
		"target directory already exists with owner only permissions": {
			dir: dirInfo{
				path:  tarPath,
				perm:  OwnerPermissions,
				umask: OwnerUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertPath: func(t *testing.T, s string) {
				info, _ := os.Stat(s)
				assert.True(t, info.Mode().IsDir())
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
		"target directory already exists with owner and group permissions": {
			opt: configure.General{
				GroupPermissions:   true,
				FFGroupPermissions: true,
			},
			dir: dirInfo{
				path:  tarPath,
				perm:  GroupPermissions,
				umask: GroupUmask,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertPath: func(t *testing.T, s string) {
				info, _ := os.Stat(s)
				assert.True(t, info.Mode().IsDir())
				assert.Equal(t, "drwxr-x---", info.Mode().String())
			},
		},
		"mkdir error observed": {
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.dir.mkdir()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertPath != nil {
				tt.assertPath(t, tt.dir.path)
			}
		})
	}
}
