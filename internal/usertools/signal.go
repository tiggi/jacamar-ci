package usertools

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

// ManageSignal is a wrapper around a call to kill(2) and handles all potential timeouts/cleanup.
func ManageSignal(msg logging.Messenger, sCmd arguments.SignalCmd, sysErr, exitZero func()) {
	// default to using kill(2)
	if err := processSignal(sCmd, syscall.Kill); err != nil {
		msg.Error(err.Error())
		sysErr()
	}
	exitZero()
}

func processSignal(sCmd arguments.SignalCmd, kill func(int, syscall.Signal) error) error {
	sig := make(chan error, 1)
	ch := make(chan bool, 1)

	// If we are in a case where a SIGNAL is not being received by the subprocess then there
	// may be other issues, we should take steps to halt any actions that too long.
	time.AfterFunc(15*time.Second, func() { ch <- false })

	go func() {
		signum, pid, err := identify(sCmd.Signal, sCmd.PID)
		if err != nil {
			sig <- err
			return
		}

		sig <- kill(pid, signum)
	}()

	select {
	case <-ch:
		return errors.New("command timeout encountered")
	case err := <-sig:
		if err != nil {
			return err
		}
	}

	return nil
}

func identify(signal, strPid string) (signum syscall.Signal, pid int, err error) {
	pid, err = strconv.Atoi(strPid)
	if err != nil {
		return
	}

	switch strings.ToLower(signal) {
	case "sigkill":
		signum = syscall.SIGKILL
	case "sigterm":
		signum = syscall.SIGTERM
	default:
		err = fmt.Errorf("invalid signal %s defined (SIGKILL|SIGTERM)", signal)
	}

	return
}
