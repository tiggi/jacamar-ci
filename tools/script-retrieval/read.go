package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		log.Fatal("missing script as argument")
	}

	b, err := os.ReadFile(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}

	for _, line := range strings.Split(string(b), "\n") {
		if strings.HasPrefix(line, ": |") {
			for _, v := range strings.Split(line, "\\n") {
				fmt.Println(v)
			}
		}
	}
}
