# Script Retrieval

Use the supplied `run.bash` as the `run_exec` script for
a custom executor, coupled with the provided `gitlab-ci.yml`
file. This will create a clean version of each execution
script under `/var/tmp/<runnerVersion>`.

Please note, we **strongly** advise running this script in a
virtualizes/container/private environment to avoid exposing
any tokens on shared resources.

## Runner Config Example

```toml
[[runners]]
  name = "script retrieval"
  url = "https://gitlab.example.com/"
  token = "TOKEN"
  executor = "custom"
  builds_dir = "/var/tmp"
  cache_dir = "/var/tmp"
  [runners.custom]
    run_exec = "/etc/gitlab-runner/run.bash"
```
