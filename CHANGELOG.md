## v0.12.1 (September 23, 2022)

### Admin Changes

* Support `auth.job_token_scope_enforced` to ensure
  [limited job token access](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#limit-gitlab-cicd-job-token-access)
  ([!380](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/380))

### Bug & Development Fixes

* Update gljobctx-go *v0.3.1* to support `/-/jwks` retry
  ([!387](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/387))

## v0.12.0 (August 22, 2022)

### Admin Changes

* Updated seccomp support and logging
  ([!360](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/360))
* Make *ioctl* rule optional and prevent invalid setuid deployments
  ([!351](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/351))
* Optional static build directory configuration
  ([!361](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/361))
* Increase default `kill_timeout` to 2 minutes
  ([!376](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/376))
* `make release-binaries` for binary only RPM
  ([!369](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/369))
* Add current executable path to `jacamar` command creation
  ([!371](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/371))
* Log restricted privileges that conflict with downscoping
  ([!356](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/356))

### Bug & Development Fixes

* Establish testing structure for latest runner compatability
  ([!363](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/363),
  [!354](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/354))
* Correct PBS signal handling and expand logging/testing
  ([!367](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/367))
* Handle response during `--no-auth` deployment
  ([!373](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/373))
* Limited buffer ReturnOutput via downscoped command
  ([!370](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/370))
* Support for obtaining software bill of materials
  ([!366](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/366))
* Refactor `AbstractExecutor` structure
  ([!349](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/349))
* Update CentOS 7 builder image
  ([!362](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/362))
* CI/CD improvements and fixes
  ([!368](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/368),
  [!365](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/365),
  [!359](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/359))
* Tool for seccomp configuration testing
  ([!358](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/358))

## v0.11.0 (April 29, 2022)

### User Changes

* Provide `JACAMAR_CI_SHELL` variable to Bash login
  ([!345](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/345))
* Illegal argument result in job failure
  ([!326](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/326))
* Corrected scheduler file tailing
  ([!335](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/335),
  [!340](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/340))

### Admin Changes

* Update JWT package and support both V1 and V2
  ([!342](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/342))
* PBS executor more closely aligns with Cobalt and expanded testing
  ([!348](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/348),
  [!346](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/346))

### Bug & Development Fixes

* Upgraded to Go version *1.18.1* and dependencies
  ([!338](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/338))
* Removed un-supported federation options
  ([!337](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/337))
* CI file organization and testing coverage expanded
  ([!332](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/332),
  [!336](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/336),
  [!347](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/347))

## v0.10.2 (March 9, 2022)

### Admin Changes

* Add `gitlab-runner` group for RPM w/capabilities to address
  missing GID in select cases
  ([!328](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/328),
  [!329](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/329))

## v0.10.1 (February 21, 2022)

### Admin Changes

* Move `setuid`/`setgid` Seccomp to `limit_setuid` config option
  ([!323](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/323))

## v0.10.0 (January 31, 2022)

### Admin Changes

* JSON Web Token package updates and configurable expiration delay
  ([!301](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/301))
* Added option for unrestricted command line arguments
  ([!294](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/294))
* Improved default seccomp and added plugin feature flag support
  ([!286](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/286))
* Manage signals in non-root for `setuid` with a constructed
  `jacamar signal SIGNAL PID` command
  ([!316](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/316))
* Added system logging when `jacamar-auth` encounters `SIGTERM`
  ([!305](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/305))
* Timeout established for `jacamar` and `jacamar-auth` from job response
  ([!311](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/311))

### Bug & Development Fixes

* Error handling enhancements for internal API
  ([!302](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/302))
* GitLab license scanning to verify compliance
  ([!298](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/298))
* Minimize argument requirements while keeping existing support
  ([!288](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/288))
* `Signaler` interface added to command structure
  ([!313](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/313))
* Removed watcher package in favor of more minimal system packages
  ([!300](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/300))
* Improved Pavilion test result output
  ([!299](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/299))
* Updated all build tag formats for Go *1.17*
  ([!293](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/293))
* Clarified function name and documentation in `verifycaps` package
  ([!287](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/287))
* Added basic tests for NERSC instance
  ([!292](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/292))
* `VERSION` provided during container builds
  ([!321](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/321))
* Corrected Slurm test image and test syntax
  ([!319](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/319))
* Verify ``go.mod`` changes are committed during CI
  ([!318](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/318))
* Mock `/jobs` endpoint for Pavilion test scripts
  ([!317](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/317))

## v0.9.1 (December 13, 2021)

### Bug & Development Fixes

* Upgrade to Go version 1.17.5
  ([!303](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/303))

## v0.9.0 (October 22, 2021)

### Admin Changes

* Incorporated better RPM procedures into updated spec file
  ([!256](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/256))
* Expand trusted variables (`$HOME` and `$USER`) in `data_dir`
  ([!272](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/272))
* Removed support for `variable_data_dir` configuration
  ([!268](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/268))
* Properly handle resolution of symlinks in `data_dir` - `@tgmachina`
  ([!269](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/269))
* Bot accounts blocked from runner by default with error message
  ([!266](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/266))
* Support added for `tls-ca-file` configuration for GitLab HTML requests
  ([!265](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/265))
* Added `JWT_ISS` (issuer) to RunAs validation environment
  ([!276](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/276))
* Batch submission can be affected by `env_keys` config
  ([!278](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/278))
* Unrecognized configuration keys logged as warnings
  ([!267](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/267))

### User Changes

* Correct directory permission provided in related error message - `@tgmachina`
  ([!274](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/274))
* Expand trusted variables (`$HOME` and `$USER`) in `CUSTOM_CI_BUILDS_DIR`
  ([!271](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/271))
* Invalid pipeline source error conveyed in job output
  ([!245](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/245))

### Bug & Development Fixes

* Upgraded to Go version *1.17.2*
  ([!273](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/273))
* Added tool versioning via ASDF
  ([!259](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/259))
* Sudo args handled as slice for use by `execve(2)`
  ([!249](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/249))
* Removed all CI Token Broker support
  ([!242](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/242))
* Introduced `ff_custom_data_dir` configuration
  ([!271](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/271))
* Reviewed and updated multiple third-party dependencies
  ([!246](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/246))
* Improved flexibility of validation `rules` package while still
  enforcing security requirements
  ([!263](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/263))
* Added OpenSUSE test package during CI
  ([!275](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/275))
* Correctly allow `500` permissions with capabilities
  ([!280](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/280))
* Added Podman support for local container testing
  ([!255](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/255),
  [!254](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/254))
* JobID logged as integer value
  ([!237](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/237))
* Corrected handling of `jacamar_path` config with binary paths
  ([!257](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/257))

## v0.8.2 (September 23, 2021)

### Bug & Development Fixes

* Properly handle GitLab username during JWT validation
  ([!260](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/238))
* Added backup mechanism to help identify successful job in Cobalt
  ([!252](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/252))

## v0.8.1 (August 12, 2021)

### Admin Changes

* Improved `sudo` downscope option and target command directory
  ([!238](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/238)) -
  @joe-snyder

### Bug & Development Fixes

* Commands using `qstat` properly observe timeout/`command_delay`
  ([!239](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/239))
* Basic `sudo` integration testing via Pavilion
  ([!236](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/236))

## v0.8.0 (July 28, 2021)

### Admin Changes

* Functional with official runner release (14.1+) via the
  `JOB_RESPONSE_FILE` providing fully payload details
  ([!211](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/211))
* Added configurable allowlist for CI pipeline sources, only valid with
  server versions 14.0+
  ([!208](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/208))
* Optional RunAs environment configuration supplied to validation script
  ([!209](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/209))
* Configurable job message during `prepare_exec` stage
  ([!216](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/216))
* Align several logging keys in Jacamar CI with those found in the runner
  ([!218](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/218))
* Check for capabilities and permissions on the `jacamar-auth` binary
  that would be present issues if found in test environments
  ([!229](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/229))
* Error printed if `jacamar-auth` launched using invalid runner version
  ([!225](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/225))
* Allow setgid bits found in directory permissions
  ([!223](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/223))
* Relaxed RPM permissions and new runner capabilities RPM for select test deployments
  ([!221](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/221))
* Minimally re-packaged GitLab-Runner RPM with single binary
  ([!234](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/234))

### User Changes

* Added prepare message to self-document key elements of the configuration
  that an affect CI jobs
  ([!226](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/226))

### Bug & Development Fixes

* Added context to `clean_exec` errors to hint at likely cause of issues
  ([!217](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/217))
* Added mutex locking in appropriate command packages
  ([!210](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/210))
* New error logging messages added and removed config file debug
  ([!220](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/220))
* Validation used in JWT results (after signature/checksum) now uses `number`
  as opposed to `numeric`
  ([!212](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/212))
* Simplified ``Authorized`` interface for the `autherusr` package
  ([!199](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/199))
* Updated to Go release *1.16.6*
  ([!227](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/227))
* Improve RPM release process for 14.1+ runners
  ([!221](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/221))

## v0.7.3 (June 30, 2021)

### Admin Changes

* Remove unnecessary preparation checks causing failed jobs during the
  `prepare_exec` for scheduler with strict environment requirements
  ([!204](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/204))

### Bug & Development Fixes

* Updates to correct `Makefile` builds and improve readability with `make help`
  ([!198](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/198))

## v0.7.2 (June 18, 2021)

### Bug & Development Fixes

* Improved conditional seccomp filters used with `setuid` downscoping
  ([!198](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/196))
* Iterative expansion of info/debug logs to better step through key
  actions of the `jacamar-auth` application
  ([!195](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/195))

## v0.7.1 (June 10, 2021)

### Admin Changes

* Logging enhancements and correctly dial local syslog
  ([!188](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/188),
  [!178](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/178))
* Support overriding `data_dir` within RunAs workflow
  ([!187](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/187))
  
### Bug & Development Fixes

* Correct RPMs builds on RHEL 8/CentOS 8/Fedora
  ([!186](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/186))
* Updated to Go release *1.16.5*
  ([!189](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/189))
* Target `arm64` if platform `aarch64` in Spack package
  ([!190](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/190))
* Log JWT verification steps
  ([!174](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/174))
* Gather and log all RunAs validation script output
  ([!191](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/191))

## v0.7.0 (May 26, 2021)

### Admin Changes

* Support for variable `data_dir` expanded in user's environment
  ([!168](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/168))
* ProcessID captured during logging
  ([!173](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/173))
* Jacamar CI RPM is now relocatable
  ([!184](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/184))

### Bug & Development Fixes

* Added Jacamar CI logo
  ([!179](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/179))
* Interactions with GitLab's JWKS (JSON Web Key Set) handled
  ([!172](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/172))
* Supported GitLab-Runner (patched) version *13.12*
  ([!180](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/180))
* Extend seccomp functionality to support testing a block-all by default model
  ([!175](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/175))
* Basic high-level panic recovery
  ([!171](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/171))
* Support quick Go test of specific packages within Docker
  ([!176](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/176))
* Extended CI build artifact duration to 1 day
  ([!181](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/181))
* Updated runner generated test data scripts
  ([!182](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/182))

## v0.6.0 (April 29, 2021)

### Admin Changes

* Improved Slurm executor by utilizing `sbatch --wait`
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143),
  [!164](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/164))
* Extensive improvement to the Git command modification process
  ([!154](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/154))
* Cobalt corrections to address issues reported with version *0.5.0*
  ([!135](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/135),
  [!150](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/150))
* Remove support for `source_script` in the `[auth]` configuration
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143))
* Correctly build JWKS URL when server found in path
  ([!157](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/157))
* In order to use `jacamar` directly without `jacamar-auth`, the
  application will only function as expected with the
  `--no-auth` command, signifying no authorization is expected
  ([!160](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/160))

### Bug & Development Fixes

* Supported GitLab-Runner (patched) version *13.11* and *13.10*
  ([!161](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/161),
  [!138](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/138))
* The authorization workflow will now validate the supplied JSON
  Web Token at each stage of the job
  ([!147](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/147))
* Added support for `make rpm-docker` and `make runner-docker` commands
  to build related RPMs locally via Docker
  ([!138](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/138),
  [!137](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/137))
* The `internal/command` packages structure has been improved to better
  differentiate between dowscoping and standard execution methods
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143))
* The first steps have been taken to improve the `internal/authuser`
  packages structure, more work will follow
  ([!160](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/160))
* Added ``ModifyCmd(string, string...)`` to Runner and Commander interface
  ([!150](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/150))
* Added the project path as a stateful variable
  ([!153](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/153))
* Added support for `downscope = "su"` as it nearly follow the same
  command structure as `sudo`
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143))
* Added optional `downscope_env` to the `[auth]` configuration
  ([!149](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/149))
* Migrated Pavilion testing from `sudo` to `su`
  ([!143](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/143))
* Updated command `make test-docker` to align with other corrections
  ([!140](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/140/))
* Updates to directory identification to align structures across directories
  ([!148](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/148))
* Corrected ``command`` handling of goroutines causing blocking actions
  ([!165](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/165))
* Configurable command directory
  ([!162](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/162))

## v0.5.0 (April 05, 2021)

### Admin Changes

* Overhaul of supplementary group identification and expand testing
  ([!127](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/127))
* New downscope option `downscope = "sudo"` added which invokes a crafted
  `sudo su` command
  ([!103](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/103))
* Support for libseccomp added with configurable blocked system calls,
  and defaults established for `jacamar-auth`
  ([!119](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/119))
* Corrected `SIGTERM` monitoring and command context handling that led
  to high CPU usage across a number of deployments
  ([!117](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/117))
* Additional testing and error handling for Slurm
  ([!105](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/105))
* Improved Git ASKPASS credential creation and remove helper from local
  `.git/config` into `$CI_PROJECT_DIR/../.credential` folder
  ([!124](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/124))
* RunAs validation support override of the target `gitlab_account` used in
  conjunction with the CI Token Broker
  ([!122](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/122))
* Fully qualify `jacamar` application when generating downscope command
  ([!101](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/101))
* Improve support for `downscope = "none"` to allow single user deployment
  to leverage authorization level features
  ([!110](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/110))
* Identify Bash shell in command creation, with optional configuration support
  ([!107](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/107))
* Clarified error message associated with `data_dir` creation and expanded
  related testing
  ([!123](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/123))
* Updated Slurm job script with `--login` prior to execution
  ([!131](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/131))
* Create Slurm output files in advance of job submission as opposed to
  allowing `sbatch` to create its own
  ([!132](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/132))

### Bug & Development Fixes

* Allow hyphens in username rules
  ([!106](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/106))
* Updated all Pavilion2 tests
  ([!126](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/126),
  [!130](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/130),
  [!129](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/129))
* Support for Go 1.16
  ([!113](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/113))
* Improve bytes buffer for command execution
  ([!104](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/104))
* Removed support + usage of `jacamar-plugins`
  ([!115](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/115))
* Updated OLCF testing pipeline
  ([!120](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/120))
* Removed outdated ALCF testing pipeline and supporting files
  ([!112](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/112))
* Tests added to ensure `source_script` functionality
  ([!121](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/121))
* Test to verify credentials removal
  ([!111](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/111))
* Check broker token response for expected patterns
  ([!109](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/109))
* Remove translation test files upon completion
  ([!108](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/108))

## v0.4.2 (February 24, 2021)

### Admin Changes

* Added RPM to deploy `jacamar-auth` application with `CAP_SETUID`/`CAP_SETGID`
  ([!97](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/97))
* Configurable `root_dir_creation = true` in Jacamar-Auth supports
  creation of top level user owned directory by a privileged user
  ([!99](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/99))
* Updated supported GitLab-Runner (patched) to version *13.9.0*
  and removed federation patch
  ([!96](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/96))

### Bug & Development Fixes

* Target server host for script augmentation
  ([!98](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/98))
* Improved static analysis and linting during CI process
  ([!95](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/95),
  [!94](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/94))

## v0.4.1 (February 16, 2021)

### General Changes

* Added `CheckJWT` to the `rules` package
  ([!91](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/91))

### Admin Changes

* Support new enhanced broker registration requirements
  ([!93](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/93))
* Update supported GitLab-Runner (patched) to version *13.8.0*
  ([!90](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/90))

### Bug & Development Fixes

* Correctly remove `CI_JOB_JWT` from user environment
  ([!89](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/89))

## v0.4.0 (January 25, 2021)

### General Changes

* Migrated two internal packages to now support a public API for key
  functionality used across multiple EPC CI projects
  ([!58](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/58))
* Fixed case in which Cobalt/Slurm jobs did not observe NFS timeout/delay
  ([!81](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/81))

### Admin Changes

* The cleanup stage (`clean` subcommand) will now require the config file
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71))
* Integrated all Federation information/context into the RunAs validation
  ([!65](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/65),
  [!86](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/86))
* Pre-RunAs list (allow/block) validation can now optionally be enabled
  ([!68](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/68))
* Jacamar subcommand (`jacamar translate`) added to translate a runner
  configuration from the forked deployment
  ([!63](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/63))
* Added experimental plugin support for RunAs validation
  ([!76](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/76))
* `jacamar--auth` now supports an `--unobfuscated` flag
  ([!83](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/83))
* Update supported GitLab-Runner (patched) to version `13.7.0`
  ([!72](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/72))
* Added configurable timeout for Jacamar-Auth to wait before sending `SIGKILL`
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71))
* Updated OLCF focused testing structure for Ascent
  ([!62](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/61))

### Bug & Development Fixes

* Updated to Go release *1.15.6*
  ([!64](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/64))
* Correctly establish default values for configuration
  ([!71](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/71))
* Cleaner error handling within `jacamar-auth` to enforce obfuscation
  ([!74](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/74))
* Correctly leverage `_prefix` macro in RPM spec
  ([!79](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/79))
* Added Go/GCC to Pavilion containers and updated associated test
  scripts to build Jacamar requirements
  ([!78](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/78),
  [!80](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/80))
* System logging of `jacamar-auth` now handled through the Logrus
  ([!82](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/82))
* Improve RPM related make commands
  ([!85](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/85))

## v0.3.2 (December 11, 2020)

### Bug & Development Fixes

* Correctly monitor and wait on background child processes initiated
  by a job script
  ([!59](https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/59))
* Go `net/url` package when identifying target host for broker
