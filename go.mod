module gitlab.com/ecp-ci/jacamar-ci

go 1.19

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/go-playground/validator/v10 v10.10.1
	github.com/golang/mock v1.6.0
	github.com/seccomp/libseccomp-golang v0.10.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	gitlab.com/ecp-ci/gljobctx-go v0.3.1
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069
	kernel.org/pub/linux/libs/security/libcap/cap v1.2.51
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.2.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.51 // indirect
)
