// Package envparser manages actions related to the retrieval and validation of key environment
// variables used by the GitLab Runner in order to provide context to a custom executor driver.
// The ideal scenario involves fetching the ExecutorEnv structure as early as possible during
// the job's preparation phase. Any missing required variables can lead to unexpected failures that
// should be avoided by strictly observing any error raised here.
package envparser

import (
	"encoding/base64"
	"errors"
	"fmt"
	"math"
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/rules"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

var vv *validator.Validate

const (
	// UserVarTagName defines the structure tag used to identify the associated environment
	// variable set by the runner.
	UserVarTagName = "key"
	// RequiredKey defines the structure tag indicating if an environment variable is
	// required. This should be observed, jobs without required variable(s) must fail.
	RequiredKey     = "required"
	regexpRunnerVer = `(\d+)\.(\d+)\.(\d+)[a-zA-Z0-9.\-&_]{0,10}`
	defaultTimeout  = "60m"
)

// RequiredEnv identifies variables from the CustomEnv for easy retrieval. During configuration
// Jacamar-Auth take additional steps to validate all variables, so they can be trusted during
// subsequent stages/processes. Please note this validation does not ensure values will work, only
// that they are sufficiently free of user influence and potentially malicious values.
type RequiredEnv struct {
	JobID        string `key:"CUSTOM_ENV_CI_JOB_ID"  validate:"number" required:"true"`
	JobToken     string `key:"CUSTOM_ENV_CI_JOB_TOKEN"  validate:"authToken" required:"true"`
	ConcurrentID string `key:"CUSTOM_ENV_CI_CONCURRENT_ID"  validate:"number,max=5" required:"true"`
	RunnerShort  string `key:"CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN"  validate:"authToken" required:"true"`
	ServerURL    string `key:"CUSTOM_ENV_CI_SERVER_URL" validate:"url" required:"true"`
	CIJobJWT     string `key:"CUSTOM_ENV_CI_JOB_JWT" validate:"jwt" required:"true"`
}

// StatefulEnv maintains variables provided back to the runner during the configuration
// that are then made available to subsequent stages. Some values are not required and may
// only be present depending on your configuration.
type StatefulEnv struct {
	// BaseDir UserContext.BaseDir - Required
	BaseDir string `key:"JACAMAR_CI_BASE_DIR" required:"true"`
	// BuildsDir UserContext.BuildsDir - Required
	BuildsDir string `key:"JACAMAR_CI_BUILDS_DIR" required:"true"`
	// CacheDir UserContext.CacheDir - Required
	CacheDir string `key:"JACAMAR_CI_CACHE_DIR" required:"true"`
	// ScriptDir UserContext.ScriptDir - Required
	ScriptDir string `key:"JACAMAR_CI_SCRIPT_DIR" required:"true"`
	// Username UserContext.UserName - Required
	Username string `key:"JACAMAR_CI_AUTH_USERNAME" required:"true"`
	// SharedGroup UserContext.SharedGroup - Optional
	SharedGroup string `key:"JACAMAR_CI_SHARED_GROUP" required:"false"`
	// ProjectPath JWT.project_path - Optional
	ProjectPath string `key:"JACAMAR_CI_PROJECT_PATH" required:"false"`
	// RunnerTimeout maximum job timeout identified in job response - Required
	RunnerTimeout string `key:"JACAMAR_CI_RUNNER_TIMEOUT" required:"true"`
}

// ExecutorEnv maintains a view of all environment variables that are necessary for the completion
// of a CI job. All variables provided to the driver from the runner should remain available
// throughout the duration of the process.
type ExecutorEnv struct {
	RequiredEnv
	StatefulEnv
	// JobResponse is a selected sub-set of variables and only available during configuration actions.
	JobResponse
}

// ExitCodes returns both the SYSTEM_FAILURE_EXIT_CODE (default: 2) and BUILD_FAILURE_EXIT_CODE
// (default: 1) if defined by the custom executor. Uses default values if none found.
func ExitCodes() (sysExit, buildExit int) {
	var buildErr, sysErr error
	ce := os.Environ()
	for _, e := range ce {
		if strings.HasPrefix(e, envkeys.SysExitCode) {
			se := strings.Join(strings.Split(e, "=")[1:], "=")
			sysExit, sysErr = strconv.Atoi(se)
		} else if strings.HasPrefix(e, envkeys.BuildExitCode) {
			be := strings.Join(strings.Split(e, "=")[1:], "=")
			buildExit, buildErr = strconv.Atoi(be)
		}
	}

	if buildExit == 0 || buildErr != nil {
		buildExit = 1
	}
	if sysExit == 0 || sysErr != nil {
		sysExit = 2
	}

	return
}

// SupportedPrefix ensures that the environment variable key provided has a supported prefix.
// This should be used to avoid sharing or closely analyzing environment variables not associated
// with the custom executor model. IMPORTANT: this does not guarantee security in cases where
// administratively defined variables match the prefix of runner defined ones.
func SupportedPrefix(s string) bool {
	if strings.HasPrefix(s, envkeys.UserEnvPrefix) ||
		strings.HasPrefix(s, envkeys.StatefulEnvPrefix) ||
		strings.HasPrefix(s, envkeys.ScriptContentsPrefix) ||
		strings.HasPrefix(s, envkeys.SysExitCode) ||
		strings.HasPrefix(s, envkeys.BuildExitCode) {
		return true
	}

	return false
}

// EstablishScriptEnv splits the contents of a CI job script according to the maximum environment
// character length and set's every environment variable accordingly.
func EstablishScriptEnv(contents string, maxEnvChars int) []string {
	if contents == "" || maxEnvChars == 0 {
		return []string{}
	}

	encoded := base64.StdEncoding.EncodeToString([]byte(contents))
	l := len(encoded)
	n := 0

	env := make([]string, int(math.Ceil(float64(l)/float64(maxEnvChars))))

	for i := 0; i < l; i += maxEnvChars {
		if (l - i) <= maxEnvChars {
			env[n] = fmt.Sprintf(
				"%s%d=%s",
				envkeys.ScriptContentsPrefix,
				n,
				encoded[i:],
			)
		} else {
			env[n] = fmt.Sprintf(
				"%s%d=%s",
				envkeys.ScriptContentsPrefix,
				n,
				encoded[i:i+maxEnvChars],
			)
		}
		n++
	}

	return env
}

// RetrieveScriptEnv returns the full contents of a job script transferred via environment
// variables. An empty string will result in an error.
func RetrieveScriptEnv() (string, error) {
	var sb strings.Builder
	n := 0

	for {
		val, exists := os.LookupEnv(envkeys.ScriptContentsPrefix + strconv.Itoa(n))
		if !exists {
			break
		}

		// Avoid passing large variables to new processes when not required.
		_ = os.Unsetenv(envkeys.ScriptContentsPrefix + strconv.Itoa(n))

		sb.WriteString(val)
		n++
	}

	if sb.Len() == 0 {
		return "", errors.New("failed to retrieve script from environment")
	}

	decoded, err := base64.StdEncoding.DecodeString(sb.String())
	if err != nil {
		return "", fmt.Errorf("error decoding script contents, %w", err)
	}

	return string(decoded), nil
}

// Fetcher retrieves expected environment variables (required and stateful) and returns them via
// the ExecutorEnv struct. If any missing variables are detected (that are deemed required) an
// error message is returned. In addition, a range of validation steps are preformed based upon
// the CI stage, with expanded checks again the job response during config_exec.
func Fetcher(
	stateReq bool,
	c arguments.ConcreteArgs,
	opt configure.Options,
) (je ExecutorEnv, err error) {
	env := envMap()

	if err = KeyVarMapping(&je.RequiredEnv, env); err != nil {
		return
	}
	(&je.RequiredEnv).override(opt)

	// Validate known values before continuing to manage response flow. This
	// may duplicate some efforts of integrating the response file.
	if err = vv.Struct(&je.RequiredEnv); err != nil {
		return
	}

	resp, err := responseFlow(je.RequiredEnv, c, opt)
	if err != nil {
		return
	}
	je.JobResponse = resp

	if stateReq {
		if err = KeyVarMapping(&je.StatefulEnv, env); err != nil {
			return
		}
	}

	return je, err
}

// envMap returns current OS Environment as a map of [key] = value pairs
// that are identified as a supported prefix.
func envMap() map[string]string {
	env := os.Environ()
	m := make(map[string]string)

	for _, e := range env {
		if SupportedPrefix(e) {
			k := strings.Split(e, "=")[0]
			v := strings.Join(strings.Split(e, "=")[1:], "=")
			m[k] = v
		}
	}

	return m
}

func (r *RequiredEnv) override(opt configure.Options) {
	if opt.General.GitLabServer != "" {
		r.ServerURL = opt.General.GitLabServer
	}

	if opt.General.FFJWTV2 {
		r.CIJobJWT = r.jwtV2()
	}
}

// jwtV2 observes the FFJWTV2 option and attempts to fetch the related
// value. If not encountered a warning will be printed and the old JWT used.
func (r *RequiredEnv) jwtV2() string {
	jwtV2 := struct {
		encoded string `validate:"jwt"`
	}{
		encoded: os.Getenv(envkeys.UserEnvPrefix + envkeys.CIJobJWTV2),
	}

	err := vv.Struct(jwtV2)
	if err != nil || jwtV2.encoded == "" {
		// Since FFJWTV2 is labeled as a debug option, simply print to stderr
		// without worrying about obfuscation.
		_, _ = fmt.Fprintln(os.Stderr, "Invalid V2 JWT encountered, reverting to V1")
		jwtV2.encoded = r.CIJobJWT
	}

	return jwtV2.encoded
}

// KeyVarMapping maps the expected key (struct tag UserVarTagName) to the
// established env map. Required values (struct tag RequiredKey) are observed.
// Only String types are supported in this workflow, any invalid field
// will result in an error message.
func KeyVarMapping(i interface{}, env map[string]string) error {
	if i == nil {
		return errors.New("invalid nil interface provided")
	}

	iType := reflect.TypeOf(i).Elem()
	if iType.Kind() != reflect.Struct {
		return errors.New("invalid non-struct interface provided")
	}

	for j := 0; j < iType.NumField(); j++ {
		id := j
		jField := iType.Field(id)
		jValue := reflect.ValueOf(i).Elem().Field(id)

		if jValue.Kind() != reflect.String {
			return fmt.Errorf("invalid field type (%v) encountered", jValue.Kind())
		}

		tag := jField.Tag.Get(UserVarTagName)
		req := jField.Tag.Get(RequiredKey)
		val := env[tag]

		if val == "" {
			if strings.ToLower(req) == "true" {
				return fmt.Errorf("missing required environment variable %s", tag)
			}
			continue
		}

		jValue.SetString(val)
	}

	return nil
}

func init() {
	vv = validator.New()
	_ = vv.RegisterValidation("directory", rules.CheckDirectory)
	_ = vv.RegisterValidation("qualifiedDir", rules.CheckQualifiedDirectory)
	_ = vv.RegisterValidation("unexpandedDirectory", rules.CheckUnexpandedDirectory)
	_ = vv.RegisterValidation("gitlabToken", rules.CheckGitLabToken)
	_ = vv.RegisterValidation("authToken", rules.CheckAuthToken)
	_ = vv.RegisterValidation("jwt", rules.CheckJWT)
}
