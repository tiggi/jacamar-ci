package envparser

import (
	"errors"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
)

// GitTrace if Git's tracing/debug is expected.
func GitTrace() bool {
	return TrueEnvVar(envkeys.UserEnvPrefix + envkeys.GitTrace)
}

// TrueEnvVar check if variable 'true' or '1'.
func TrueEnvVar(key string) bool {
	val, present := os.LookupEnv(key)
	if present {
		val = strings.ToLower(strings.TrimSpace(val))
		if val == "true" || val == "1" {
			return true
		}
	}

	return false
}

// ValidRunnerVersion identify if the runner version triggering the job is valid
// against the provided major.minor release version. Since we check against custom
// environmental variables this should only be used as a smoke test to avoid later
// difficult errors and not as a security requirement.
func ValidRunnerVersion(major, minor int) bool {
	val, found := os.LookupEnv(envkeys.UserEnvPrefix + envkeys.CIRunnerVer)
	if found {
		matched, _ := regexp.MatchString(regexpRunnerVer, val)
		if matched {
			split := strings.Split(val, ".")
			majorInt, _ := strconv.Atoi(split[0])
			minorInt, _ := strconv.Atoi(split[1])

			if major == majorInt && minor <= minorInt {
				return true
			} else if major < majorInt {
				return true
			}
		}
	}

	return false
}

// CustomBuildsDir retrieve and validate a user defined CUSTOM_BUILDS_DIR variable to be used
// in related directory identification and creation. The lack of any corresponding value is
// conveyed as a boolean and does not result in any error message. Unexpanded variables detected
// in the directory do not cause errors, and upon expansion the path should be re-validated.
func CustomBuildsDir() (dir string, found bool, err error) {
	return findDir(
		envkeys.UserEnvPrefix+envkeys.CustomBuildsDir,
		func(dir string) error {
			params := struct {
				Dir string `validate:"unexpandedDirectory"`
			}{
				Dir: dir,
			}

			if err = vv.Struct(&params); err != nil {
				err = errors.New("invalid directory provided")
			}

			return err
		})
}

// SchedulerLogDir retrieve and validate a user defined COPY_SCHEDULER_LOGS variable to be used
// in related directory identification and creation. The lack of any corresponding value is
// conveyed as a boolean and does not result in any error message.
func SchedulerLogDir() (dir string, found bool, err error) {
	return findDir(envkeys.UserEnvPrefix+envkeys.CopySchedulerLogs, ValidateDirectory)
}

// ValidateDirectory ensures that proposed directory path meets Unix criteria and absolute.
// The existence of the directory is not required.
func ValidateDirectory(dir string) (err error) {
	params := struct {
		Dir string `validate:"qualifiedDir"`
	}{
		Dir: dir,
	}

	if err = vv.Struct(&params); err != nil {
		err = errors.New("invalid directory provided")
	}

	return err
}

func findDir(tarDir string, validateDir func(string) error) (dir string, found bool, err error) {
	dir, found = os.LookupEnv(tarDir)
	if !found {
		return
	}

	if err = validateDir(dir); err != nil {
		// Avoid returning a potentially malicious directory value.
		return "", found, err
	}

	return
}
