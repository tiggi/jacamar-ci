package envparser

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// JobResponse maintains the structured via of the JSON provided to the runner by the server
// for the purposes of job execution. Though the entirety of the response is made available
// to the custom executor, only a small subset may be made available as we rely on the runner
// to realize the majority of the functionality.
// https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/common/network.go
type JobResponse struct {
	ID         int        `json:"id"`
	Token      string     `json:"token"`
	GitInfo    GitInfo    `json:"git_info"`
	Variables  []Variable `json:"variables"`
	RunnerInfo RunnerInfo `json:"runner_info"`
}

type GitInfo struct {
	RepoURL string `json:"repo_url"`
	Ref     string `json:"ref"`
	RefType string `json:"ref_type"`
	Sha     string `json:"sha"`
}

type Variable struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type RunnerInfo struct {
	Timeout int `json:"timeout"`
}

func responseFlow(
	req RequiredEnv,
	c arguments.ConcreteArgs,
	opts configure.Options,
) (JobResponse, error) {
	if c.Config == nil && (c.Cleanup == nil || c.NoAuth) {
		// There is currently no identified use for this information outside of config_exec's
		// authorization flow or re-validation during cleanup (accounting for a failed config).
		return JobResponse{}, nil
	}

	file, found := os.LookupEnv(envkeys.JobResponse)
	if !found {
		return JobResponse{}, fmt.Errorf(
			"no %s variable found, verify runner version 14.1+", envkeys.JobResponse,
		)
	}

	resp, err := obtainResponse(file)
	if err != nil {
		return resp, fmt.Errorf("identifying job response file: %w", err)
	}

	err = resp.secondaryVerification(req, opts)
	if err != nil && c.Config != nil {
		// In cases of failures attempted user influence implied, error should be conveyed to user
		// during the config_exec stage, repetition of error message will only clutter system logging.
		log.Println(fmt.Sprintf(
			"Please correct your CI job before proceeding: %s",
			err.Error(),
		))
	}

	return resp, err
}

func obtainResponse(file string) (resp JobResponse, err error) {
	/* #nosec */
	// variable file path required
	content, err := os.ReadFile(filepath.Clean(file))
	if err != nil {
		return resp, err
	}

	err = json.Unmarshal(content, &resp)

	return
}

// secondaryVerification performs additional checks on all requirement environment variables that
// have been identified. Each check is done on a case-by-case basis to ensure that values match some
// basic understanding provided by the job response and configuration.
func (j JobResponse) secondaryVerification(req RequiredEnv, opts configure.Options) (err error) {
	totalVars := j.duplicateVariables()

	switch true {
	case j.checkJobID(req):
		return errors.New("invalid JobID detected, do not attempt to influence CI_JOB_ID")
	case j.checkJobToken(req):
		return errors.New("invalid JobToken detected, do not attempt to influence CI_JOB_TOKEN")
	case j.checkJWT(totalVars):
		return errors.New("invalid JobJWT detected, do not attempt to influence CI_JOB_JWT")
	case j.checkConcurrentID(totalVars):
		return errors.New("invalid ConcurrentID detected, do not attempt to influence CI_CONCURRENT_ID")
	case j.checkRunnerShort(totalVars):
		return errors.New("invalid RunnerShortToken detected, do not attempt to influence CI_RUNNER_SHORT_TOKEN")
	case j.checkServerURL(totalVars, req, opts):
		return errors.New("invalid ServerURL detected, do not attempt to influence CI_SERVER_URL")
	}
	return
}

func (j JobResponse) duplicateVariables() map[string]int {
	totals := make(map[string]int)
	for i := range j.Variables {
		totals[j.Variables[i].Key] = totals[j.Variables[i].Key] + 1
	}

	return totals
}

func (j JobResponse) checkJobID(req RequiredEnv) bool {
	return !(strconv.Itoa(j.ID) == req.JobID)
}

func (j JobResponse) checkJobToken(req RequiredEnv) bool {
	return !(j.Token == req.JobToken)
}

func (j JobResponse) checkJWT(totalVars map[string]int) bool {
	return totalVars["CI_JOB_JWT"] > 1
}

func (j JobResponse) checkRunnerShort(totalVars map[string]int) bool {
	return totalVars["CI_RUNNER_SHORT_TOKEN"] > 0
}

func (j JobResponse) checkConcurrentID(totalVars map[string]int) bool {
	return totalVars["CI_CONCURRENT_ID"] > 0
}

func (j JobResponse) checkServerURL(totalVars map[string]int, req RequiredEnv, opts configure.Options) bool {
	if opts.General.GitLabServer == req.ServerURL {
		// Administrative override provided, skip verification.
		return false
	}

	u, err := url.Parse(req.ServerURL)
	if err != nil ||
		// We should be able to show a recreated RepoURL with the server URL.
		!strings.HasPrefix(j.GitInfo.RepoURL, fmt.Sprintf(
			"%s://gitlab-ci-token:%s@%s",
			u.Scheme,
			j.Token,
			strings.Replace(u.String(), u.Scheme+"://", "", 1),
		)) {
		return true
	}

	return totalVars["CI_SERVER_URL"] > 1
}

// ExpandState utilizes select elements of the server's response payload to expand
// the stateful variables during the config_exec stage.
func (j JobResponse) ExpandState(state *StatefulEnv) {
	state.RunnerTimeout = j.runnerTimeout()
}

func (j JobResponse) runnerTimeout() string {
	if j.RunnerInfo.Timeout == 0 {
		// A zero timeout is not expected by the server or supported by the server.
		return defaultTimeout
	}

	return (time.Duration(j.RunnerInfo.Timeout) * time.Second).String()
}
