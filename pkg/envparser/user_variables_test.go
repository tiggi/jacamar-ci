package envparser

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
)

func TestTrueEnvVar(t *testing.T) {
	tests := map[string]envTests{
		"Undefined TEST_TRUE_ENV_VAR env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_TRUE_ENV_VAR": "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined 1 TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_TRUE_ENV_VAR": "1",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
		"Defined false TEST_TRUE_ENV_VAR env": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "TEST_TRUE_ENV_VAR": "false",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got := TrueEnvVar(envkeys.UserEnvPrefix + "TEST_TRUE_ENV_VAR")

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}

func TestGitTrace(t *testing.T) {
	tests := map[string]envTests{
		"Undefined GIT_TRACE env": {
			assertBoolean: func(t *testing.T, b bool) {
				assert.False(t, b)
			},
		},
		"Defined true GIT_TRACE env": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.GitTrace: "true",
			},
			assertBoolean: func(t *testing.T, b bool) {
				assert.True(t, b)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got := GitTrace()

			if tt.assertBoolean != nil {
				tt.assertBoolean(t, got)
			}
		})
	}
}

func TestValidRunnerVersion(t *testing.T) {
	tests := map[string]struct {
		targetEnv map[string]string
		major     int
		minor     int
		want      bool
	}{
		"no environment variable found": {
			want: false,
		},
		"matching versions": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.1.0",
			},
			major: 14,
			minor: 1,
			want:  true,
		},
		"outdated major version": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.1.0",
			},
			major: 15,
			minor: 0,
			want:  false,
		},
		"outdated minor version": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "15.5.5",
			},
			major: 15,
			minor: 10,
			want:  false,
		},
		"updated minor version": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "15.5.5",
			},
			major: 15,
			minor: 1,
			want:  true,
		},
		"correct with RC": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.1.0-RC1",
			},
			major: 13,
			minor: 0,
			want:  true,
		},
		"text string": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "some text",
			},
			major: 14,
			minor: 1,
			want:  false,
		},
		"correct with modified sha": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.4.1.abcd1234&",
			},
			major: 14,
			minor: 1,
			want:  true,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			if got := ValidRunnerVersion(tt.major, tt.minor); got != tt.want {
				t.Errorf("ValidRunnerVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCustomBuildsDir(t *testing.T) {
	t.Run("no directory found", func(t *testing.T) {
		gotDir, gotFound, err := CustomBuildsDir()
		assert.Equal(t, "", gotDir)
		assert.False(t, gotFound)
		assert.NoError(t, err)
	})
	t.Run("found directory no error", func(t *testing.T) {
		t.Setenv(envkeys.UserEnvPrefix+envkeys.CustomBuildsDir, "/example/working/dir")

		gotDir, gotFound, err := CustomBuildsDir()
		assert.Equal(t, "/example/working/dir", gotDir)
		assert.True(t, gotFound)
		assert.NoError(t, err)
	})
	t.Run("found directory invalid", func(t *testing.T) {
		t.Setenv(envkeys.UserEnvPrefix+envkeys.CustomBuildsDir, "/invalid/$(dir)")

		gotDir, gotFound, err := CustomBuildsDir()
		assert.Equal(t, "", gotDir)
		assert.True(t, gotFound)
		assert.EqualError(t, err, "invalid directory provided")
	})
	t.Run("unresolved directory", func(t *testing.T) {
		t.Setenv(envkeys.UserEnvPrefix+envkeys.CustomBuildsDir, "/test/../hello/world")

		gotDir, gotFound, err := CustomBuildsDir()
		assert.Equal(t, "/test/../hello/world", gotDir)
		assert.True(t, gotFound)
		// CustomBuildsDir does not enforce qualified paths
		assert.NoError(t, err)
	})
	t.Run("found directory illegal charactesr", func(t *testing.T) {
		t.Setenv(envkeys.UserEnvPrefix+envkeys.CustomBuildsDir, "/invalid\\/test")

		_, _, err := CustomBuildsDir()
		assert.EqualError(t, err, "invalid directory provided")
	})
}

func TestSchedulerLogDir(t *testing.T) {
	t.Run("no directory found", func(t *testing.T) {
		gotDir, gotFound, err := SchedulerLogDir()
		assert.Equal(t, "", gotDir)
		assert.False(t, gotFound)
		assert.NoError(t, err)
	})
	t.Run("found directory no error", func(t *testing.T) {
		_ = os.Setenv(envkeys.UserEnvPrefix+envkeys.CopySchedulerLogs, "/example/working/dir")
		defer func() { _ = os.Unsetenv(envkeys.UserEnvPrefix + envkeys.CopySchedulerLogs) }()

		gotDir, gotFound, err := SchedulerLogDir()
		assert.Equal(t, "/example/working/dir", gotDir)
		assert.True(t, gotFound)
		assert.NoError(t, err)
	})
	t.Run("found directory invalid", func(t *testing.T) {
		_ = os.Setenv(envkeys.UserEnvPrefix+envkeys.CopySchedulerLogs, "invalid/$(dir)")
		defer func() { _ = os.Unsetenv(envkeys.UserEnvPrefix + envkeys.CopySchedulerLogs) }()

		gotDir, gotFound, err := SchedulerLogDir()
		assert.Equal(t, "", gotDir)
		assert.True(t, gotFound)
		assert.EqualError(t, err, "invalid directory provided")
	})
}
