package envparser

import (
	"path/filepath"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_responseFlow(t *testing.T) {
	respFile := filepath.Clean("../../test/testdata/valid_job_response.json")
	reqResp := &RequiredEnv{
		ConcurrentID: validConcurrentID,
		JobID:        validJobID,
		JobToken:     validJobToken,
		RunnerShort:  validRunnerShort,
		ServerURL:    validServerURL,
		CIJobJWT:     validJWT,
	}

	tests := map[string]envTests{
		"skip no-auth": {
			c: arguments.ConcreteArgs{
				NoAuth: true,
			},
			req:         &RequiredEnv{},
			assertError: tst.AssertNoError,
		},
		"skip none config stages": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			req:         &RequiredEnv{},
			assertError: tst.AssertNoError,
		},
		"no job env variable found": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			req: &RequiredEnv{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no JOB_RESPONSE_FILE variable found, verify runner version 14.1+")
			},
		},
		"response file not found": {
			targetEnv: map[string]string{
				envkeys.JobResponse: "/invalid/file/not.found",
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			req: &RequiredEnv{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "identifying job response file: open /invalid/file/not.found: no such file or directory")
			},
		},
		"acceptable required environment identified": {
			targetEnv: map[string]string{
				envkeys.JobResponse: respFile,
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			req:         reqResp,
			assertError: tst.AssertNoError,
		},
		"invalid jobID encountered": {
			targetEnv: map[string]string{
				envkeys.JobResponse: respFile,
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        "444",
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid JobID detected, do not attempt to influence CI_JOB_ID")
			},
		},
		"cleanup_exex acceptable required environment identified": {
			targetEnv: map[string]string{
				envkeys.JobResponse: respFile,
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			req:         reqResp,
			assertError: tst.AssertNoError,
		},
		"no-auth acceptable required environment identified": {
			targetEnv: map[string]string{
				envkeys.JobResponse: respFile,
			},
			c: arguments.ConcreteArgs{
				NoAuth: true,
				Config: &arguments.ConfigCmd{},
			},
			req:         reqResp,
			assertError: tst.AssertNoError,
		},
		"no-auth cleanup skip": {
			c: arguments.ConcreteArgs{
				NoAuth:  true,
				Cleanup: &arguments.CleanupCmd{},
			},
			req:         &RequiredEnv{},
			assertError: tst.AssertNoError,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			resp, err := responseFlow(*tt.req, tt.c, tt.opt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertResponse != nil {
				tt.assertResponse(t, resp)
			}
		})
	}
}

func TestJobResponse_duplicateVariables(t *testing.T) {
	type fields struct {
		Variables []Variable
	}
	tests := map[string]struct {
		fields     fields
		wantTotals map[string]int
	}{
		"duplicates found": {
			fields: fields{
				Variables: []Variable{
					{
						Key:   "one",
						Value: "one-1",
					}, {
						Key:   "one",
						Value: "one-2",
					}, {
						Key:   "two",
						Value: "two-2",
					},
				},
			},
			wantTotals: map[string]int{
				"one": 2,
				"two": 1,
			},
		}, "no duplicates found": {
			fields: fields{
				Variables: []Variable{
					{
						Key:   "one",
						Value: "one-1",
					}, {
						Key:   "two",
						Value: "two-2",
					},
				},
			},
			wantTotals: map[string]int{
				"one": 1,
				"two": 1,
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			j := JobResponse{
				Variables: tt.fields.Variables,
			}

			if gotTotals := j.duplicateVariables(); !reflect.DeepEqual(gotTotals, tt.wantTotals) {
				t.Errorf("duplicateVariables() = %v, want %v", gotTotals, tt.wantTotals)
			}
		})
	}
}

func TestJobResponse_secondaryVerification(t *testing.T) {
	baseResponse := JobResponse{
		ID:    123,
		Token: "abc123efg456hij789kl",
		GitInfo: GitInfo{
			RepoURL: "https://gitlab-ci-token:abc123efg456hij789kl@gitlab.example.com/user/ci-json-example.git",
		},
		Variables: []Variable{
			{
				Key:   "CI_PIPELINE_ID",
				Value: "82078",
			}, {
				Key:   "CI_JOB_ID",
				Value: "123",
			}, {
				Key:   "CI_JOB_URL",
				Value: "https://gitlab.example.com/user/ci-json-example/-/jobs/123",
			}, {
				Key:   "CI_JOB_TOKEN",
				Value: "abc123efg456hij789kl",
			}, {
				Key:   "CI_JOB_JWT",
				Value: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6MzI0OTE4MjAxNjB9.WHWvWckMj-ZaK6tolpKCs4XJtlVwmSdnmyTk5F3Aqw4hHK-tzk9JrfLf1MeWtXpU1H2ZTUTCKjD57wYBZsLoy5sHySd5cny_ao_MwJMbsE7s9pKEilr_4o4S57aL9-2pD56jt4w1lezgic9gQXxjCXuiPVNpCZqixUPimZ2lD3Vm1Hy2PTc8lTlHozKtdq8pttvBA1TlgiDOMxmn0DaPdFs59iBhvaGCeWhB3ZMcARDtOQxtwhBHo0Th9gquGxXxDCYoxI6c_lNhWoAlSeD3fbWp_oT8XjEb5V5se7-plxOHaJSWaWPkw6Kx17T1VyvH9ddY17HBEW7crGIYiYrGHw",
			}, {
				Key:   "CI_SERVER_URL",
				Value: "https://gitlab.example.com",
			},
		},
	}

	urlResponse := baseResponse
	urlResponse.Variables = append(urlResponse.Variables, Variable{Key: "CI_SERVER_URL", Value: "example"})

	jwtResponse := baseResponse
	jwtResponse.Variables = append(jwtResponse.Variables, Variable{Key: "CI_JOB_JWT", Value: "example.job.jwt"})

	runnerShort := baseResponse
	runnerShort.Variables = append(runnerShort.Variables, Variable{Key: "CI_RUNNER_SHORT_TOKEN", Value: "42"})

	concurrentResponse := baseResponse
	concurrentResponse.Variables = append(concurrentResponse.Variables, Variable{Key: "CI_CONCURRENT_ID", Value: "42"})

	tests := map[string]envTests{
		"valid RequiredEnv": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp:        baseResponse,
			assertError: tst.AssertNoError,
		}, "invalid JobID": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        "444",
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: baseResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid JobID detected, do not attempt to influence CI_JOB_ID")
			},
		}, "invalid JobToken": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     "token",
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: baseResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid JobToken detected, do not attempt to influence CI_JOB_TOKEN")
			},
		}, "multiple JWT variables": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: jwtResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid JobJWT detected, do not attempt to influence CI_JOB_JWT")
			},
		},
		"configured ServerURL": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    "https://example.com/gitlab",
				CIJobJWT:     validJWT,
			},
			resp: baseResponse,
			opt: configure.Options{
				General: configure.General{
					GitLabServer: "https://example.com/gitlab",
				},
			},
			assertError: tst.AssertNoError,
		},
		"invalid ServerURL": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    "https://example.com",
				CIJobJWT:     validJWT,
			},
			resp: baseResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid ServerURL detected, do not attempt to influence CI_SERVER_URL")
			},
		},
		"multiple ServerURL variables": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: urlResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid ServerURL detected, do not attempt to influence CI_SERVER_URL")
			},
		},
		"multiple RunnerShort variables": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: runnerShort,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid RunnerShortToken detected, do not attempt to influence CI_RUNNER_SHORT_TOKEN")
			},
		},
		"multiple ConcurrentID variables": {
			req: &RequiredEnv{
				ConcurrentID: validConcurrentID,
				JobID:        validJobID,
				JobToken:     validJobToken,
				RunnerShort:  validRunnerShort,
				ServerURL:    validServerURL,
				CIJobJWT:     validJWT,
			},
			resp: concurrentResponse,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid ConcurrentID detected, do not attempt to influence CI_CONCURRENT_ID")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.resp.secondaryVerification(*tt.req, tt.opt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestJobResponse_runnerTimeout(t *testing.T) {
	tests := map[string]struct {
		timeout int
		want    string
	}{
		"default timeout": {
			timeout: 0,
			want:    defaultTimeout,
		},
		"duration parsed": {
			timeout: 300,
			want:    "5m0s",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			j := JobResponse{
				RunnerInfo: RunnerInfo{
					Timeout: tt.timeout,
				},
			}
			s := StatefulEnv{}
			j.ExpandState(&s)

			assert.Equal(t, tt.want, s.RunnerTimeout)
		})
	}
}
