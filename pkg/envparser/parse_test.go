package envparser

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

const (
	validConcurrentID = "1"
	validJobID        = "123"
	validJobToken     = "abc123efg456hij789kl"
	validRunnerShort  = "abcd1234"
	validProjectPath  = "user/ci-json-example"
	validUserLogin    = "user"
	validServerURL    = "https://gitlab.example.com"
	validJWT          = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InRlc3QifQ.eyJ1c2VyX2xvZ2luIjoidXNlciIsInByb2plY3RfcGF0aCI6Imdyb3VwL3Byb2plY3QiLCJuYW1lc3BhY2VfaWQiOiIxMDAxIiwiam9iX2lkIjoiMTIzIiwicGlwZWxpbmVfaWQiOiI0NTYiLCJwcm9qZWN0X2lkIjoiMjAwMiIsInVzZXJfaWQiOiI3ODkiLCJ1c2VyX2VtYWlsIjoidXNlckBleGFtcGxlLmNvbSIsImZlZGVyYXRlZF91c2VybmFtZSI6ImZlZF91c2VyIiwiYXV0aF90b2tlbiI6InRvazNuIiwic3ViIjoiam9iXzEyMyIsImV4cCI6MzI0OTE4MjAxNjB9.WHWvWckMj-ZaK6tolpKCs4XJtlVwmSdnmyTk5F3Aqw4hHK-tzk9JrfLf1MeWtXpU1H2ZTUTCKjD57wYBZsLoy5sHySd5cny_ao_MwJMbsE7s9pKEilr_4o4S57aL9-2pD56jt4w1lezgic9gQXxjCXuiPVNpCZqixUPimZ2lD3Vm1Hy2PTc8lTlHozKtdq8pttvBA1TlgiDOMxmn0DaPdFs59iBhvaGCeWhB3ZMcARDtOQxtwhBHo0Th9gquGxXxDCYoxI6c_lNhWoAlSeD3fbWp_oT8XjEb5V5se7-plxOHaJSWaWPkw6Kx17T1VyvH9ddY17HBEW7crGIYiYrGHw"
)

type envTests struct {
	req         *RequiredEnv
	c           arguments.ConcreteArgs
	opt         configure.Options
	resp        JobResponse
	targetEnv   map[string]string
	stateReq    bool
	contents    string
	maxEnvChars int

	assertExecutorEnv func(*testing.T, ExecutorEnv)
	assertRequiredEnv func(*testing.T, *RequiredEnv)
	assertResponse    func(*testing.T, JobResponse)
	assertExitStatus  func(*testing.T, int, int)
	assertError       func(*testing.T, error)
	assertString      func(*testing.T, string)
	assertSlice       func(*testing.T, []string)
	assertBoolean     func(*testing.T, bool)
}

func TestExitCodes(t *testing.T) {
	tests := map[string]envTests{
		"No exit status variables defined in environment": {
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 1, build, "Default build failure exit code expected")
				assert.Equal(t, 2, sys, "Default system failure exit code expected")
			},
		},
		"Missing build exit code failure": {
			targetEnv: map[string]string{"SYSTEM_FAILURE_EXIT_CODE": "22"},
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 1, build, "Default build failure exit code expected")
				assert.Equal(t, 22, sys)
			},
		},
		"Working environment, both build & system codes defined": {
			targetEnv: map[string]string{
				"BUILD_FAILURE_EXIT_CODE":  "11",
				"SYSTEM_FAILURE_EXIT_CODE": "22",
			},
			assertExitStatus: func(t *testing.T, build, sys int) {
				assert.Equal(t, 11, build)
				assert.Equal(t, 22, sys)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			sys, build := ExitCodes()

			if tt.assertExitStatus != nil {
				tt.assertExitStatus(t, build, sys)
			}
		})
	}
}

func TestEstablishScriptEnv(t *testing.T) {
	tests := map[string]envTests{
		"empty contents provided": {
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
		"zero maxEnvChars provided (should not occur due to defaults)": {
			contents: "script",
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 0)
			},
		},
		"single element return expected": {
			contents:    "script",
			maxEnvChars: 100,
			assertSlice: func(t *testing.T, s []string) {
				assert.Len(t, s, 1)
				assert.Equal(t, []string{
					envkeys.ScriptContentsPrefix + "0=c2NyaXB0",
				}, s)
			},
		},
		"multi-element return expected": {
			contents:    "ABCD",
			maxEnvChars: 1,
			assertSlice: func(t *testing.T, s []string) {
				assert.Equal(t, []string{
					envkeys.ScriptContentsPrefix + "0=Q", envkeys.ScriptContentsPrefix + "1=U",
					envkeys.ScriptContentsPrefix + "2=J", envkeys.ScriptContentsPrefix + "3=D",
					envkeys.ScriptContentsPrefix + "4=R", envkeys.ScriptContentsPrefix + "5=A",
					envkeys.ScriptContentsPrefix + "6==", envkeys.ScriptContentsPrefix + "7==",
				}, s)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := EstablishScriptEnv(tt.contents, tt.maxEnvChars)

			if tt.assertSlice != nil {
				tt.assertSlice(t, got)
			}
		})
	}
}

func TestRetrieveScriptEnv(t *testing.T) {
	tests := map[string]envTests{
		"no environment variables established, error": {
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to retrieve script from environment")
			},
		},
		"single ScriptContentsPrefix defined": {
			targetEnv: map[string]string{
				envkeys.ScriptContentsPrefix + "0": "c2NyaXB0",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "script", s)
			},
		},
		"multiple ScriptContentsPrefix defined": {
			targetEnv: map[string]string{
				envkeys.ScriptContentsPrefix + "0": "QU",
				envkeys.ScriptContentsPrefix + "1": "JD",
				envkeys.ScriptContentsPrefix + "2": "RA",
				envkeys.ScriptContentsPrefix + "3": "==",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Equal(t, "ABCD", s)
			},
		},
		"bad encoding found": {
			targetEnv: map[string]string{
				envkeys.ScriptContentsPrefix + "0": "c2Nya%%0",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"error decoding script contents, illegal base64 data at input byte 5",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			got, err := RetrieveScriptEnv()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func TestFetcher(t *testing.T) {
	tests := map[string]envTests{
		"complete test environment provided (stateful + required variables), prepare_exec": {
			stateReq: true,
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				envkeys.StatefulEnvPrefix + "AUTH_USERNAME":     "tester",
				envkeys.StatefulEnvPrefix + "BASE_DIR":          "/ci",
				envkeys.StatefulEnvPrefix + "BUILDS_DIR":        "/ci/builds",
				envkeys.StatefulEnvPrefix + "CACHE_DIR":         "/ci/cache",
				envkeys.StatefulEnvPrefix + "SCRIPT_DIR":        "/ci/script",
				envkeys.StatefulEnvPrefix + "PROJECT_PATH":      "group/project",
				envkeys.RunnerTimeout:                           "300s",
				"BUILD_FAILURE_EXIT_CODE":                       "1",
				"TEST_PATH":                                     "/usr/bin:/usr/sbin",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				assert.Equal(t, RequiredEnv{
					ConcurrentID: validConcurrentID,
					JobID:        validJobID,
					JobToken:     validJobToken,
					RunnerShort:  validRunnerShort,
					ServerURL:    validServerURL,
					CIJobJWT:     validJWT,
				}, env.RequiredEnv)
				assert.Equal(t, StatefulEnv{
					Username:      "tester",
					BaseDir:       "/ci",
					BuildsDir:     "/ci/builds",
					CacheDir:      "/ci/cache",
					ScriptDir:     "/ci/script",
					ProjectPath:   "group/project",
					RunnerTimeout: "300s",
				}, env.StatefulEnv)
			},
		},
		"missing variables in CI environment, config_exec": {
			stateReq: false,
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			targetEnv: map[string]string{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing required environment variable CUSTOM_ENV_CI_JOB_ID",
				)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				assert.Equal(t, ExecutorEnv{}, env)
			},
		},
		"missing stateful variables in CI environment, run_exec": {
			stateReq: true,
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				"TEST_PATH":               "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE": "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"missing required environment variable JACAMAR_CI_BASE_DIR",
				)
			},
		},
		"invalid CI_JOB_JWT specified": {
			stateReq: false,
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        "eyJ:)y.eyJoZWx.sm0r7FO",
				"TRUSTED_CI_CACHE_DIR":                          "/ci/cache",
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				"TEST_PATH":               "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE": "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"Key: 'RequiredEnv.CIJobJWT' Error:Field validation for 'CIJobJWT' failed on the 'jwt' tag",
				)
			},
		},
		"invalid CI_SERVER_URL specified": {
			stateReq: false,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         "gitlab.",
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				"TRUSTED_CI_CACHE_DIR":                          "/ci/cache",
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				"TEST_PATH":               "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE": "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"Key: 'RequiredEnv.ServerURL' Error:Field validation for 'ServerURL' failed on the 'url' tag",
				)
			},
		},
		"valid CI_SERVER_URL specified": {
			stateReq: false,
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         "https://example.com/gitlab",
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				"TRUSTED_CI_CACHE_DIR":                          "/ci/cache",
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				"TEST_PATH":               "/usr/bin:/usr/sbin",
				"BUILD_FAILURE_EXIT_CODE": "1",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"no JOB_RESPONSE_FILE found, config_exec": {
			stateReq: true,
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				envkeys.StatefulEnvPrefix + "AUTH_USERNAME":     "tester",
				envkeys.StatefulEnvPrefix + "BASE_DIR":          "/ci",
				envkeys.StatefulEnvPrefix + "BUILDS_DIR":        "/ci/builds",
				envkeys.StatefulEnvPrefix + "CACHE_DIR":         "/ci/cache",
				envkeys.StatefulEnvPrefix + "SCRIPT_DIR":        "/ci/script",
				envkeys.StatefulEnvPrefix + "PROJECT_PATH":      "group/project",
				"BUILD_FAILURE_EXIT_CODE":                       "1",
				"TEST_PATH":                                     "/usr/bin:/usr/sbin",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"no JOB_RESPONSE_FILE variable found, verify runner version 14.1+",
				)
			},
		},
		"gitlab server url override, run_exec": {
			stateReq: true,
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			opt: configure.Options{
				General: configure.General{
					GitLabServer: "https://example.com/gitlab",
				},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				envkeys.StatefulEnvPrefix + "AUTH_USERNAME":     "tester",
				envkeys.StatefulEnvPrefix + "BASE_DIR":          "/ci",
				envkeys.StatefulEnvPrefix + "BUILDS_DIR":        "/ci/builds",
				envkeys.StatefulEnvPrefix + "CACHE_DIR":         "/ci/cache",
				envkeys.StatefulEnvPrefix + "SCRIPT_DIR":        "/ci/script",
				envkeys.StatefulEnvPrefix + "PROJECT_PATH":      "group/project",
				envkeys.RunnerTimeout:                           "300s",
				"BUILD_FAILURE_EXIT_CODE":                       "1",
				"TEST_PATH":                                     "/usr/bin:/usr/sbin",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				assert.Equal(t, "https://example.com/gitlab", env.ServerURL)
			},
		},
		"complete test environment provided, config_exec": {
			stateReq: false,
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "CI_CONCURRENT_ID":      validConcurrentID,
				envkeys.UserEnvPrefix + "CI_JOB_ID":             validJobID,
				envkeys.UserEnvPrefix + "CI_JOB_TOKEN":          validJobToken,
				envkeys.UserEnvPrefix + "CI_RUNNER_SHORT_TOKEN": validRunnerShort,
				envkeys.UserEnvPrefix + "CI_PROJECT_PATH_SLUG":  validProjectPath,
				envkeys.UserEnvPrefix + "GITLAB_USER_LOGIN":     validUserLogin,
				envkeys.UserEnvPrefix + "CI_SERVER_URL":         validServerURL,
				envkeys.UserEnvPrefix + envkeys.CIJobJWT:        validJWT,
				envkeys.UserEnvPrefix + "CI_COMMIT_DESCRIPTION": "",
				envkeys.UserEnvPrefix + "CI_PROJECT_VISIBILITY": "private",
				"BUILD_FAILURE_EXIT_CODE":                       "1",
				"TEST_PATH":                                     "/usr/bin:/usr/sbin",
				envkeys.JobResponse:                             "../../test/testdata/valid_job_response.json",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutorEnv: func(t *testing.T, env ExecutorEnv) {
				assert.Equal(t, RequiredEnv{
					ConcurrentID: validConcurrentID,
					JobID:        validJobID,
					JobToken:     validJobToken,
					RunnerShort:  validRunnerShort,
					ServerURL:    validServerURL,
					CIJobJWT:     validJWT,
				}, env.RequiredEnv)
				assert.Equal(t, 3600, env.JobResponse.RunnerInfo.Timeout)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			env, err := Fetcher(tt.stateReq, tt.c, tt.opt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutorEnv != nil {
				tt.assertExecutorEnv(t, env)
			}
		})
	}
}

func TestKeyVarMapping(t *testing.T) {
	type StandardTest struct {
		ID  string `key:"TEST_ID"`
		SHA string `key:"TEST_SHA"`
	}
	type RequiredTest struct {
		ID    string `key:"TEST_ID" required:"false"`
		Token string `key:"TEST_TOKEN" required:"true"`
	}
	type NonString struct {
		ID int `key:"TEST_ID"`
	}

	var testString string

	tests := map[string]struct {
		i               interface{}
		env             map[string]string
		assertError     func(*testing.T, error)
		assertInterface func(*testing.T, interface{})
	}{
		"empty interface": {
			i: nil,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid nil interface provided")
			},
		},
		"standard test interface": {
			i: &StandardTest{},
			env: map[string]string{
				"TEST_ID":  "123",
				"TEST_SHA": "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertInterface: func(t *testing.T, i interface{}) {
				assert.Equal(t, &StandardTest{
					ID:  "123",
					SHA: "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317",
				}, i)
			},
		},
		"required value provided": {
			i: &RequiredTest{},
			env: map[string]string{
				"TEST_ID":    "123",
				"TEST_TOKEN": "T0k3n",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertInterface: func(t *testing.T, i interface{}) {
				assert.Equal(t, &RequiredTest{
					ID:    "123",
					Token: "T0k3n",
				}, i)
			},
		},
		"required value missing": {
			i: &RequiredTest{},
			env: map[string]string{
				"TEST_ID": "123",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing required environment variable TEST_TOKEN")
			},
		},
		"optional value missing": {
			i: &RequiredTest{},
			env: map[string]string{
				"TEST_TOKEN": "T0k3n",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertInterface: func(t *testing.T, i interface{}) {
				assert.Equal(t, &RequiredTest{
					Token: "T0k3n",
				}, i)
			},
		},
		"non-string type encountered": {
			i: &NonString{},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid field type (int) encountered")
			},
		},
		"non-struct type interface": {
			i: &testString,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid non-struct interface provided")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := KeyVarMapping(tt.i, tt.env)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertInterface != nil {
				tt.assertInterface(t, tt.i)
			}
		})
	}

	for n := 0; n < 10; n++ {
		t.Run("repeat run, missing variable", func(t *testing.T) {
			i := &RequiredTest{}
			err := KeyVarMapping(i, map[string]string{
				"TEST_ID": "123",
			})
			assert.EqualError(t, err, "missing required environment variable TEST_TOKEN")
		})
	}
}

func TestRequiredEnv_override(t *testing.T) {
	tests := map[string]envTests{
		"no changes required": {
			req: &RequiredEnv{
				ServerURL: "example.com",
				JobID:     "123",
				CIJobJWT:  "example.working.jwt",
			},
			assertRequiredEnv: func(t *testing.T, env *RequiredEnv) {
				assert.Equal(
					t,
					RequiredEnv{
						ServerURL: "example.com",
						JobID:     "123",
						CIJobJWT:  "example.working.jwt",
					},
					*env,
				)
			},
		},
		"override server url": {
			req: &RequiredEnv{
				CIJobJWT: "example.com",
			},
			opt: configure.Options{
				General: configure.General{
					GitLabServer: "https://example.com/server",
				},
			},
			assertRequiredEnv: func(t *testing.T, env *RequiredEnv) {
				assert.Equal(t, "https://example.com/server", env.ServerURL)
			},
		},
		"fail to override jwt": {
			req: &RequiredEnv{
				CIJobJWT: "example.working.jwt",
			},
			opt: configure.Options{
				General: configure.General{
					FFJWTV2: true,
				},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIJobJWTV2: "",
			},
			assertRequiredEnv: func(t *testing.T, env *RequiredEnv) {
				assert.Equal(t, "example.working.jwt", env.CIJobJWT)
			},
		},
		"override jwt": {
			req: &RequiredEnv{
				CIJobJWT: "example.working.jwt",
			},
			opt: configure.Options{
				General: configure.General{
					FFJWTV2: true,
				},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIJobJWTV2: "version.two.jwt",
			},
			assertRequiredEnv: func(t *testing.T, env *RequiredEnv) {
				assert.Equal(t, "version.two.jwt", env.CIJobJWT)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			tt.req.override(tt.opt)

			if tt.assertRequiredEnv != nil {
				tt.assertRequiredEnv(t, tt.req)
			}
		})
	}
}
