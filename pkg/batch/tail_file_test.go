package batch

import (
	"errors"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type tailTests struct {
	file      string
	maxWait   time.Duration
	filenames []string

	msg logging.Messenger

	done         chan struct{}
	manageFile   func(string)
	closeChannel bool

	assertError      func(*testing.T, error)
	mockWatchFactory func(string, chan struct{}, logging.Messenger) error
}

func Test_TailFiles(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	f, _ := os.CreateTemp(t.TempDir(), "tailFiles")

	tests := map[string]tailTests{
		"no filenames provided": {
			filenames: []string{},
			done:      make(chan struct{}),
			maxWait:   10 * time.Second,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err, "no filenames should not results in any error")
			},
		},
		"valid + invalid filename provided": {
			filenames: []string{f.Name(), "/random/file/123.txt"},
			done:      make(chan struct{}),
			maxWait:   10 * time.Second,
			assertError: func(t *testing.T, err error) {
				assert.Equal(
					t,
					err.Error(),
					"unable to locate /random/file/123.txt before timeout",
				)
			},
		},
		"invalid time duration provided": {
			filenames: []string{},
			done:      make(chan struct{}),
			maxWait:   -1,
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, err.Error(), "invalid time (-1ns) provided, must be >= 0")
			},
		},
		"no maxTime configured + invalid filename": {
			filenames: []string{"/random/file/123.txt"},
			done:      make(chan struct{}),
			maxWait:   0,
			assertError: func(t *testing.T, err error) {
				assert.Equal(
					t,
					err.Error(),
					"unable to locate /random/file/123.txt before timeout",
				)
			},
		},
		"invoke empty watchFactory": {
			filenames: []string{f.Name()},
			done:      make(chan struct{}),
			maxWait:   0,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			mockWatchFactory: func(file string, done chan struct{}, msg logging.Messenger) error {
				return nil
			},
		},
		"message failures in watch without causing error": {
			filenames: []string{f.Name()},
			done:      make(chan struct{}),
			msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Warn("error encountered tailing job output: %s", gomock.Any())
				m.EXPECT().Stdout("scheduled job will continue without output")
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			mockWatchFactory: func(file string, done chan struct{}, msg logging.Messenger) error {
				return errors.New("example error")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			defer close(tt.done)
			if tt.mockWatchFactory != nil {
				watchFactory = tt.mockWatchFactory
			}

			if tt.msg == nil {
				tt.msg = logging.NewMessenger()
			}

			j := Job{}
			err := j.TailFiles(tt.filenames, tt.done, tt.maxWait, tt.msg)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_waitFile(t *testing.T) {
	f, _ := os.CreateTemp(t.TempDir(), "waitFile")

	tests := map[string]tailTests{
		"unable to find file + valid max wait time": {
			file:    "/random/file/123.txt",
			maxWait: 12 * time.Second,
			assertError: func(t *testing.T, err error) {
				assert.Equal(t, err.Error(), "file /random/file/123.txt not found")
			},
		},
		"unable to find file + no max wait time": {
			file:    "/random/file/123.txt",
			maxWait: 0,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"test file exists + valid max timeout": {
			file:    f.Name(),
			maxWait: 12 * time.Second,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"no test file declared": {
			file:    "",
			maxWait: 12 * time.Second,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := waitFile(tt.file, tt.maxWait)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_watch(t *testing.T) {
	// shorten limitation that is intended for NFS
	watchWait = 1 * time.Second

	f1, _ := os.CreateTemp(t.TempDir(), "watch")
	f2, _ := os.CreateTemp(t.TempDir(), "watch")
	f3, _ := os.CreateTemp(t.TempDir(), "watch")

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	noMsg := mock_logging.NewMockMessenger(ctrl)

	helloWorld := mock_logging.NewMockMessenger(ctrl)
	helloWorld.EXPECT().Stdout("Hello").Times(1)
	helloWorld.EXPECT().Stdout("World!").Times(1)

	countFile := mock_logging.NewMockMessenger(ctrl)
	countFile.EXPECT().Stdout("1").Times(1)
	countFile.EXPECT().Stdout("2").Times(1)
	countFile.EXPECT().Stdout("3").Times(1)
	countFile.EXPECT().Stdout("4").Times(1)

	tests := map[string]tailTests{
		"valid file provided and contents written": {
			file: f1.Name(),
			done: make(chan struct{}),
			msg:  helloWorld,
			manageFile: func(filename string) {
				time.Sleep(2 * time.Second)
				f, _ := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 700)
				_, _ = f.WriteString("Hello\nWorld!\n")
				time.Sleep(2 * time.Second)
			},
		},
		"no filename provided": {
			file: "",
			msg:  noMsg,
			done: make(chan struct{}),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"invalid filename provided": {
			file: "/random/file/123.txt",
			done: make(chan struct{}),
			msg:  noMsg,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "open /random/file/123.txt: no such file or directory")
			},
		},
		"valid file provided and removed unexpectedly": {
			file: f2.Name(),
			done: make(chan struct{}),
			// Simulating stat error event cannot be done predictably.
			msg: logging.NewMessenger(),
			manageFile: func(filename string) {
				time.Sleep(2 * time.Second)
				_ = os.Remove(filename)
			},
		},
		"valid file provided and renamed unexpectedly": {
			file: f2.Name(),
			done: make(chan struct{}),
			// Simulating stat error event cannot be done predictably.
			msg: logging.NewMessenger(),
			manageFile: func(filename string) {
				time.Sleep(2 * time.Second)
				_ = os.Rename(filename, t.TempDir()+"/renamed.file")
			},
		},
		"eowf reached": {
			file: f3.Name(),
			done: make(chan struct{}),
			msg:  countFile,
			manageFile: func(filename string) {
				f, _ := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 700)
				_, _ = f.WriteString("1\n")
				time.Sleep(1 * time.Second)
				_, _ = f.WriteString("2\n")
				time.Sleep(1 * time.Second)
				_, _ = f.WriteString("3\n")
				time.Sleep(1 * time.Second)
				_, _ = f.WriteString("4\n")
				time.Sleep(1 * time.Second)
				_ = f.Close()
			},
		},
	}

	for name, tt := range tests {
		var wg sync.WaitGroup
		wg.Add(1)

		go func() {
			defer wg.Done()
			if tt.manageFile != nil {
				tt.manageFile(tt.file)
			}
			time.Sleep(2 * time.Second)
			close(tt.done)
		}()

		var err error
		t.Run(name, func(t *testing.T) {
			err = watch(tt.file, tt.done, tt.msg)
		})
		wg.Wait()

		if tt.assertError != nil {
			tt.assertError(t, err)
		}
	}
}

func TestCreateFiles(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn("Error while attempting to create file (%s): %s", "/invalid/file/name", "open /invalid/file/name: no such file or directory").Times(1)

	tests := map[string]struct {
		files       []string
		msg         logging.Messenger
		assertFiles func(*testing.T, []string)
	}{
		"create files successfully": {
			files: []string{"/tmp/file1", "/tmp/file2"},
			msg:   m,
			assertFiles: func(t *testing.T, s []string) {
				for _, f := range s {
					info, err := os.Stat(f)
					assert.NoError(t, err, "unable to state file")
					assert.Equal(t, "-rw-------", info.Mode().String())
				}
			},
		},
		"error encountered while attempting to create files": {
			files: []string{"/invalid/file/name"},
			msg:   m,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			CreateFiles(tt.files, tt.msg)

			if tt.assertFiles != nil {
				tt.assertFiles(t, tt.files)
			}
		})
	}
}
