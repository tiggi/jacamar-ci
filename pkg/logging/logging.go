// Package logging maintains interface for two core supported types of logging
// that occur within Jacamar. First is communication with the job log that is
// handled via a Messenger. Secondly is the optionally configured system
// logging that is provided via the Logger. Any system logging is targeted at
// a privileged user with Jacamar-Auth.
package logging

import (
	"fmt"
	"io"
	"log"
	"log/syslog"
	"os"
	"path/filepath"
	"strconv"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
	lWriter "github.com/sirupsen/logrus/hooks/writer"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// Using the same codes as upstream GitLab.
const (
	ANSIBoldRed    = "\033[31;1m" // Error
	ANSIBoldGreen  = "\033[32;1m" // Notify
	ANSIYellow     = "\033[0;33m" // Warning
	ANSIReset      = "\033[0;m"
	logPermissions = 0600
)

// Messenger is the interface to conveying key user information to the
// current CI job log via stdout/stderr to the GitLab runner. Messages
// of sufficient lever can be wrapped in appropriate ANSI characters to
// increase visibility to the user.
type Messenger interface {
	// Stdout prints a formatted message (no color) to stdout.
	Stdout(string, ...interface{})
	// Stderr prints a formatted message (no color) to stderr.
	Stderr(string, ...interface{})
	// Warn prints a formatted message (yellow text) to stdout.
	Warn(string, ...interface{})
	// Notify prints a formatted message (bold green text) to stdout.
	Notify(string, ...interface{})
	// Error prints a formatted message (bold red text) to stderr.
	Error(string, ...interface{})
}

type jobLog struct{}

// NewMessenger generates a functional Messenger interface that should be used
// to convey details/messages to the job log.
func NewMessenger() Messenger {
	return jobLog{}
}

// Stdout prints a formatted message (no color) to stdout.
func (j jobLog) Stdout(msg string, a ...interface{}) {
	msg = format(msg, a...)
	printStdout(msg)
}

// Stderr prints a formatted message (no color) to stderr.
func (j jobLog) Stderr(msg string, a ...interface{}) {
	msg = format(msg, a...)
	printStderr(msg)
}

// Warn prints a formatted message (yellow text) to stdout.
func (j jobLog) Warn(msg string, a ...interface{}) {
	msg = ANSIYellow + format(msg, a...) + ANSIReset
	printStdout(msg)
}

// Notify prints a formatted message (bold green text) to stdout.
func (j jobLog) Notify(msg string, a ...interface{}) {
	msg = ANSIBoldGreen + format(msg, a...) + ANSIReset
	printStdout(msg)
}

// Error prints a formatted message (bold red text) to stderr.
func (j jobLog) Error(msg string, a ...interface{}) {
	msg = ANSIBoldRed + format(msg, a...) + ANSIReset
	printStderr(msg)
}

func format(msg string, a ...interface{}) string {
	if a == nil {
		return msg
	}
	return fmt.Sprintf(msg, a...)
}

func printStdout(msg ...interface{}) {
	_, _ = fmt.Fprintln(os.Stdout, msg...)
}

func printStderr(msg ...interface{}) {
	_, _ = fmt.Fprintln(os.Stderr, msg...)
}

type LogFile struct {
	file string
}

func (l LogFile) Write(p []byte) (n int, err error) {
	f, err := os.OpenFile(l.file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, logPermissions)
	if err != nil {
		return 0, err
	}
	defer func() { _ = f.Close() }()

	return f.Write(p)
}

// EstablishLogger uses a combination of known/trusted environmental context coupled with
// administrator configuration to establish rules for Logrus.
func EstablishLogger(
	stage string,
	opts configure.Options,
	req envparser.RequiredEnv,
) (*logrus.Entry, error) {
	if !opts.Auth.Logging.Enabled {
		logger := logrus.New()
		logger.Out = io.Discard
		return logrus.NewEntry(logger), nil
	}

	hook, err := loggingHook(opts.Auth.Logging)
	if err != nil {
		return nil, err
	}

	logger := &logrus.Logger{
		Out: io.Discard,
		Formatter: &logrus.JSONFormatter{
			DisableTimestamp: false,
		},
		Hooks: make(logrus.LevelHooks),
		Level: loggingLevel(opts.Auth.Logging),
	}
	logger.AddHook(hook)
	loggerEntry := logger.WithFields(fields(stage, opts, req))

	return loggerEntry, nil
}

func loggingHook(logOpts configure.Logging) (logrus.Hook, error) {
	if logOpts.Location == "syslog" || logOpts.Location == "" {
		// Level from Logrus Entry observed, set to lowest supported level.
		return lSyslog.NewSyslogHook(
			logOpts.Network,
			logOpts.Address,
			syslog.LOG_DEBUG,
			"jacamar-auth",
		)
	}

	filePath := filepath.Clean(logOpts.Location)
	hook := &lWriter.Hook{
		Writer: LogFile{
			file: filePath,
		},
		LogLevels: logrus.AllLevels,
	}

	return hook, nil
}

func loggingLevel(logOpts configure.Logging) logrus.Level {
	level, err := logrus.ParseLevel(logOpts.Level)
	if err != nil {
		log.Println("invalid logging level defined, defaulting to debug")
		level = logrus.DebugLevel
	}

	return level
}

func fields(stage string,
	opts configure.Options,
	req envparser.RequiredEnv,
) logrus.Fields {
	// JobID validated as number in earlier steps.
	jobID, _ := strconv.Atoi(req.JobID)

	f := logrus.Fields{
		"job":       jobID,
		"runner":    req.RunnerShort,
		"stage":     stage,
		"processID": os.Getpid(),
	}

	if opts.General.Name != "" {
		f["jacamar-name"] = opts.General.Name
	}

	host, err := os.Hostname()
	if err == nil {
		// Failure to identify hostname should not result in a job failure.
		f["hostname"] = host
	}

	return f
}
