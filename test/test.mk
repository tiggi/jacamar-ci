.PHONY: test
test: #T Run entire Go test suite locally, coverage report generated.
	${GOCMD} test -p 1 -coverprofile ${COVER_REPORT} -cover -tags netgo -timeout 2m -v ./... \
		&& ${GOCMD} tool cover -func=${COVER_REPORT}

.PHONY: test-package
test-package: #T Run Go test targeting specific package (e.g., 'test-package PKG=pkg/configure')
	${GOCMD} test -p 1 -tags netgo -timeout 2m -v ./${PACKAGE}

.PHONY: test-container
test-container: #T Run entire Go test suite using defined container runtime.
	@bash ./test/scripts/test_container.bash test ${PACKAGE}

.PHONY: test-quality
test-quality: #T Go static linting (requires: https://github.com/golangci/golangci-lint).
	golangci-lint --version
	golangci-lint run

.PHONY: test-quality-container
test-quality-container: #T Run static linting using defined container runtime.
	${CONTAINER_RUNTIME} run \
      --rm \
      -v "${ROOT_DIR}:${ci_dir}:z" \
      -e "CI_PROJECT_DIR=${ci_dir}" \
      -w "${ci_dir}" \
      -t "${GO_IMG}" \
      bash -c "make test-quality"

.PHONY: test-security
test-security: #T Go security check (requires: https://github.com/securego/gosec).
	gosec --version
	gosec -exclude-dir=tools -exclude-dir=test  -exclude-dir=build ./...

.PHONY: test-security-container
test-security-container: #T Run security check using defined container runtime.
	${CONTAINER_RUNTIME} run \
      --rm \
      -v "${ROOT_DIR}:${ci_dir}:z" \
      -e "CI_PROJECT_DIR=${ci_dir}" \
      -w "${ci_dir}" \
      -t "${GO_IMG}" \
      bash -c "make test-security"

.PHONY: test-vuln
test-vuln: #T Go vulnerability check (see for overview: https://go.dev/blog/vuln)
	go install golang.org/x/vuln/cmd/govulncheck@latest
	govulncheck -v -json ./... > govulncheck.json
	go run tools/vulnerability-check/vulnerability-check.go govulncheck.json ignored-vulns.json

.PHONY: test-vuln-container
test-vuln-container: #T Run vulnerability check using defined container runtime.
	${CONTAINER_RUNTIME} run \
	--rm \
	-v "${ROOT_DIR}:${ci_dir}:z" \
	-e "CI_PROJECT_DIR=${ci_dir}" \
	-w "${ci_dir}" \
	-t "${GO_IMG}" \
	bash -c "make test-vuln"

.PHONY: check-mod
check-mod: #T Check if modifications to go.sum required during CI/CD.
	${GOCMD} mod tidy
	git diff --exit-code -- go.sum

.PHONY: coverage
coverage: #M Analyze Go coverage profile.
	${GOCMD} tool cover -func=${COVER_REPORT}

TARGET_JOB_ID ?= ""

.PHONY: buildtest-nersc
buildtest-nersc:
	@bash ./test/buildtest/nersc/run.bash ${TARGET_JOB_ID}

RUNNER_VERSION ?= ""

.PHONY: runner-env-startup
runner-env-startup: #T Install and run preferred runner version in a container (defaults to latest nightly build if no version is selected).
	@bash ./test/scripts/runner-env.bash ${RUNNER_VERSION}

############################################################
# Keep Docker commands to support backwards compatability. #
############################################################

.PHONY: test-docker
test-docker:
	CONTAINER_RUNTIME=docker $(MAKE) test-container
