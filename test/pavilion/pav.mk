export PAV_CONFIG_DIR=${CI_PROJECT_DIR}/test/pavilion

#
# General workflows
.PHONY: pav-clean
pav-clean:
	cd "${PAV_CONFIG_DIR}/working_dir" &&  ls | grep -v .gitignore | xargs rm -rf ||:

.PHONY: pav-container-build
pav-container-build: #T Test build application for targeted Pavilion2 testing via a container.
	@bash ./test/pavilion/run/start.bash build

#
# Integration test workflows
.PHONY: pav-container-auth
pav-container-auth: #T Test jacamar-auth functionality using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash auth

.PHONY: pav-container-capabilities
pav-container-capabilities: #T Test jacamar-auth with Linux capabilities using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash capabilities

.PHONY: pav-container-flux
pav-container-flux: #T Testing Flux executor using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash flux

.PHONY: pav-container-jacamar
pav-container-jacamar: #T Test jacamar application using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash jacamar

.PHONY: pav-container-log
pav-container-log: #T Test jacamar-auth logging using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash log

.PHONY: pav-container-pbs
pav-container-pbs: #T Testing PBS executor using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash pbs

.PHONY: pav-container-slurm
pav-container-slurm: #T Testing Slurm executor using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash slurm

.PHONY: pav-container-su
pav-container-su: #T Test jacamar-auth with Linux su(1) using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash su

.PHONY: pav-container-seccomp
pav-container-seccomp: #T Test seccomp filters using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash seccomp

.PHONY: pav-container-sudo
pav-container-sudo: #T Test jacamar-auth with Linux sudo(8) using Pavilion2 via a container.
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash sudo

.PHONY: pav-container-strace
pav-container-strace:
	$(MAKE) pav-clean
	@bash ./test/pavilion/run/start.bash strace


############################################################
# Keep Docker commands to support backwards compatability. #
############################################################

.PHONY: pav-docker-auth
pav-docker-auth:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-auth

.PHONY: pav-docker-build
pav-docker-build:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-build

.PHONY: pav-docker-capabilities
pav-docker-capabilities:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-capabilities

.PHONY: pav-docker-jacamar
pav-docker-jacamar:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-jacamar

.PHONY: pav-docker-slurm
pav-docker-slurm:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-slurm

.PHONY: pav-docker-su
pav-docker-su:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-su

.PHONY: pav-docker-seccomp
pav-docker-seccomp:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-seccomp

.PHONY: pav-docker-sudo
pav-docker-sudo:
	CONTAINER_RUNTIME=docker $(MAKE) pav-container-sudo

.PHONY: pav-container-clean
pav-container-clean:
	$(MAKE) pav-clean
