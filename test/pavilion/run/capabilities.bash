#!/usr/bin/env bash

# Execute Pavilion tests within a containers.

set -eo pipefail
set +o noclobber

source /tmp/jacamar-ci/test/pavilion/run/functions.bash

migrate
mock_gl_api

# *IMPORTANT* this script is designed to prepare capabilities
# focused testing for pav-container-capabilities. Do no attempt
# to run outside the associated container environment.

mkdir -p /opt/jacamar/bin

cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-auth
cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-chown
cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-perms
cp /usr/local/sbin/jacamar-auth /opt/jacamar/bin/jacamar-inheritable

chmod 700 -R /opt
chown jacamar:jacamar -R /opt
chmod 705 /opt/jacamar/bin/jacamar-perms

# Set capabilities
setcap cap_setuid,cap_setgid+ep /opt/jacamar/bin/jacamar-auth
setcap cap_chown,cap_setuid,cap_setgid+ep /opt/jacamar/bin/jacamar-chown
setcap cap_setuid,cap_setgid+ep /opt/jacamar/bin/jacamar-perms
setcap cap_chown,cap_setuid,cap_setgid+epi /opt/jacamar/bin/jacamar-inheritable

# Directory creation
mkdir /ecp
chmod 701 /ecp
mkdir /ecp/user
chown user:user /ecp/user
chmod 700 /ecp/user
chown jacamar:jacamar /ecp

mkdir /ecp-chown
chmod 701 /ecp-chown
chown jacamar:jacamar /ecp-chown

su jacamar -c "source ${PAV_CI_DIR}/test/pavilion/run/functions.bash && pav_series" && rs=0 || rs=1
relocate

exit $rs
