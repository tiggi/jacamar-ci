#!/usr/bin/env bash

# Execute Pavilion tests within a containers.

set -eo pipefail
set +o noclobber

source /tmp/jacamar-ci/test/pavilion/run/functions.bash

migrate
mock_gl_api
su sudo_user -c "source ${PAV_CI_DIR}/test/pavilion/run/functions.bash && pav_series" && rs=0 || rs=1
relocate

exit $rs
