#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

source /tmp/jacamar-ci/test/pavilion/run/functions.bash

cp /usr/local/sbin/* /usr/local/bin

/etc/init.d/pbs start
/opt/pbs/bin/qmgr -c "set server job_history_enable=1"

migrate
mock_gl_api

su pbsadmin -c "source ${PAV_CI_DIR}/test/pavilion/run/functions.bash && pav_series" && rs=0 || rs=1

relocate

exit $rs
