//go:build linux

package main

import (
	"syscall"

	libseccomp "github.com/seccomp/libseccomp-golang"
)

func SeccompExpansion(filter *libseccomp.ScmpFilter, stage string) error {
	var err error

	if stage != "" {
		callID, _ := libseccomp.GetSyscallFromName("mkdir")
		err = filter.AddRule(callID, libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM)))
	}

	return err
}
