#!/bin/bash

set -x

echo "start $(date)"
sleep 42
echo "end $(date)"
touch "/tmp/background-${CUSTOM_ENV_CI_JOB_ID}"
