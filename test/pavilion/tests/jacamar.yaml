_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
  run:
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '1h0m0s'

_job-home-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/user/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/user/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/user/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/user_authorized_setuid.toml")}}'

_job-dir-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /var/tmp/user
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/builds
      JACAMAR_CI_CACHE_DIR: /var/tmp/user/cache
      JACAMAR_CI_SCRIPT_DIR: /var/tmp/user/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/dir_authorized_setuid.toml")}}'

_job-home-save:
  inherits_from: _job-home-env
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 456
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/456
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/job_home_save.toml")}}'

_job_tmp_none:
  inherits_from: _cfg-env
  run:
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 444
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      JACAMAR_CI_BASE_DIR: /var/tmp/user
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/builds
      JACAMAR_CI_CACHE_DIR: /var/tmp/user/cache
      JACAMAR_CI_SCRIPT_DIR: /var/tmp/user/scripts/group/project/444
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/auth_none_downscope.toml")}}'

_invalid_cfg_str:
  inherits_from: _job-home-env
  run:
    env:
      JACAMAR_CI_CONFIG_STR: INVALID123

_job-static-shell:
  inherits_from: _job-dir-env
  run:
    env:
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/static/00000123
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/static_shell.toml")}}'

_job-static-keep:
  inherits_from: _job-dir-env
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 456
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/static/00000456
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/static_keep.toml")}}'

###############################################################################
# general tests
###############################################################################

signal-sub-command:
  summary: Verify jacamar signal basic functionality.
  run:
    cmds:
      - "sleep 60&"
      - "export SIGTERM_PID=$!"
      - "jacamar --no-auth signal SIGKILL ${SIGTERM_PID}"
      - "sleep 1" # avoid timing issue seen with docker-in-docker
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sleep process is still running'"
  result_parse:
    regex:
      ps:
        regex: 'sleep process is still running'
        action: store_false
  result_evaluate:
    result: 'ps'

missing-no-auth:
  # Limit risk of accidental miss-configure.
  summary: Cannot be run without no-auth flag.
  run:
    cmds:
      - "jacamar run some-script.bash step_script"
  result_parse:
    regex:
      error:
        regex: 'Jacamar requires --no-auth in order to support execution requirements.'
        action: store_true
  result_evaluate:
    result: 'error and return_value == 1'

###############################################################################
# config_exec tests
###############################################################################

config-shell-dir:
  inherits_from: _cfg-env
  summary: Load a valid shell executor configuration targeting data_dir directory.
  run:
    cmds:
      - 'jacamar --no-auth config --configuration {{configs}}/general/custom_shell.toml'

config-shell-home:
  inherits_from: _cfg-env
  summary: Load a valid shell executor configuration targeting home directory.
  run:
    cmds:
      - 'jacamar --no-auth config --configuration {{configs}}/general/job_home_env.toml'

config-custom-dir:
  inherits_from: _cfg-env
  summary: Load a valid shell executor configuration targeting data_dir directory.
  run:
    env:
      CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR: /custom/builds/dir
    cmds:
      - 'jacamar --no-auth config --configuration {{configs}}/general/custom_shell.toml'
  result_parse:
    regex:
      builds_dir:
        regex: '"builds_dir":"/custom/builds/dir/user/builds/token123/000"'
        action: store_true
      timeout:
        regex: '"JACAMAR_CI_RUNNER_TIMEOUT":"2h0m0s"'
        action: store_true
  result_evaluate:
    result: 'builds_dir and timeout'

config-none-downscope:
  inherits_from: _cfg-env
  summary: Allow optional use of jacamar-auth while downscope is none.
  run:
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/general/auth_none_downscope.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      timeout:
        regex: '"JACAMAR_CI_RUNNER_TIMEOUT":"2h0m0s"'
        action: store_true
  result_evaluate:
    result: 'username and timeout'

config-static-builds:
  inherits_from: _cfg-env
  summary: Utilize static builds directory.
  run:
    env:
      CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR: /custom/builds/dir
    cmds:
      - 'jacamar --no-auth config --configuration {{configs}}/general/static_shell.toml'
  result_parse:
    regex:
      builds_dir:
        regex: '"builds_dir":"/var/tmp/user/static/00000123"'
        action: store_true
  result_evaluate:
    result: 'builds_dir'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-home-dir:
  inherits_from: _job-home-env
  summary: User's home directory prepared for job.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
      # We don't need to confirm permissions, only the folder existence as we
      # rely on the $HOME directory permissions.

prepare-data-dir:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
  result_parse:
    regex:
      base_dir:
        regex: 'drwx------ user user user'
        action: store_true
  result_evaluate:
    result: 'base_dir and return_value == 0'

prepare-none-downscope:
  inherits_from: _job_tmp_none
  summary: Allow optional use of jacamar-auth while downscope is none.
  run:
    cmds:
      - 'jacamar-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'

prepare-setuid-invalid:
  inherits_from: _cfg-env
  summary: Unauthorized use of setuid error encountered.
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/tester/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/tester/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/tester/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/tester/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: tester
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/jacamar_authorized_setuid.toml")}}'
    cmds:
      - 'jacamar-auth -u prepare'
  result_parse:
    regex:
      output:
        regex: 'fork/exec .*: operation not permitted'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 1'

prepare-valid-symlink:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    env:
      SYMLINK_BASE_DIR: /var/tmp/user/valid-sym
      JACAMAR_CI_BASE_DIR: /home/user/valid-sym
      JACAMAR_CI_BUILDS_DIR: /home/user/valid-sym/builds
      JACAMAR_CI_CACHE_DIR: /home/user/valid-sym/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/valid-sym/scripts/slug/test/123
    cmds:
      - 'mkdir -p $SYMLINK_BASE_DIR'
      - 'ln -s $SYMLINK_BASE_DIR $JACAMAR_CI_BASE_DIR'
      - 'jacamar --no-auth prepare'
      - 'namei -l $SYMLINK_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
  result_parse:
    regex:
      base_dir:
        regex: 'drwx------ user user valid-sym'
        action: store_true
      builds_sym:
          regex: 'lrwxrwxrwx user user valid-sym -> /var/tmp/user/valid-sym'
          action: store_true
      builds_dir:
        regex: 'drwx------ user user builds'
        action: store_true
  result_evaluate:
    result: 'base_dir and builds_sym and builds_dir and return_value == 0'

prepare-invalid-symlink:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    env:
      SYMLINK_BASE_DIR: /root/invalid-sym
      JACAMAR_CI_BASE_DIR: /home/user/invalid-sym
    cmds:
      - 'ln -s $SYMLINK_BASE_DIR $JACAMAR_CI_BASE_DIR'
      - 'ls -l /home/user'
      - 'jacamar --no-auth prepare'
  result_parse:
    regex:
      err_msg:
        regex: 'failed to resolve target directory: lstat'
        action: store_true
  result_evaluate:
    result: 'err_msg and return_value == 2'

prepare-symlink-permissions:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    env:
      SYMLINK_BASE_DIR: /var/tmp/permissions-sym
      JACAMAR_CI_BASE_DIR: /home/user/permissions-sym
    cmds:
      - 'mkdir -p $SYMLINK_BASE_DIR'
      - 'ln -s $SYMLINK_BASE_DIR $JACAMAR_CI_BASE_DIR'
      - 'chmod 774 $SYMLINK_BASE_DIR'
      - 'jacamar --no-auth prepare'
  result_parse:
    regex:
      err_msg:
        regex: 'invalid permissions for directory'
        action: store_true
  result_evaluate:
    result: 'err_msg and return_value == 2'

prepare-keep-static:
  inherits_from: _job-static-keep
  summary: Verify static directories kept after prepare/cleanup actions.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'jacamar --no-auth cleanup --configuration {{configs}}/general/static_keep.toml'
      - 'ls -l /var/tmp/user/static'
  result_parse:
    regex:
      output:
        regex: '00000456'
        action: store_true
  result_evaluate:
    result: 'output'

###############################################################################
# run_exec tests
###############################################################################

run-clean-env:
  inherits_from: _job-home-env
  summary: Existing environment variables should be cleansed before execution.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 123
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      FOO: "BAR"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /var/tmp/get-env.bash'
      - 'echo "env | grep FOO && exit 1 || echo "PASS"" >> /var/tmp/get-env.bash'
      - 'jacamar --no-auth run /var/tmp/get-env.bash step_script'

run-pass-script:
  inherits_from: _job-home-env
  summary: Run passing job script.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 456
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/456
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /var/tmp/job-pass.bash'
      - 'jacamar --no-auth run /var/tmp/job-pass.bash step_script'

run-fail-script:
  inherits_from: _job-home-env
  summary: Run failing job script.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 789
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/789
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-failure.bash /var/tmp/job-failure.bash'
      - 'jacamar --no-auth run /var/tmp/job-failure.bash step_script'
  result_evaluate:
    result: 'return_value == 1'

run-background-script:
  inherits_from: _job-home-env
  summary: Run passing job that background sleep process.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1011
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1011
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-background.bash /var/tmp/job-background.bash'
      - 'jacamar --no-auth run /var/tmp/job-background.bash step_script'
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-BACKGROUND"'
  result_parse:
    regex:
      output:
        regex: 'SUCCESS-BACKGROUND'
        action: store_true
  result_evaluate:
    result: 'output'

run-none-downscope:
  inherits_from: _job_tmp_none
  summary: None downscope type succesfully runs job.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["job_id=1213","sub=job_1213"])}}'
      CUSTOM_ENV_CI_JOB_ID: 1213
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/1213
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /var/tmp/job-downscope.bash'
      - 'jacamar-auth -u run /var/tmp/job-downscope.bash step_script'

run-context-timeout:
  inherits_from: _job-home-env
  summary: Encounter response defined timeout and cancle job.
  variables:
    jobscript: /tmp/contexttimeout.bash
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 5000
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/5000
      JACAMAR_CI_RUNNER_TIMEOUT: '4s'
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - 'jacamar --no-auth prepare'
      - 'jacamar --no-auth run {{jobscript}} step_script'
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-CONTEXT-TIMEOUT"'
  result_parse:
    regex:
      output:
        regex: 'SUCCESS-CONTEXT-TIMEOUT'
        action: store_true
  result_evaluate:
    result: 'output'

###############################################################################
# cleanup_exec tests
###############################################################################

cleanup-invalid-cfg-encoding:
  inherits_from: _invalid_cfg_str
  summary: Incorrectly encoded configuration string provided.
  run:
    cmds:
      - 'jacamar --no-auth cleanup --configuration not_found.toml'
  result_parse:
    regex:
      output:
        regex: 'unable to establish Configurer'
        action: store_true
  result_evaluate:
    result: 'output'

cleanup-missing-directories:
  inherits_from: _job-dir-env
  summary: Cleanup requires
  run:
    env:
      JACAMAR_CI_BUILDS_DIR: /dir/not/found
    cmds:
      - 'jacamar --no-auth cleanup --configuration {{configs}}/general/dir_authorized_setuid.toml'
  result_parse:
    regex:
      output:
        regex: 'file does not exist'
        action: store_true
  result_evaluate:
    result: 'output'

cleanup-verify-script:
  inherits_from: _job-home-save
  summary: Check job cleanup and log retention.
  variables:
    script_dir: /home/user/.jacamar-ci/scripts/token123/0/group/project/123
  run:
    env:
      CUSTOM_ENV_COPY_SCHEDULER_LOGS: /home/user/logs
    cmds:
      - 'mkdir -p ${JACAMAR_CI_BASE_DIR} ${JACAMAR_CI_SCRIPT_DIR} ${JACAMAR_CI_CACHE_DIR} ${JACAMAR_CI_BUILDS_DIR}'
      - 'chmod -R 700 ${JACAMAR_CI_BASE_DIR}'
      - 'touch ${JACAMAR_CI_SCRIPT_DIR}/{output,error}'
      - 'jacamar --no-auth cleanup --configuration {{configs}}/general/job_home_save.toml'
      - 'echo "logs moved $(ls -l ${CUSTOM_ENV_COPY_SCHEDULER_LOGS}/456 | grep -v ^l | wc -l)"'
      - 'echo "logs retained $(ls -l ${JACAMAR_CI_SCRIPT_DIR} | grep -v ^l | wc -l)"'
  result_parse:
    regex:
      logs:
        regex: 'logs moved\s*[2-4]'
        action: store_true
      retain:
        regex: 'logs retained\s*[2-4]'
        action: store_true
  result_evaluate:
    result: 'logs and retain'

cleanup-credentials-removed:
  inherits_from: _job-home-save
  summary: Verify Git credentials removed during clenaup.
  run:
    env:
      JACAMAR_CI_BUILDS_DIR: /home/user/.cleanup/builds/test-404
    cmds:
      - 'mkdir -p ${JACAMAR_CI_BUILDS_DIR}/.credentials'
      - 'touch ${JACAMAR_CI_BUILDS_DIR}/.credentials/pass-456'
      - 'jacamar --no-auth cleanup --configuration {{configs}}/general/job_home_save.toml'
      - 'echo "credentials removed $(ls -l ${JACAMAR_CI_BUILDS_DIR}/.credentials | grep -v ^l | wc -l)"'
  result_parse:
    regex:
      output:
        # Provide a range in case we add more tests.
        regex: 'credentials removed\s*[1-4]'
        action: store_true
  result_evaluate:
    result: 'output'

cleanup-static-files:
  inherits_from: _job-static-shell
  summary: Verify old static directories removed.
  run:
    cmds:
      - 'jacamar --no-auth prepare'
      - 'jacamar --no-auth cleanup --configuration {{configs}}/general/static_shell.toml'
      - 'ls -l /var/tmp/user/static'
  result_parse:
    regex:
      output:
        regex: '00000123'
        action: store_false
  result_evaluate:
    result: 'output'


###############################################################################
# sigterm tests - separate to avoid potential collision with scripting
###############################################################################

run-sigterm:
  inherits_from: _job-home-save
  summary: Verify logging for captured SIGTERM.
  variables:
    jobscript: /tmp/sigtermlogsrun.bash
  run:
    cmds:
      - "cp {{scripts}}/jobs/job-whoami.bash {{jobscript}}"
      - "echo 'sleep 60' >> {{jobscript}}"
      - "jacamar --no-auth prepare"
      - "jacamar --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      - "sleep 7"
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sigtermlogsrun process is still running'"
  result_parse:
    regex:
      exited:
        regex: 'encountered during job: signal: killed'
        action: store_true
      ps:
        regex: 'sigtermlogs process is still running'
        action: store_false
  result_evaluate:
    result: 'exited and ps'

###############################################################################
# shell-profile tests - separate to avoid potential collision with profile
###############################################################################

run-jacamar-shell:
  inherits_from: _job-home-env
  summary: Enforce that a user's profile should expect the JACAMAR_CI_SHELL
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 123
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /var/tmp/job-pass.bash'
      - 'echo "env | grep JACAMAR_CI_SHELL && echo "PASS_SHELL"" >> /home/user/.profile'
      - 'jacamar --no-auth run /var/tmp/job-pass.bash step_script'
  result_parse:
    regex:
      shell:
        regex: 'PASS_SHELL'
        action: store_true
  result_evaluate:
    result: 'shell'
