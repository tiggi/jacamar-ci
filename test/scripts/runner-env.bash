#!/usr/bin/env bash

# Set default runner version to be latest nightly build

set -eo pipefail
set +o noclobber

mkdir -p ${CI_PROJECT_DIR}/srv/

if [ -z "${RUNNER_VERSION}" ]; then
  echo "Getting latest nightly build..."
  curl -o ${CI_PROJECT_DIR}/srv/gitlab_runner.rpm https://s3.amazonaws.com/gitlab-runner-downloads/main/rpm/gitlab-runner_amd64.rpm
else
  echo "Getting v${RUNNER_VERSION}..."
  curl -L -o %{CI_PROJECT_DIR}/srv/gitlab_runner.rpm https://gitlab.com/gitlab-org/gitlab-runner/-/releases/v${RUNNER_VERSION}/downloads/packages/rpm/gitlab-runner_amd64.rpm
fi

mv ${NIGHTLY_TEST_RUNNER_CONFIG} ${CI_PROJECT_DIR}/srv/config.toml
mv ${NIGHTLY_TEST_RUNNER_CUSTOM_CONFIG} ${CI_PROJECT_DIR}/srv/custom-config.toml

echo "Setting up container..."

${CONTAINER_RUNTIME} run \
  --rm \
  --detach \
  -v "${CI_PROJECT_DIR}/srv:/etc/gitlab-runner" \
  -v "${CI_PROJECT_DIR}/test/scripts/runner-install.bash:/runner-install.bash" \
  -v "${CI_PROJECT_DIR}/test/certs:/certs" \
  -v "${CI_PROJECT_DIR}/binaries:/opt/jacamar/bin" \
  -e "NIGHTLY_TEST_BOT_USER=${NIGHTLY_TEST_BOT_USER}" \
  -w "/etc/gitlab-runner" \
  -t "${SUSE_IMG}" \
  bash -c "/runner-install.bash"
