#!/usr/bin/env bash

# Execute 'make test' within a container.
set -eo pipefail
set +o noclobber

echo "Running Jacamar unit tests..."

[ -z "${ROOT_DIR}" ] && ROOT_DIR=$(pwd)
ci_dir="/builds/ecp-ci/jacamar-ci"

if [ -n "${2}" ] ; then
  echo "Target package ${2}..."
  TEST_CMD="make test-package PACKAGE=${2}"
else
  TEST_CMD="make test"
fi

${CONTAINER_RUNTIME} run \
  --rm \
  -v "${ROOT_DIR}:${ci_dir}:z" \
  -e "CI_PROJECT_DIR=${ci_dir}" \
  -e "CGO_ENABLED=0" \
  -w "${ci_dir}" \
  -t  "${GO_IMG}" \
  bash -c "${TEST_CMD}"
