#!/bin/bash

if [ "${VALIDATION_TEST_ENV}" == "testing" ] ; then
		echo '{"username": "pass"}'
		exit 0
elif [ "${RUNAS_CURRENT_USER}" == "gitlab" ] ; then
    echo '{"username": "gitlab"}'
    exit 0
fi

exit 1
