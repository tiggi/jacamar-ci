#!/usr/bin/env python

import gitlab
import os
import time

print("Getting project instance...")

gl = gitlab.Gitlab(url=os.getenv('NIGHTLY_TEST_PROJECT_URL'), private_token=os.getenv('NIGHTLY_TEST_TOKEN'))

project_id = os.getenv('NIGHTLY_TEST_PROJECT_ID')
project = gl.projects.get(project_id)
job_id = os.getenv('CI_JOB_ID')
runner_id = os.getenv('NIGHTLY_TEST_RUNNER_ID')

print("Checking is runner is online...")

runners_list = project.runners.list()
runner = [r for r in runners_list if str(r.id) == runner_id][0]

# Set counter for seconds elapsed waiting for runner to start
runner_time_elapsed = 0

while not runner.online:
  runner = [r for r in runners_list if str(r.id) == runner_id][0]
  time.sleep(5)
  runner_time_elapsed += 5
  if runner_time_elapsed > 120:
    break

if runner.online:
  print("Runner is online.")
else:
  raise Exception("WARNING: Runner is not online!")

print("Creating pipeline...")

pipeline = project.pipelines.create({'ref': 'main', 'variables': [{'key': 'UPSTREAM_CI_JOB_ID', 'value': job_id}]})

print("Checking pipeline status...")

running = project.pipelines.get(pipeline.id)

job_done_status = ["success", "failed", "canceled", "skipped"]

while running.status not in job_done_status:
  running = project.pipelines.get(pipeline.id)
  time.sleep(5)

print("Job complete - getting exit status...")

if running.status == "success":
  print("Nightly test job status: " + running.status)
else:
  raise Exception("WARNING: Nightly test job status: " + running.status)
