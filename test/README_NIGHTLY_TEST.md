## Nightly Testing With The Latest Runner Environment

In order to ensure the latest version of Jacamar CI remains compatible
with the latest version of Gitlab's runner, a workflow has been introduced
via a nightly scheduled job.

### Workflow

A nightly scheduled job has been put in place which establishes a project runner
using the latest version of Gitlab's runner and the latest version of
Jacamar CI. This triggers a CI/CD pipeline in a separate project
(`nightly-test-project`). The diagram below visualises the workflow involved:

![Nightly Test Workflow](../build/images/NightlyTestDiagram.png)

While running in `nightly-test-project`, the job utilises variables that
have been set in both the Gitlab CI/CD config file, and ones that have
also been set as project secrets due to containing sensitive information.

If a job is successful, the pipelines for both Jacamar CI and the nightly test
project will return with a success status. Any exit status other than success
(eg, failed, cancelled, skipped) will cause the pipeline in Jacamar CI to fail.

### Variables

A number of new variables have been introduced to facilitate this nightly testing
feature. Currently these include:

- `NIGHTLY_TEST_BOT_USER` - Username associated with the bot running the CI job
- `NIGHTLY_TEST_PROJECT_ID` - ID belonging to the project which the runner is
  registered to
- `NIGHTLY_TEST_PROJECT_URL` - URL used for the Gitlab instance hosting the project
- `NIGHTLY_TEST_RUNNER_ID` - ID belonging to the Gitlab runner registered to the
  nightly test project
- `NIGHTLY_TEST_TOKEN` - A secret token used for access to the project via the API